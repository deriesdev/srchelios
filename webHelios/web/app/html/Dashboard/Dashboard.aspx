﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="web.app.html.Dashboard.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Inicio • SigloBPO
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Inicio</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">

                    <div class="row">

                        <div class="col-lg-6 col-lg-offset-3">

                            <div style="text-align: center;">
                                <img style="max-width: 350px;" alt="image" src="/src/img/SigloBPO.png">
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">
</asp:Content>

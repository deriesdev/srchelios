using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcService
    {
        public entService add(entService _obj)
        {
            try
            {
                logService oEnt = new logService();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entService upd(entService _obj)
        {
            try
            {
                logService oEnt = new logService();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entService del(entService _obj)
        {
            try
            {
                logService oEnt = new logService();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entService sel(entService _obj)
        {
            try
            {
                logService oEnt = new logService();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entService> lis(entService _obj)
        {
            try
            {
                logService oEnt = new logService();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcService")]
    public partial interface intSvcService
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addService",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addService")]
        entService add(entService _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updService",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updService")]
        entService upd(entService _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delService",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delService")]
        entService del(entService _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selService",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selService")]
        entService sel(entService _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisService",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisService")]
        IList<entService> lis(entService _obj);
    }
}


using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entServiceType : entBase<entServiceType>
    {
        #region Propiedades
        [DataMember(Name ="Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        #endregion

        #region Constructor
        public entServiceType () { }
        public entServiceType (int _id) { id = _id; }
        public entServiceType(int _id, string _name, bool _active)
        {
            id = _id;
            name = _name;
            active = _active;
        }
        #endregion
        }
    }

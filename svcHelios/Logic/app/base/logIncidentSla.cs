using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logIncidentSla : ILogic<entIncidentSla>
    {
        public entIncidentSla add(entIncidentSla _obj)
        {
            try
            {
                if (!_obj.add()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentSla upd(entIncidentSla _obj)
        {
            try
            {
                if (!_obj.upd()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentSla del(entIncidentSla _obj)
        {
            try
            {
                if (!_obj.del()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentSla sel(entIncidentSla _obj)
        {
            try
            {
                if (!_obj.sel()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entIncidentSla> lis(entIncidentSla _obj)
        {
            try { return _obj.lis(); }
            catch (Exception ex) { throw ex; }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using web.intern;

namespace web.app.html.Incident
{
    public partial class IncidentUpd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string codIncident = Page.RouteData.Values["cod"] as string;
            _Session.set(_Session.list.incId, codIncident);
        }
    }
}
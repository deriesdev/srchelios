﻿CREATE TABLE [dbo].[people]
(
	[id]		INT				NOT NULL PRIMARY KEY IDENTITY, 
	[dni]		VARCHAR(20)		NOT NULL,
	[name]		VARCHAR(100)	NOT NULL,
	[nameP]		VARCHAR(50)		NOT NULL,
	[nameM]		VARCHAR(50)		NOT NULL,
	[pass]		VARCHAR(100)	NOT NULL,
	[birth]		DATETIME		NOT NULL,
	[email]		VARCHAR(100)	NOT NULL,
	[telf1]		VARCHAR(20)		NOT NULL DEFAULT '(--) ---------',
	[telf2]		VARCHAR(20)		NOT NULL DEFAULT '(--) ---------',
	[note]		VARCHAR(MAX)	NOT NULL DEFAULT 'Nota adicional',
	[comId]		INT	NOT NULL REFERENCES [company](id),
	[offId]		INT	NOT NULL REFERENCES [office](id),
	[jobId]		INT	NOT NULL REFERENCES [job](id),
	[typeId]	INT	NOT NULL REFERENCES [userType](id),
	[active]	BIT	NOT NULL DEFAULT 1
)

﻿CREATE TABLE [dbo].[incident]
(
	[id]			INT NOT NULL PRIMARY KEY IDENTITY,
	[name]			VARCHAR(200) NULL,

	[subject]		VARCHAR(200) NOT NULL,

	[origin]		INT NOT NULL,
	[state]			INT NOT NULL,

	[comId]			INT NOT NULL REFERENCES[company](id),
	[offId]			INT NOT NULL REFERENCES[office](id),
	[peoId]			INT NOT NULL REFERENCES[people](id),
	[serId]			INT NOT NULL REFERENCES[service](id),

	[urgId]			INT NOT NULL REFERENCES[urgency](id),
	[impId]			INT NOT NULL REFERENCES[impact](id),
	[priId]			INT NOT NULL REFERENCES[priority](id),

	[scale]			INT NULL REFERENCES[office](id),
	[scaleType]		BIT NULL,

	[expert]		INT NULL REFERENCES[people](id), 

    [likert]		INT NULL,
)

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="IncidentSel.aspx.cs" Inherits="web.app.html.Incident.IncidentSel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Incidencia • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Agregar Incidencia</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Incidente</a></li>
                <li class="active"><strong>Agregar</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <form id="formIncidentAddSet" role="form" novalidate="novalidate">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group" style="text-align: right;">
                    <div class="btn-group">
                        <button id="Inc_Get" style="display:none;" class="btn btn-white" type="button" onclick="jsIncident.Expert();">Tomar</button>
                        <button id="Inc_Upd" style="display:none;" class="btn btn-white" type="button" onclick="Incident_Edit();">Seguimiento</button>
                        <button id="Inc_Lik" style="display:none;" class="btn btn-primary" type="button" onclick="open_Likert();"><i class="fa fa-star" aria-hidden="true"></i>Evaluar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Incidencia</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Asunto:</label>
                                    <p id="Incident_Asunto"></p>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <i class="fa fa-ticket" aria-hidden="true"></i>
                                    <label>Ticket:</label>
                                    <p id="Incident_Cod"></p>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Origen:</label>
                                    <p id="Incident_Origin"></p>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Estado:</label>
                                    <p id="Incident_Status"></p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Compañia:</label>
                                    <p id="Incident_Company"></p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Área:</label>
                                    <p id="Incident_Office"></p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Persona:</label>
                                    <p id="Incident_People"></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Servicio:</label>
                                    <p id="Incident_Service"></p>
                                </div>
                            </div>

                        </div>
                        <!-- -->

                    </div>

                </div>

            </div>


            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Prioridad</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Urgency:</label>
                                    <div class="form-control-select2">
                                        <p id="Incident_Urgency"></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Impacto:</label>
                                    <div class="form-control-select2">
                                        <p id="Incident_Impact"></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Prioridad:</label>
                                    <div class="form-control-select2">
                                        <p id="Incident_Priority"></p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Detalle</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content ibox-heading">
                        <h3>Información detallada por nuestros colaboradores</h3>
                        <small><i class="fa fa-headphones"></i>Atención al Cliente - </small>
                        <small><i class="fa fa-wrench"></i>TI - Soporte Técnico - </small>
                        <small><i class="fa fa-laptop"></i>Desarrollo - Sistemas - </small>
                        <small><i class="fa fa-database"></i>Base de datos - </small>
                        <small><i class="fa fa-file-text"></i>Otra</small>
                    </div>
                    <div id="Incident_Detail" class="ibox-content inspinia-timeline"></div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Acuerdos de Servicio</h5>
                        <div class="ibox-tools">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-ellipsis-v"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" onclick="tblF5();"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            </ul>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">
                        <table id="tblIncidentSla" class="table table-striped table-bordered table-hover dataTables-example"></table>
                    </div>

                </div>
            </div>

        </div>
    </form>

    <!-- Modal -->
    <div class="modal inmodal" id="jsIncidentLikertSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Satisfacción del cliente</h4>
                </div>
                <form id="formIncidentLikertSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre:</label>
                                    <input id="IncidentLikertSet_Likert" class="irs-hidden-input" tabindex="-1" readonly="">
                                </div>

                            </div>
                        <!-- -->

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="IncidentLikertSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>






</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        //Likert
        $("#IncidentLikertSet_Likert").ionRangeSlider({
            grid: true,
            from: 2,
            min: 1,
            max: 5,
            values: ["Muy mala", "Mala", "Regular", "Buena", "Muy Buena"]
        });

        function open_Likert() {
            $('#jsIncidentLikertSet_md').modal('show');
        }

        function Incident_Edit() {
            let cod = $('#Id').val();
            jsUtil.Short.toPage('/Incident/upd/' + cod);
        }
        
        function tblF5() {
            jsIncidentSla.tblLis("#tblIncidentSla", $('#Id').val());
        }


        $(document).ready(function () {
            start();
        });


        function Inc_btn(d) {

            console.log(d);

            if (d.State == 4) { }
            else if (d.State == 3) {
                if (d.PeoId == $('#sId').val()) {
                    $('#Inc_Lik').show();
                }
            }
            else if (d.State > 0) {

                if (d.Expert != null) {

                    if (d.Expert == $('#sId').val()) {
                        $('#Inc_Upd').show();
                    }

                } else {
                    
                    if (d.Scale == $('#sOff').val()) {

                        let jer = (d.ScaleType == true) ? 3 : 5;

                        if ($('#sType').val() < jer) {
                            $('#Inc_Get').show();
                        }

                    }
                    
                }
            } else {
            jsUtil.Short.toPage('/Incident');
            }           
        }



        async function start() {
            var Inc = await jsIncident.Sel($("#sInc").val());

            $('#Id').val(Inc.Id);

            if (Inc.Id > 0) {

                Inc_btn(Inc);

                $('#Incident_Cod').text(Inc.Name);
                $('#Incident_Asunto').text(Inc.Subject);
                $('#Incident_Origin').text(jsUtil.Replace.byId(lstOrigen, 'Id', 'Name', Inc.Origin));
                $('#Incident_Status').text(jsUtil.Replace.byId(lstStatusAll, 'Id', 'Name', Inc.State));
                $('#Incident_Company').text(jsUtil.Replace.byId(await jsCompany.Lis(), 'Id', 'Name', Inc.ComId));
                $('#Incident_Office').text(jsUtil.Replace.byId(await jsOffice.Lis(), 'Id', 'Name', Inc.OffId));
                var Peo = await jsUtil.Replace.getObj(await jsPeople.Lis(), 'Id', Inc.PeoId);
                $('#Incident_People').text(`${Peo['NameP']} ${Peo['NameM']}, ${Peo['Name']}`);
                $('#Incident_Service').text(jsUtil.Replace.byId(await jsService.Lis(), 'Id', 'Name', Inc.SerId));

                $('#Incident_Urgency').text(jsUtil.Replace.byId(await jsUrgency.Lis(), 'Id', 'Name', Inc.UrgId));
                $('#Incident_Impact').text(jsUtil.Replace.byId(await jsImpact.Lis(), 'Id', 'Name', Inc.ImpId));
                $('#Incident_Priority').text(jsUtil.Replace.byId(await jsPriority.Lis(), 'Id', 'Name', Inc.PriId));

                await jsIncidentSla.tblLis("#tblIncidentSla", Inc.Id);


                var entExpert = await jsIncidentExpert.Obj();
                entExpert.IncId = Inc.Id;

                var qwe = await jsIncidentExpert.Lis(entExpert);

                var Incident_Item = `
                    <div class="timeline-item">
                        <div class="row">
                            <div class="col-xs-3 date">
                                <i class="fa <<|Office|>>"></i>
                                <<|Date|>>
                                <br />
                                <small><<|Hour|>></small>
                                <small class="text-navy"><<|Min|>> min.</small>
                                <br />
                                <small class="text-navy"><<|People|>></small>
                            </div>
                            <div class="col-xs-9 content">
                                <p class="m-b-xs"><strong>Interno</strong></p>
                                <div id="Incident_Int_<<|x|>>" class="summernote"></div>
                                <p class="m-b-xs"><strong>Público</strong></p>
                                <div id="Incident_Ext_<<|x|>>" class="summernote"></div>
                            </div>
                        </div>
                    </div>
                `;

                await $.each(qwe, async function (i, item) {

                    let template = Incident_Item.replace('<<|x|>>', i);
                    template = template.replace('<<|x|>>', i);

                    let PeoId = await jsPeople.Sel(item.PeoId);
                    let Name = `${PeoId.NameP} ${PeoId.NameM}, ${PeoId.Name}`

                    template = template.replace('<<|People|>>', Name);

                    let Office = "";

                    switch (PeoId.OffId) {
                        case 1: Office = "fa-headphones"; break;
                        case 2: Office = "fa-wrench"; break;
                        case 3: Office = "fa-laptop"; break;
                        case 4: Office = "fa-database"; break;
                        default: Office = "fa fa-file-text"; break;
                    }

                    template = template.replace('<<|Office|>>', Office);

                    let date = new Date(item.Date);
                    let day = `${addZero(date.getDate())}/${addZero(date.getMonth() + 1)}/${addZero(date.getFullYear())}`;
                    let hour = formatAMPM(date);

                    template = template.replace('<<|Date|>>', day);
                    template = template.replace('<<|Hour|>>', hour);
                    template = template.replace('<<|Min|>>', item.Time);

                    $('#Incident_Detail').append(template);

                    let Int = '#Incident_Int_' + i;
                    $(Int).summernote('code', item.NoteInt);
                    $(Int).summernote('destroy');
                    let Ext = '#Incident_Ext_' + i;
                    $(Ext).summernote('code', item.NoteExt);
                    $(Ext).summernote('destroy');
                });


            }
        }
    </script>

    <style>
        

    </style>


</asp:Content>

using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logPriority : ILogic<entPriority>
    {
        public entPriority add(entPriority _obj)
        {
            try
            {
                if (!_obj.add()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entPriority upd(entPriority _obj)
        {
            try
            {
                if (!_obj.upd()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entPriority del(entPriority _obj)
        {
            try
            {
                if (!_obj.del()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entPriority sel(entPriority _obj)
        {
            try
            {
                if (!_obj.sel()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entPriority> lis(entPriority _obj)
        {
            try { return _obj.lis(); }
            catch (Exception ex) { throw ex; }
        }
    }
}


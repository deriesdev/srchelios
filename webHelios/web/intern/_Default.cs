﻿using System;

namespace web.intern
{
    internal static class _Default
    {
        internal static class Var
        {
            internal static int Int = 0;
            internal static string String = "undefined";
            internal static DateTime Date = DateTime.MinValue;
            internal static bool Bool = false;
        }

        internal static class System
        {
            internal static int Id = 3;
        }

    }
}
﻿using System.Collections.Generic;

namespace Logic
{
    public interface ILogic<T>
    {
        T add(T _obj);
        T upd(T _obj);
        T del(T _obj);
        T sel(T _obj);
        IList<T> lis(T _obj);
    }
}

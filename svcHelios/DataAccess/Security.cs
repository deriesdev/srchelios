﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Crypt
    {
        //  lenght = 16 o 24 Byte
        private static string key = "$abc1234SigloMLV";

        //Public
        public static string En(string _s)
        {
            byte[] sArray = UTF8Encoding.UTF8.GetBytes(_s);
            TripleDESCryptoServiceProvider temp = new TripleDESCryptoServiceProvider();
            temp.Key = UTF8Encoding.UTF8.GetBytes(key);
            temp.Mode = CipherMode.ECB;
            temp.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = temp.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(sArray, 0, sArray.Length);
            temp.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string De(string _s)
        {
            byte[] sArray = Convert.FromBase64String(_s);
            TripleDESCryptoServiceProvider temp = new TripleDESCryptoServiceProvider();
            temp.Key = UTF8Encoding.UTF8.GetBytes(key);
            temp.Mode = CipherMode.ECB;
            temp.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = temp.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(sArray, 0, sArray.Length);
            temp.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}

﻿CREATE TABLE [dbo].[incidentSla]
(
	[id]			INT NOT NULL PRIMARY KEY IDENTITY,
	[name]			VARCHAR(100) NOT NULL,
	[state]			BIT NOT NULL DEFAULT 1,
	[incId]			INT NOT NULL REFERENCES[incident](id),

)

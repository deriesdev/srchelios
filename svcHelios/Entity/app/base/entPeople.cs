﻿using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entPeople : entBase<entPeople>
    {
        #region Propiedades
        [DataMember(Name = "Dni", Order = 2)]
        public string dni { set; get; } = string.Empty;
        [DataMember(Name = "Name", Order = 3)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name = "NameP", Order = 4)]
        public string nameP { set; get; } = string.Empty;
        [DataMember(Name = "NameM", Order = 5)]
        public string nameM { set; get; } = string.Empty;
        [DataMember(Name = "Pass", Order = 6)]
        public string pass { set; get; } = string.Empty;
        [DataMember(Name = "Birth", Order = 7)]
        public DateTime birth { set; get; } = DateTime.MinValue;
        [DataMember(Name = "Email", Order = 8)]
        public string email { set; get; } = string.Empty;
        [DataMember(Name = "Telf1", Order = 9)]
        public string telf1 { set; get; } = string.Empty;
        [DataMember(Name = "Telf2", Order = 10)]
        public string telf2 { set; get; } = string.Empty;
        [DataMember(Name = "Note", Order = 11)]
        public string note { set; get; } = string.Empty;
        [DataMember(Name = "OffId", Order = 12)]
        public int offId { set; get; } = 0;
        [DataMember(Name = "JobId", Order = 14)]
        public int jobId { set; get; } = 0;
        [DataMember(Name = "TypeId", Order = 15)]
        public int typeId { set; get; } = 0;
        #endregion

        #region Constructor
        public entPeople() { }
        public entPeople(int _id) { id = _id; }
        public entPeople(int _id, string _dni, string _name, string _nameP, string _nameM, string _pass, DateTime _birth, string _email, string _telf1, string _telf2, string _note, int _comId, int _offId, int _jobId, int _typeId, bool _active)
        {
            id = _id;
            dni = _dni;
            name = _name;
            nameP = _nameP;
            nameM = _nameM;
            pass = _pass;
            birth = _birth;
            email = _email;
            telf1 = _telf1;
            telf2 = _telf2;
            note = _note;
            comId = _comId;
            offId = _offId;
            jobId = _jobId;
            typeId = _typeId;
            active = _active;
        }
        #endregion
    }
}

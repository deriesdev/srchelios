﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="SLA.aspx.cs" Inherits="web.app.html.SLA.SLA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Servicio • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Servicio</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Administración de la configuración</a></li>
                <li class="active"><strong>Servicio</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista de servicio</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsSla.tblLis('#tblSla');"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            <li><a href="#" onclick="jsSla.$Form.Sel('#jsSlaSet_md', '#formSlaSet', '#tblSla');"><i class="fa fa-eye"></i>Ver</a></li>
                            <li><a href="#" onclick="jsSla.$Form.Ins('#jsSlaSet_md', '#formSlaSet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsSla.$Form.Upd('#jsSlaSet_md', '#formSlaSet', '#tblSla');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblSla" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>

            </div>
        </div>


        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista de tipo servicio</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsSlaType.tblLis('#tblSlaType');"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            <li><a href="#" onclick="jsSlaType.$Form.Sel('#jsSlaTypeSet_md', '#formSlaTypeSet', '#tblSlaType');"><i class="fa fa-eye"></i>Ver</a></li>
                            <li><a href="#" onclick="jsSlaType.$Form.Ins('#jsSlaTypeSet_md', '#formSlaTypeSet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsSlaType.$Form.Upd('#jsSlaTypeSet_md', '#formSlaTypeSet', '#tblSlaType');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblSlaType" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>

            </div>
        </div>



    </div>



    <!-- Modal Sla -->
    <div class="modal inmodal" id="jsSlaSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formSlaSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre:</label>
                                    <input type="text" id="SlaSet_Name" name="SlaSet_Name" placeholder="Nombre" class="form-control valid" required />
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Tipo:</label>
                                    <div class="form-control-select2">
                                        <select id="SlaSet_Type" name="SlaSet_Type" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Estado:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="SlaSet_Active" name="SlaSet_Active" class="onoffswitch-checkbox" disabled />
                                            <label class="onoffswitch-label" for="SlaSet_Active">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="SlaSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!-- Modal Sla Type -->
    <div class="modal inmodal" id="jsSlaTypeSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formSlaTypeSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre:</label>
                                    <input type="text" id="SlaTypeSet_Name" name="SlaTypeSet_Name" placeholder="Nombre" class="form-control valid" required />
                                </div>
                            </div>                        

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Estado:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="SlaTypeSet_Active" name="SlaTypeSet_Active" class="onoffswitch-checkbox" disabled />
                                            <label class="onoffswitch-label" for="SlaTypeSet_Active">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="SlaTypeSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            try {
                jsSla.tblLis('#tblSla');
                jsSlaType.tblLis('#tblSlaType');
            } catch (e) {
                jsNotify.Show(5, "Error", e.message.toString());
            }
        });

    </script>

</asp:Content>

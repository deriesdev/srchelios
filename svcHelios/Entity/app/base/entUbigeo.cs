using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entUbigeo : entBase<entUbigeo>
    {
        #region Propiedades
        [DataMember(Name = "Cod", Order = 2)]
        public string cod { set; get; } = string.Empty;
        [DataMember(Name = "Name", Order = 3)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name = "Up", Order = 4)]
        public int up { set; get; } = 0;
        [DataMember(Name = "Type", Order = 5)]
        public int type { set; get; } = 0;
        #endregion

        #region Constructor
        public entUbigeo() { }
        public entUbigeo(int _id) { id = _id; }
        public entUbigeo(int _id, string _cod, string _name, int _up, int _type)
        {
            id = _id;
            cod = _cod;
            name = _name;
            up = _up;
            type = _type;
        }
        #endregion
    }
}

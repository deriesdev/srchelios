using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logSla : ILogic<entSla>
    {
        public entSla add(entSla _obj)
        {
            try
            {
                if (!_obj.add()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entSla upd(entSla _obj)
        {
            try
            {
                if (!_obj.upd()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entSla del(entSla _obj)
        {
            try
            {
                if (!_obj.del()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entSla sel(entSla _obj)
        {
            try
            {
                if (!_obj.sel()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entSla> lis(entSla _obj)
        {
            try { return _obj.lis(); }
            catch (Exception ex) { throw ex; }
        }
    }
}


using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcCompany
    {
        public entCompany add(entCompany _obj)
        {
            try
            {
                logCompany oEnt = new logCompany();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entCompany upd(entCompany _obj)
        {
            try
            {
                logCompany oEnt = new logCompany();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entCompany del(entCompany _obj)
        {
            try
            {
                logCompany oEnt = new logCompany();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entCompany sel(entCompany _obj)
        {
            try
            {
                logCompany oEnt = new logCompany();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entCompany> lis(entCompany _obj)
        {
            try
            {
                logCompany oEnt = new logCompany();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcCompany")]
    public partial interface intSvcCompany
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addCompany",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addCompany")]
        entCompany add(entCompany _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updCompany",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updCompany")]
        entCompany upd(entCompany _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delCompany",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delCompany")]
        entCompany del(entCompany _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selCompany",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selCompany")]
        entCompany sel(entCompany _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisCompany",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisCompany")]
        IList<entCompany> lis(entCompany _obj);
    }
}


﻿using System;
using System.Web;
using System.Web.Routing;
using web.intern;

namespace web
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            _Route.RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (_Session.get(_Session.list.id) == null)
            {
                Response.RedirectToRoute("Login");
            }
            else
            {
                Response.RedirectToRoute("Dashboard");
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var error = Server.GetLastError() as HttpException;

            if (null != error)
            {
                int errorCode = error.GetHttpCode();

                if (404 == errorCode)
                {
                    Server.ClearError();
                    Server.Transfer("/404");
                }
            }
        }
        protected void Session_End(object sender, EventArgs e)
        {

        }
        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
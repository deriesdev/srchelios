﻿CREATE TABLE [dbo].[service]
(
	[id]		INT NOT NULL PRIMARY KEY IDENTITY,
	[name]		VARCHAR(80) NOT NULL,
	[type]		INT NULL REFERENCES[serviceType](id),
	[active]	BIT NOT NULL DEFAULT 1
)

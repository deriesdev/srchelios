using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcIncidentSla
    {
        public entIncidentSla add(entIncidentSla _obj)
        {
            try
            {
                logIncidentSla oEnt = new logIncidentSla();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentSla upd(entIncidentSla _obj)
        {
            try
            {
                logIncidentSla oEnt = new logIncidentSla();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentSla del(entIncidentSla _obj)
        {
            try
            {
                logIncidentSla oEnt = new logIncidentSla();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentSla sel(entIncidentSla _obj)
        {
            try
            {
                logIncidentSla oEnt = new logIncidentSla();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entIncidentSla> lis(entIncidentSla _obj)
        {
            try
            {
                logIncidentSla oEnt = new logIncidentSla();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcIncidentSla")]
    public partial interface intSvcIncidentSla
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addIncidentSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addIncidentSla")]
        entIncidentSla add(entIncidentSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updIncidentSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updIncidentSla")]
        entIncidentSla upd(entIncidentSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delIncidentSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delIncidentSla")]
        entIncidentSla del(entIncidentSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selIncidentSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selIncidentSla")]
        entIncidentSla sel(entIncidentSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisIncidentSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisIncidentSla")]
        IList<entIncidentSla> lis(entIncidentSla _obj);
    }
}


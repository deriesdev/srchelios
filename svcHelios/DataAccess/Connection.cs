﻿using System;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataAccess
{
    public class Connection : Persistence, IDisposable
    {
        #region Variables
        private DbConnection con = null;
        private string conStr = null;
        #endregion

        #region Properties
        protected override string cnStr => conStr;
        protected override DbConnection cn => con;
        #endregion

        #region Constructors
        public Connection()
        {
            var origin = DB.get(DB.list.Default);

            switch (origin.motor)
            {
                case DB.Motor.MsSQL:
                    conStr = $"Data Source={origin.svr}; Initial Catalog={origin.db}; Persist Security Info=True; User ID={origin.user}; Password={origin.pass}";
                    con = new SqlConnection(cnStr);
                    break;
                case DB.Motor.MySQL:
                    break;
                case DB.Motor.PostgreSQL:
                    break;
                case DB.Motor.DB2:
                    break;
                case DB.Motor.Oracle:
                    break;
                default:
                    throw new Exception("Motor of DB not found.");
            }

            cnOpen();
        }

        ~Connection()
        {
            Dispose();
        }

        public void Dispose()
        {
            cnClose();
        }
        #endregion
    }
}

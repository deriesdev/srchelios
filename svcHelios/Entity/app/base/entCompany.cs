using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entCompany : entBase<entCompany>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name = "NumberDoc", Order = 3)]
        public string numberDoc { set; get; } = string.Empty;
        [DataMember(Name = "Address", Order = 4)]
        public string address { set; get; } = string.Empty;
        [DataMember(Name = "Ubigeo", Order = 5)]
        public int ubigeo { set; get; } = 0;
        #endregion

        #region Constructor
        public entCompany() { }
        public entCompany(int _id) { id = _id; }
        public entCompany(int _id, string _name, string _numberDoc, string _address, int _ubigeo, bool _active)
        {
            id = _id;
            name = _name;
            numberDoc = _numberDoc;
            address = _address;
            ubigeo = _ubigeo;
            active = _active;
        }
        #endregion
    }
}

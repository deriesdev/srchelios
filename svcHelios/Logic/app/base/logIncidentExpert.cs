using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logIncidentExpert : ILogic<entIncidentExpert>
    {
        public entIncidentExpert add(entIncidentExpert _obj)
        {
            try
            {
                if (!_obj.add()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentExpert upd(entIncidentExpert _obj)
        {
            try
            {
                if (!_obj.upd()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentExpert del(entIncidentExpert _obj)
        {
            try
            {
                if (!_obj.del()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentExpert sel(entIncidentExpert _obj)
        {
            try
            {
                if (!_obj.sel()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entIncidentExpert> lis(entIncidentExpert _obj)
        {
            try { return _obj.lis(); }
            catch (Exception ex) { throw ex; }
        }
    }
}


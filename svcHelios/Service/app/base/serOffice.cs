using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcOffice
    {
        public entOffice add(entOffice _obj)
        {
            try
            {
                logOffice oEnt = new logOffice();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entOffice upd(entOffice _obj)
        {
            try
            {
                logOffice oEnt = new logOffice();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entOffice del(entOffice _obj)
        {
            try
            {
                logOffice oEnt = new logOffice();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entOffice sel(entOffice _obj)
        {
            try
            {
                logOffice oEnt = new logOffice();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entOffice> lis(entOffice _obj)
        {
            try
            {
                logOffice oEnt = new logOffice();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcOffice")]
    public partial interface intSvcOffice
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addOffice",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addOffice")]
        entOffice add(entOffice _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updOffice",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updOffice")]
        entOffice upd(entOffice _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delOffice",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delOffice")]
        entOffice del(entOffice _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selOffice",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selOffice")]
        entOffice sel(entOffice _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisOffice",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisOffice")]
        IList<entOffice> lis(entOffice _obj);
    }
}


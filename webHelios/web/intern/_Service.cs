﻿namespace web.intern
{
    public partial class entException
    {
        public int Id { get; set; } = 0;
        public string Err { get; set; } = string.Empty;
        public string ErrCatch { get; set; } = string.Empty;

        internal entException() { }
    }

}
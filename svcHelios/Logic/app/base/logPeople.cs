﻿using DataAccess;
using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logPeople : ILogic<entPeople>
    {
        public entPeople add(entPeople _obj)
        {
            try
            {
                _obj.pass = Crypt.En(_obj.dni);

                if (!_obj.add()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entPeople upd(entPeople _obj)
        {
            try
            {
                if (!_obj.upd()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entPeople del(entPeople _obj)
        {
            try
            {
                if (!_obj.del()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entPeople sel(entPeople _obj)
        {
            try
            {
                if (!_obj.sel()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entPeople> lis(entPeople _obj)
        {
            try { return _obj.lis(); }
            catch (Exception ex) { throw ex; }
        }
    }
}


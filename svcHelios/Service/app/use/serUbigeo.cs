using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcUbigeo
    {
        public IList<entUbigeo> children(entUbigeo _obj)
        {
            try
            {
                logUbigeo oEnt = new logUbigeo();
                return oEnt.children(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entUbigeo> reverse(entUbigeo _obj)
        {
            try
            {
                logUbigeo oEnt = new logUbigeo();
                return oEnt.reverse(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

    }
    public partial interface intSvcUbigeo
    {

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/childrenUbigeo",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "childrenUbigeo")]
        IList<entUbigeo> children(entUbigeo _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/reverseUbigeo",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "reverseUbigeo")]
        IList<entUbigeo> reverse(entUbigeo _obj);

    }
}


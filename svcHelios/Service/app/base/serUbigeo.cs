using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcUbigeo
    {
        public entUbigeo add(entUbigeo _obj)
        {
            try
            {
                logUbigeo oEnt = new logUbigeo();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUbigeo upd(entUbigeo _obj)
        {
            try
            {
                logUbigeo oEnt = new logUbigeo();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUbigeo del(entUbigeo _obj)
        {
            try
            {
                logUbigeo oEnt = new logUbigeo();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUbigeo sel(entUbigeo _obj)
        {
            try
            {
                logUbigeo oEnt = new logUbigeo();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entUbigeo> lis(entUbigeo _obj)
        {
            try
            {
                logUbigeo oEnt = new logUbigeo();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

    }

    [ServiceContract(Name = "svcUbigeo")]
    public partial interface intSvcUbigeo
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addUbigeo",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addUbigeo")]
        entUbigeo add(entUbigeo _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updUbigeo",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updUbigeo")]
        entUbigeo upd(entUbigeo _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delUbigeo",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delUbigeo")]
        entUbigeo del(entUbigeo _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selUbigeo",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selUbigeo")]
        entUbigeo sel(entUbigeo _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisUbigeo",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisUbigeo")]
        IList<entUbigeo> lis(entUbigeo _obj);
    }
}


using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logCompany : ILogic<entCompany>
    {
        public entCompany add(entCompany _obj)
        {
            try
            {
                if (!_obj.add()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entCompany upd(entCompany _obj)
        {
            try
            {
                if (!_obj.upd()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entCompany del(entCompany _obj)
        {
            try
            {
                if (!_obj.del()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entCompany sel(entCompany _obj)
        {
            try
            {
                if (!_obj.sel()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entCompany> lis(entCompany _obj)
        {
            try { return _obj.lis(); }
            catch (Exception ex) { throw ex; }
        }
    }
}


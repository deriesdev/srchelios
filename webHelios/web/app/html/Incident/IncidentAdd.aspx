﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="IncidentAdd.aspx.cs" Inherits="web.app.html.Incident.IncidentAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Registrar incidencia • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Agregar Incidencia</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Incidente</a></li>
                <li class="active"><strong>Agregar</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <form id="formIncidentAddSet" role="form" novalidate="novalidate">
        <div class="row">

            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Incidencia</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Asunto:</label>
                                    <input type="text" id="Incident_Asunto" name="Incident_Asunto" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Origen:</label>
                                    <select id="Incident_Origen" name="Incident_Origen" class="select2" style="width: 100% !important;"></select>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Estado:</label>
                                    <select id="Incident_Status" name="Incident_Status" class="select2" style="width: 100% !important;"></select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Compañia:</label>
                                    <select id="Incident_Company" name="Incident_Company" class="select2" onchange="jsOffice.$cbOffice.get('#Incident_Company', '#Incident_Office', false); jsComSvc.$cb.get('#Incident_Company', '#Incident_Service');" style="width: 100% !important;"></select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Área:</label>
                                    <select id="Incident_Office" name="Incident_Office" class="select2" onchange="jsPeople.$cb.getOff('#Incident_Office', '#Incident_People');" style="width: 100% !important;"></select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Persona:</label>
                                    <select id="Incident_People" name="Incident_People" class="select2" style="width: 100% !important;"></select>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Servicio:</label>
                                    <select id="Incident_Service" name="Incident_Service" class="select2" style="width: 100% !important;"></select>
                                </div>
                            </div>

                        </div>
                        <!-- -->

                    </div>

                </div>

            </div>


            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Prioridad</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Urgency:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Urgency" name="Incident_Urgency" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Impacto:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Impact" name="Incident_Impact" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Prioridad:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Priority" name="Incident_Priority" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Detalle</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Interno:</label>
                                    <div id="Incident_NoteInt" class="summernote"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Publico:</label>
                                    <div id="Incident_NoteExt" class="summernote"></div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Escalado</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Jerárquico:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="Incident_ScaleType" name="Incident_ScaleType" class="onoffswitch-checkbox"/>
                                            <label class="onoffswitch-label" for="Incident_ScaleType">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Área:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Scale" name="Incident_Scale" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>


            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Cierre</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Tiempo utilizado:</label>
                                    <input type="time" id="Incident_Time" name="Incident_Time" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                        </div>
                        <!-- -->

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                            <button id="CompanySet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </form>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            try {

                $('#Id').val(0);

                jsCompany.$cb.get('#Incident_Company');
                jsStatus.$cb.get('#Incident_Status');
                jsOrigen.$cb.get('#Incident_Origen');
                jsUrgency.$cb.get('#Incident_Urgency');
                jsImpact.$cb.get('#Incident_Impact');
                jsPriority.$cb.get('#Incident_Priority');
                jsOffice.$cbOffice.getOP('#Incident_Scale');
            } catch (e) {
                debugger;
                jsNotify.Show(5, "Error", e.message.toString());
            }
        });
               


    </script>

</asp:Content>

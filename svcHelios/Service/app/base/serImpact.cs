using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcImpact
    {
        public entImpact add(entImpact _obj)
        {
            try
            {
                logImpact oEnt = new logImpact();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entImpact upd(entImpact _obj)
        {
            try
            {
                logImpact oEnt = new logImpact();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entImpact del(entImpact _obj)
        {
            try
            {
                logImpact oEnt = new logImpact();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entImpact sel(entImpact _obj)
        {
            try
            {
                logImpact oEnt = new logImpact();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entImpact> lis(entImpact _obj)
        {
            try
            {
                logImpact oEnt = new logImpact();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcImpact")]
    public partial interface intSvcImpact
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addImpact",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addImpact")]
        entImpact add(entImpact _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updImpact",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updImpact")]
        entImpact upd(entImpact _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delImpact",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delImpact")]
        entImpact del(entImpact _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selImpact",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selImpact")]
        entImpact sel(entImpact _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisImpact",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisImpact")]
        IList<entImpact> lis(entImpact _obj);
    }
}


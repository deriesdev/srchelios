﻿CREATE TABLE [dbo].[comSvc]
(
	[id]		INT NOT NULL PRIMARY KEY IDENTITY,
	[comId]		INT NULL REFERENCES[company](id),
	[serId]		INT NULL REFERENCES[service](id),
	[active]	BIT NOT NULL DEFAULT 1
)

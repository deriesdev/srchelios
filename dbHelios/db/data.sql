﻿/* USP */

	--------------------------------------------------
	-- All Trigger
	--------------------------------------------------

	CREATE TRIGGER trgIncidentSla ON [dbo].[incident]
	FOR INSERT
	AS
	BEGIN

		DECLARE @id INT;
		SELECT @id = i.id from inserted i;

		DECLARE @comId INT;
		SELECT @comId = i.comId from inserted i;
		DECLARE @serId INT;
		SELECT @serId = i.serId from inserted i;
	
		DECLARE @ConSvc INT = (select id from comSvc where comId = @comId and serId = @serId);

		INSERT INTO [dbo].[incidentSla] ([name], [incId])
		SELECT CONCAT(s.name, ' ', css.data), @id FROM comSvcSla css
		INNER JOIN SLA s on s.id = css.sla
		WHERE css.active = 1 and css.comSvc = @ConSvc
	
		DECLARE @cod VARCHAR(10) = (concat ('X-',RIGHT('00000000' + CAST(@id AS VARCHAR(8)), 8)))

		UPDATE [dbo].[incident] SET [name] = @cod WHERE id = @id

	END
	GO

	--------------------------------------------------
	-- All Function
	--------------------------------------------------

	CREATE FUNCTION [dbo].[Active](
		@active bit
	)
	RETURNS int
	AS
		BEGIN
				DECLARE @rpt int
				SET @rpt = CASE @active
					WHEN 0 THEN -1
					WHEN 1 THEN 0
				END
				return @rpt
		END
	GO

	CREATE FUNCTION [dbo].[IncidentTime](
		@incId INT
	)
	RETURNS int
	AS
		BEGIN
				DECLARE @rpt INT
				SET @rpt = (SELECT SUM(DATEDIFF(MINUTE, '0:00:00', time)) FROM incidentExpert WHERE incId = @incId)
				return @rpt
		END
	GO

	CREATE FUNCTION [dbo].[IncidentDate](
		@incId INT
	)
	RETURNS DATETIME
	AS
		BEGIN
				DECLARE @rpt DATETIME
				SET @rpt = (select TOP 1 date from incidentExpert where incid = @incId ORDER BY id asc)
				return @rpt
		END
	GO

	CREATE FUNCTION [dbo].[IncidentSLAVal](
		@incId int
	)
	RETURNS bit
	AS
		BEGIN
				DECLARE @sel int
				SET @sel = (select COUNT(id) from incidentSLA WHERE state = 0 and incId = @incId)
				
				return CAST(IIF (@sel > 0, 0, 1) AS BIT);
		END
	GO



	--------------------------------------------------
	-- All Store Procedure userType
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspUserTypeIns(
			@name VARCHAR(200),
			@active BIT
		)
		AS
			INSERT INTO [dbo].[UserType] (name, active) VALUES
				(@name, @active)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspUserTypeUpd(
			@id INT,
			@name VARCHAR(200),
			@active BIT
		)
		AS
			UPDATE [dbo].[UserType] SET
				name = @name,
				active = @active
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspUserTypeDel( @id INT )
		AS
			DELETE FROM [dbo].[UserType] WHERE id = @id
		GO


		CREATE PROCEDURE uspUserTypeSel( @id INT )
		AS
			SELECT * FROM [dbo].[UserType] WHERE id = @id
		GO


		CREATE PROCEDURE uspUserTypeLis(
			@active bit
		)
		AS
			SET @active = ISNULL(@active, 0);

			declare @bool BIT
			set @bool = dbo.Active(@active);

			SELECT * FROM [dbo].[UserType]
			WHERE (@bool <> 0 or isnull(active, 0) <> @bool)
			ORDER BY [active] desc, [name] asc
		GO


	--------------------------------------------------
	-- All Store Procedure job
	--------------------------------------------------

		/* Base */
		CREATE PROCEDURE uspJobIns(
			@name VARCHAR(50)
		)
		AS
			INSERT INTO [dbo].[Job] (name) VALUES
				(@name)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO
		CREATE PROCEDURE uspJobUpd(
			@id INT,
			@name VARCHAR(50),
			@active BIT
		)
		AS
			UPDATE [dbo].[Job] SET
				name = @name,
				active = @active
			WHERE
				id = @id
		GO
		CREATE PROCEDURE uspJobDel( @id INT )
		AS
			DELETE FROM [dbo].[Job] WHERE id = @id
		GO
		CREATE PROCEDURE uspJobSel( @id INT )
		AS
			SELECT * FROM [dbo].[Job] WHERE id = @id
		GO
		CREATE PROCEDURE uspJobLis (
			@active bit
		)
		AS
			SET @active = ISNULL(@active, 0);

			declare @bool BIT
			set @bool = dbo.Active(@active);

			SELECT * FROM [dbo].[Job] 
			WHERE (@bool <> 0 or isnull(active, 0) <> @bool)
			ORDER BY [active] desc, [name] asc
		GO


	--------------------------------------------------
	-- All Store Procedure ubigeoType
	--------------------------------------------------

		/* It is not necessar */
		

	--------------------------------------------------
	-- All Store Procedure ubigeo
	--------------------------------------------------

		/* Base */ /* It is not necessar */
		
		/* Special */

		CREATE PROCEDURE uspUbigeoChildren( @id INT )
		AS
			IF (@id < 0)
				SELECT * FROM [dbo].[ubigeo] WHERE [type] = 1;
			ELSE IF (@id > 0)
				SELECT * FROM [dbo].[ubigeo] WHERE [up] = @id;
		GO

		CREATE PROCEDURE uspUbigeoReverse( @id INT )
		AS
			CREATE TABLE #TEMP ([id] INT, [cod] VARCHAR(10), [name] VARCHAR(50), [up] INT, [type] INT);
			WHILE @id IS NOT NULL
			BEGIN
				INSERT INTO #TEMP ([id], [cod] ,[name], [up], [type])
				SELECT * FROM [dbo].[ubigeo] WHERE [id] = @id;
				SET @id = (SELECT [up] FROM [dbo].[ubigeo] WHERE [id] = @id);	
			END;
	
			SELECT * FROM #TEMP ORDER BY type asc;
			DROP TABLE #TEMP;
		GO


	--------------------------------------------------
	-- All Store Procedure company
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspCompanyIns(
			@name VARCHAR(50),
			@numberDoc VARCHAR(20),
			@address VARCHAR(200),
			@ubigeo INT
		)
		AS
			INSERT INTO [dbo].[Company] (name, numberDoc, address, ubigeo) VALUES
				(@name, @numberDoc, @address, @ubigeo)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspCompanyUpd(
			@id INT,
			@name VARCHAR(50),
			@numberDoc VARCHAR(20),
			@address VARCHAR(200),
			@ubigeo INT,
			@active BIT
		)
		AS
			UPDATE [dbo].[Company] SET
				name = @name,
				numberDoc = @numberDoc,
				address = @address,
				ubigeo = @ubigeo,
				active = @active
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspCompanyDel( @id INT )
		AS
			DELETE FROM [dbo].[Company] WHERE id = @id
		GO


		CREATE PROCEDURE uspCompanySel( @id INT )
		AS
			SELECT * FROM [dbo].[Company] WHERE id = @id
		GO


		CREATE PROCEDURE uspCompanyLis(
			@active bit
		)
		AS
			SET @active = ISNULL(@active, 0);

			declare @bool BIT
			set @bool = dbo.Active(@active);

			SELECT * FROM [dbo].[Company] 
			WHERE 
				(@bool <> 0 or isnull(active, 0) <> @bool)
			ORDER BY  [active] desc, [name] asc
		GO


	--------------------------------------------------
	-- All Store Procedure office
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspOfficeIns(
			@name VARCHAR(50),
			@comId INT,
			@type BIT
		)
		AS
			INSERT INTO [dbo].[Office] (name, comId, type) VALUES
				(@name, @comId, @type)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspOfficeUpd(
			@id INT,
			@name VARCHAR(50),
			@comId INT,
			@type BIT,
			@active BIT
		)
		AS
			UPDATE [dbo].[Office] SET
				name = @name,
				comId = @comId,
				type = @type,
				active = @active
			WHERE
				id = @id
		GO

		CREATE PROCEDURE uspOfficeDel( @id INT )
		AS
			DELETE FROM [dbo].[Office] WHERE id = @id
		GO

		CREATE PROCEDURE uspOfficeSel( @id INT )
		AS
			SELECT * FROM [dbo].[Office] WHERE id = @id
		GO

		CREATE PROCEDURE uspOfficeLis
		(
			@comId int,
			@type bit,
			@active bit
		)
		AS
			BEGIN
				SET @comId = ISNULL(@comId, 0);
				SET @type = ISNULL(@type, 0);
				SET @active = ISNULL(@active, 0);

				declare @area BIT
				set @area = dbo.Active(@type);

				declare @bool BIT
				set @bool = dbo.Active(@active);

				SELECT * FROM [dbo].[Office]
				WHERE
					(@comId = 0 or isnull(comId, 0)= @comId) and
					(@area <> 0 or isnull(type, 0) <> @area) and
					(@bool <> 0 or isnull(active, 0) <> @bool)
				ORDER BY  [active] desc, [name] asc
			END
		GO


	--------------------------------------------------
	-- All Store Procedure impact
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspImpactIns(
			@name VARCHAR(50)
		)
		AS
			INSERT INTO [dbo].[Impact] (name) VALUES
				(@name)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspImpactUpd(
			@id INT,
			@name VARCHAR(50)
		)
		AS
			UPDATE [dbo].[Impact] SET
				name = @name
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspImpactDel( @id INT )
		AS
			DELETE FROM [dbo].[Impact] WHERE id = @id
		GO


		CREATE PROCEDURE uspImpactSel( @id INT )
		AS
			SELECT * FROM [dbo].[Impact] WHERE id = @id
		GO


		CREATE PROCEDURE uspImpactLis
		AS
			SELECT * FROM [dbo].[Impact]
		GO

	--------------------------------------------------
	-- All Store Procedure priority
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspPriorityIns(
			@name VARCHAR(50)
		)
		AS
			INSERT INTO [dbo].[Priority] (name) VALUES
				(@name)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspPriorityUpd(
			@id INT,
			@name VARCHAR(50)
		)
		AS
			UPDATE [dbo].[Priority] SET
				name = @name
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspPriorityDel( @id INT )
		AS
			DELETE FROM [dbo].[Priority] WHERE id = @id
		GO


		CREATE PROCEDURE uspPrioritySel( @id INT )
		AS
			SELECT * FROM [dbo].[Priority] WHERE id = @id
		GO


		CREATE PROCEDURE uspPriorityLis
		AS
			SELECT * FROM [dbo].[Priority]
		GO


	--------------------------------------------------
	-- All Store Procedure urgency
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspUrgencyIns(
			@name VARCHAR(50)
		)
		AS
			INSERT INTO [dbo].[Urgency] (name) VALUES
				(@name)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspUrgencyUpd(
			@id INT,
			@name VARCHAR(50)
		)
		AS
			UPDATE [dbo].[Urgency] SET
				name = @name
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspUrgencyDel( @id INT )
		AS
			DELETE FROM [dbo].[Urgency] WHERE id = @id
		GO


		CREATE PROCEDURE uspUrgencySel( @id INT )
		AS
			SELECT * FROM [dbo].[Urgency] WHERE id = @id
		GO


		CREATE PROCEDURE uspUrgencyLis
		AS
			SELECT * FROM [dbo].[Urgency]
		GO


	--------------------------------------------------
	-- All Store Procedure serviceType
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspServiceTypeIns(
			@name VARCHAR(80)
		)
		AS
			INSERT INTO [dbo].[ServiceType] (name) VALUES
				(@name)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspServiceTypeUpd(
			@id INT,
			@name VARCHAR(80),
			@active BIT
		)
		AS
			UPDATE [dbo].[ServiceType] SET
				name = @name,
				active = @active
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspServiceTypeDel( @id INT )
		AS
			DELETE FROM [dbo].[ServiceType] WHERE id = @id
		GO


		CREATE PROCEDURE uspServiceTypeSel( @id INT )
		AS
			SELECT * FROM [dbo].[ServiceType] WHERE id = @id
		GO


		CREATE PROCEDURE uspServiceTypeLis
		(
		@active bit
		)
		AS
			SET @active = ISNULL(@active, 0);

			declare @bool BIT
			set @bool = dbo.Active(@active);					

			SELECT * FROM [dbo].[ServiceType]
			WHERE
				(@bool <> 0 or isnull(active, 0) <> @bool)
			ORDER BY  [active] desc, [name] asc

		GO


	--------------------------------------------------
	-- All Store Procedure service
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspServiceIns(
			@name VARCHAR(80),
			@type INT
		)
		AS
			INSERT INTO [dbo].[Service] (name, type) VALUES
				(@name, @type)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspServiceUpd(
			@id INT,
			@name VARCHAR(80),
			@type INT,
			@active BIT
		)
		AS
			UPDATE [dbo].[Service] SET
				name = @name,
				type = @type,
				active = @active
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspServiceDel( @id INT )
		AS
			DELETE FROM [dbo].[Service] WHERE id = @id
		GO


		CREATE PROCEDURE uspServiceSel( @id INT )
		AS
			SELECT * FROM [dbo].[Service] WHERE id = @id
		GO


		CREATE PROCEDURE uspServiceLis
		(
			@type int,
			@active bit
		)
		AS
			SET @type = ISNULL(@type, 0);
			SET @active = ISNULL(@active, 0);

			declare @bool BIT
			set @bool = dbo.Active(@active);

			SELECT * FROM [dbo].[Service]
			WHERE
				(@type = 0 or isnull(type, 0)= @type) and
				(@bool <> 0 or isnull(active, 0) <> @bool)
			ORDER BY  [active] desc, [name] asc
		GO


	--------------------------------------------------
	-- All Store Procedure slaType
	--------------------------------------------------

	/* Base */

	CREATE PROCEDURE uspSlaTypeIns(
		@name VARCHAR(80)
	)
	AS
		INSERT INTO [dbo].[SlaType] (name) VALUES
			(@name)

		IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
		ELSE SELECT 0
	GO


	CREATE PROCEDURE uspSlaTypeUpd(
		@id INT,
		@name VARCHAR(80),
		@active BIT
	)
	AS
		UPDATE [dbo].[SlaType] SET
			name = @name,
			active = @active
		WHERE
			id = @id
	GO


	CREATE PROCEDURE uspSlaTypeDel( @id INT )
	AS
		DELETE FROM [dbo].[SlaType] WHERE id = @id
	GO


	CREATE PROCEDURE uspSlaTypeSel( @id INT )
	AS
		SELECT * FROM [dbo].[SlaType] WHERE id = @id
	GO

	CREATE PROCEDURE uspSlaTypeLis
	(
		@active bit
	)
	AS
		SET @active = ISNULL(@active, 0);

		declare @bool BIT
		set @bool = dbo.Active(@active);					

		SELECT * FROM [dbo].[SlaType]
		WHERE
			(@bool <> 0 or isnull(active, 0) <> @bool)
		ORDER BY  [active] desc, [name] asc

	GO

	--------------------------------------------------
	-- All Store Procedure sla
	--------------------------------------------------

	/* Base */

	CREATE PROCEDURE uspSlaIns(
		@name VARCHAR(80),
		@type INT
	)
	AS
		INSERT INTO [dbo].[Sla] (name, type) VALUES
			(@name, @type)

		IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
		ELSE SELECT 0
	GO


	CREATE PROCEDURE uspSlaUpd(
		@id INT,
		@name VARCHAR(80),
		@type INT,
		@active BIT
	)
	AS
		UPDATE [dbo].[Sla] SET
			name = @name,
			type = @type,
			active = @active
		WHERE
			id = @id
	GO


	CREATE PROCEDURE uspSlaDel( @id INT )
	AS
		DELETE FROM [dbo].[Sla] WHERE id = @id
	GO


	CREATE PROCEDURE uspSlaSel( @id INT )
	AS
		SELECT * FROM [dbo].[Sla] WHERE id = @id
	GO

	CREATE PROCEDURE uspSlaLis
	(
		@type INT,
		@active bit
	)
	AS
		SET @type = ISNULL(@type, 0);
		SET @active = ISNULL(@active, 0);

		declare @bool BIT
		set @bool = dbo.Active(@active);

		SELECT * FROM [dbo].[Sla]
		WHERE
			(@type = 0 or isnull(type, 0)= @type) and
			(@bool <> 0 or isnull(active, 0) <> @bool)
		ORDER BY  [active] desc, [name] asc
	GO




	--------------------------------------------------
	-- All Store Procedure comSvc
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspComSvcIns(
			@comId INT,
			@serId INT
		)
		AS
			INSERT INTO [dbo].[ComSvc] (comId, serId) VALUES
				(@comId, @serId)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspComSvcUpd(
			@id INT,
			@active BIT
		)
		AS
			UPDATE [dbo].[ComSvc] SET
				active = @active
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspComSvcDel( @id INT )
		AS
			DELETE FROM [dbo].[ComSvc] WHERE id = @id
		GO


		CREATE PROCEDURE uspComSvcSel( @id INT )
		AS
			SELECT * FROM [dbo].[ComSvc] WHERE id = @id
		GO


		CREATE PROCEDURE uspComSvcLis
		( 
			@comId int,
			@active BIT 
		)
		AS
			SET @comId = ISNULL(@comId, 0);
			SET @active = ISNULL(@active, 0);

			declare @bool BIT
			set @bool = dbo.Active(@active);

			SELECT * 
			FROM [dbo].[ComSvc]
			WHERE
				(@comId = 0 or isnull(comId, 0)= @comId) and
				(@bool <> 0 or isnull(active, 0) <> @bool)
			ORDER BY  [active] desc
		GO




	--------------------------------------------------
	-- All Store Procedure comSvcSla
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspComSvcSlaIns(
			@data VARCHAR(20),
			@comSvc INT,
			@sla INT
		)
		AS
			INSERT INTO [dbo].[ComSvcSla] (data, comSvc, sla) VALUES
				(@data, @comSvc, @sla)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspComSvcSlaUpd(
			@id INT,
			@data VARCHAR(20),
			@active BIT
		)
		AS
			UPDATE [dbo].[ComSvcSla] SET
				data = @data,
				active = @active
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspComSvcSlaDel( @id INT )
		AS
			DELETE FROM [dbo].[ComSvcSla] WHERE id = @id
		GO


		CREATE PROCEDURE uspComSvcSlaSel( @id INT )
		AS
			SELECT * FROM [dbo].[ComSvcSla] WHERE id = @id
		GO


		CREATE PROCEDURE uspComSvcSlaLis
		( 
			@comSvc int,
			@active BIT 
		)
		AS
			SET @comSvc = ISNULL(@comSvc, 0);
			SET @active = ISNULL(@active, 0);

			declare @bool BIT
			set @bool = dbo.Active(@active);

			SELECT * 
			FROM [dbo].[ComSvcSla]
			WHERE
				(@comSvc = 0 or isnull(comSvc, 0)= @comSvc) and
				(@bool <> 0 or isnull(active, 0) <> @bool)
			ORDER BY  [active] desc
		GO


	--------------------------------------------------
	-- All Store Procedure incident
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspIncidentIns(
			@name VARCHAR(80),
			@subject VARCHAR(20),
			@origin INT,
			@state INT,
			@comId INT,
			@offId INT,
			@peoId INT,
			@serId INT,
			@urgId INT,
			@impId INT,
			@priId INT,
			@scale INT,
			@scaleType BIT,
			@expert INT
		)
		AS
			INSERT INTO [dbo].[Incident] (name, subject, origin, state, comId, offId, peoId, serId, urgId, impId, priId, scale, scaleType, expert) VALUES
				(@name, @subject, @origin, @state, @comId, @offId, @peoId, @serId, @urgId, @impId, @priId, @scale, @scaleType, @expert)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO


		CREATE PROCEDURE uspIncidentUpd(
			@id INT,
			@subject VARCHAR(200),
			@state INT,
			@urgId INT,
			@impId INT,
			@priId INT,
			@scale INT,
			@scaleType BIT,
			@expert INT
		)
		AS
			UPDATE [dbo].[Incident] SET
				subject = @subject,
				state = @state,
				urgId = @urgId,
				impId = @impId,
				priId = @priId,
				scale = @scale,
				scaleType = @scaleType,
				expert = @expert
			WHERE
				id = @id
		GO


		CREATE PROCEDURE uspIncidentDel( @id INT )
		AS
			DELETE FROM [dbo].[Incident] WHERE id = @id
		GO


		CREATE PROCEDURE uspIncidentSel( @id INT )
		AS
			SELECT	I.[id],
					I.[name],
					I.[subject],
					I.[origin],
					I.[state],
					I.[comId],
					I.[offId],
					I.[peoId],
					I.[serId],
					I.[urgId],
					I.[impId],
					I.[priId],
					I.[scale],
					I.[scaleType],
					I.[expert],
					I.[likert],
					dbo.IncidentTime(I.[id]) AS min,
					dbo.IncidentDate(I.[id]) AS date,
					dbo.IncidentSLAVal(I.[id]) AS sla  
			FROM	[dbo].[Incident] AS I
			WHERE	id = @id
		GO


		CREATE PROCEDURE uspIncidentLis
		AS
			SELECT	I.[id],
					I.[name],
					I.[subject],
					I.[origin],
					I.[state],
					I.[comId],
					I.[offId],
					I.[peoId],
					I.[serId],
					I.[urgId],
					I.[impId],
					I.[priId],
					I.[scale],
					I.[scaleType],
					I.[expert],
					I.[likert],
					dbo.IncidentTime(I.[id]) AS min,
					dbo.IncidentDate(I.[id]) AS date,
					dbo.IncidentSLAVal(I.[id]) AS sla  
			FROM	[dbo].[Incident] AS I
		GO


		/* Special */
		CREATE PROCEDURE uspIncidentUpdLikert(
			@id INT,
			@state INT,
			@likert INT
		)
		AS
			UPDATE [dbo].[Incident] SET
				likert = @likert,
				state = @state,
				expert = Null,
				scaleType = 0
			WHERE
				id = @id
		Go

		CREATE PROCEDURE uspIncidentUpdExpert(
			@id INT,
			@expert INT,
			@scale INT
		)
		AS
			UPDATE [dbo].[Incident] SET
				expert = @expert,
				scale = @scale,
				scaleType = 0
			WHERE
				id = @id
		Go

	--------------------------------------------------
	-- All Store Procedure incidentSla
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspIncidentSlaUpd(
			@id INT,
			@state BIT
		)
		AS
			UPDATE [dbo].[IncidentSla] SET
				state = @state
			WHERE
				id = @id
		GO

		CREATE PROCEDURE uspIncidentSlaSel( @id INT )
		AS
			SELECT * FROM [dbo].[IncidentSla] WHERE id = @id
		GO


		CREATE PROCEDURE uspIncidentSlaLis
		(
			@incId INT
		)
		AS
			SELECT * FROM [dbo].[IncidentSla]
			WHERE incId = @incId
		GO


	--------------------------------------------------
	-- All Store Procedure incidentExpert
	--------------------------------------------------

		/* Base */

		CREATE PROCEDURE uspIncidentExpertIns(
			@noteInt VARCHAR(MAX),
			@noteExt VARCHAR(MAX),
			@time TIME,
			@peoId INT,
			@incId INT
		)
		AS
			INSERT INTO [dbo].[IncidentExpert] (noteInt, noteExt, time, date, peoId, incId) VALUES
				(@noteInt, @noteExt, @time, GETDATE(), @peoId, @incId)

			IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
			ELSE SELECT 0
		GO

		CREATE PROCEDURE uspIncidentExpertSel( @id INT )
		AS
			SELECT * FROM [dbo].[IncidentExpert] WHERE id = @id
		GO
		
		CREATE PROCEDURE uspIncidentExpertLis
		(
			@incId INT
		)
		AS
			SELECT * FROM [dbo].[IncidentExpert]
			WHERE incId = @incId
			ORDER BY [date] asc
		GO



	--------------------------------------------------
	-- All Store Procedure people
	--------------------------------------------------
		
		/* Base */
			CREATE PROCEDURE uspPeopleIns(
				@dni VARCHAR(20),
				@name VARCHAR(100),
				@nameP VARCHAR(50),
				@nameM VARCHAR(50),
				@pass VARCHAR(100),
				@birth DATETIME,
				@email VARCHAR(100),
				@telf1 VARCHAR(20),
				@telf2 VARCHAR(20),
				@note VARCHAR(MAX),
				@comId INT,
				@offId INT,
				@jobId INT,
				@typeId INT
			)
			AS
				INSERT INTO [dbo].[People] (dni, name, nameP, nameM, pass, birth, email, telf1, telf2, note, comId, offId, jobId, typeId) VALUES
					(@dni, @name, @nameP, @nameM, @pass, @birth, @email, @telf1, @telf2, @note, @comId, @offId, @jobId, @typeId)

				IF(@@ROWCOUNT = 1) SELECT CAST(scope_identity() AS int);
				ELSE SELECT 0
			GO


			CREATE PROCEDURE uspPeopleUpd(
				@id INT,
				@dni VARCHAR(20),
				@name VARCHAR(100),
				@nameP VARCHAR(50),
				@nameM VARCHAR(50),
				@birth DATETIME,
				@email VARCHAR(100),
				@telf1 VARCHAR(20),
				@telf2 VARCHAR(20),
				@note VARCHAR(MAX),
				@comId INT,
				@offId INT,
				@jobId INT,
				@typeId INT,
				@active BIT
			)
			AS
				UPDATE [dbo].[People] SET
					dni = @dni,
					name = @name,
					nameP = @nameP,
					nameM = @nameM,
					birth = @birth,
					email = @email,
					telf1 = @telf1,
					telf2 = @telf2,
					note = @note,
					comId = @comId,
					offId = @offId,
					jobId = @jobId,
					typeId = @typeId,
					active = @active
				WHERE
					id = @id
			GO
			CREATE PROCEDURE uspPeopleDel ( @id INT )
			AS
				DELETE FROM [dbo].[people] WHERE id = @id;
			GO
			CREATE PROCEDURE uspPeopleSel ( @id INT )
			AS
				SELECT * FROM [dbo].[people] WHERE id = @id;
			GO
			CREATE PROCEDURE uspPeopleLis ( 
				@comId int,
				@offId int,
				@active BIT 
			)
			AS
				SET @comId = ISNULL(@comId, 0);
				SET @offId = ISNULL(@offId, 0);
				SET @active = ISNULL(@active, 0);

				declare @bool BIT
				set @bool = dbo.Active(@active);

				SELECT [id], [dni], [name], [nameP], [nameM], [birth], [email], [telf1], [telf2], [note], [comId], [offId], [jobId], [typeId], [active]	
				FROM [dbo].[people] T 
				WHERE 
					(@comId = 0 or isnull(comId, 0)= @comId) and
					(@offId = 0 or isnull(offId, 0)= @offId) and
					(@bool <> 0 or isnull(active, 0) <> @bool)
				ORDER BY  [active] desc, [name] asc
			GO

		/* Special */
			CREATE PROCEDURE uspPeopleAuth ( @dni VARCHAR(20) )
			AS
				SELECT * FROM [dbo].[people] WHERE dni = @dni;
			GO

			CREATE PROCEDURE uspPeopleUpdPass(
				@id INT,
				@pass VARCHAR(100)
			)
			AS
				UPDATE [dbo].[People] SET
					pass = @pass
				WHERE
					id = @id
			GO

			CREATE PROCEDURE uspPeopleUpdContact(
				@id INT,
				@telf1 VARCHAR(20),
				@telf2 VARCHAR(20),
				@note VARCHAR(200)
			)
			AS
				UPDATE [dbo].[People] SET
					telf1 = @telf1,
					telf2 = @telf2,
					note = @note
				WHERE
					id = @id
			GO





/* data */
	
	--= UbigeoType
		INSERT INTO [dbo].[ubigeoType] ([name]) VALUES ('País'),('Departamento'),('Provincia'), ('Distrito');
	--= Ubigeo
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('9589', 'Perú', null, 1);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('010000','Amazonas',1,2),
		('010100','Chachapoyas',2,3), ('010101','Chachapoyas',3,4),('010102','Asuncion',3,4),('010103','Balsas',3,4),('010104','Cheto',3,4),('010105','Chiliquin',3,4),('010106','Chuquibamba',3,4),('010107','Granada',3,4),('010108','Huancas',3,4),('010109','La Jalca',3,4),('010110','Leimebamba',3,4),('010111','Levanto',3,4),('010112','Magdalena',3,4),('010113','Mariscal Castilla',3,4),('010114','Molinopampa',3,4),('010115','Montevideo',3,4),('010116','Olleros',3,4),('010117','Quinjalca',3,4),('010118','San Francisco de Daguas',3,4),('010119','San Isidro de Maino',3,4),('010120','Soloco',3,4),('010121','Sonche',3,4),
		('010200','Bagua',2,3), ('010201','Bagua',25,4), ('010202','Aramango',25,4), ('010203','Copallin',25,4), ('010204','El Parco',25,4), ('010205','Imaza',25,4), ('010206','La Peca',25,4), 
		('010300','Bongara',2,3), ('010301','Jumbilla',32,4), ('010302','Chisquilla',32,4), ('010303','Churuja',32,4), ('010304','Corosha',32,4), ('010305','Cuispes',32,4), ('010306','Florida',32,4), ('010307','Jazán',32,4), ('010308','Recta',32,4), ('010309','San Carlos',32,4), ('010310','Shipasbamba',32,4), ('010311','Valera',32,4), ('010312','Yambrasbamba',32,4), 
		('010400','Condorcanqui',2,3), ('010401','Nieva',45,4), ('010402','El Cenepa',45,4), ('010403','Rio Santiago',45,4),
		('010500','Luya',2,3), ('010501','Lamud',49,4), ('010502','Camporredondo',49,4), ('010503','Cocabamba',49,4), ('010504','Colcamar',49,4), ('010505','Conila',49,4), ('010506','Inguilpata',49,4), ('010507','Longuita',49,4), ('010508','Lonya Chico',49,4), ('010509','Luya',49,4), ('010510','Luya Viejo',49,4), ('010511','Maria',49,4), ('010512','Ocalli',49,4), ('010513','Ocumal',49,4), ('010514','Pisuquia',49,4), ('010515','Providencia',49,4), ('010516','San Cristobal',49,4), ('010517','San Francisco del Yeso',49,4), ('010518','San Jeronimo',49,4), ('010519','San Juan de Lopecancha',49,4), ('010520','Santa Catalina',49,4), ('010521','Santo Tomas',49,4), ('010522','Tingo',49,4), ('010523','Trita',49,4), 
		('010600','Rodriguez de Mendoza',2,3), ('010601','San Nicolas',73,4), ('010602','Chirimoto',73,4), ('010603','Cochamal',73,4), ('010604','Huambo',73,4), ('010605','Limabamba',73,4), ('010606','Longar',73,4), ('010607','Mariscal Benavides',73,4), ('010608','Milpuc',73,4), ('010609','Omia',73,4), ('010610','Santa Rosa',73,4), ('010611','Totora',73,4), ('010612','Vista Alegre',73,4), 
		('010700','Utcubamba',2,3), ('010701','Bagua Grande',86,4), ('010702','Cajaruro',86,4), ('010703','Cumba',86,4), ('010704','El Milagro',86,4), ('010705','Jamalca',86,4), ('010706','Lonya Grande',86,4), ('010707','Yamon',86,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('020000','Ancash',1,2),
		('020100','Huaraz',94,3), ('020101','Huaraz',95,4), ('020102','Cochabamba',95,4), ('020103','Colcabamba',95,4), ('020104','Huanchay',95,4), ('020105','Independencia',95,4), ('020106','Jangas',95,4), ('020107','La Libertad',95,4), ('020108','Olleros',95,4), ('020109','Pampas',95,4), ('020110','Pariacoto',95,4), ('020111','Pira',95,4), ('020112','Tarica',95,4), 
		('020200','Aija',94,3), ('020201','Aija',108,4), ('020202','Coris',108,4), ('020203','Huacllan',108,4), ('020204','La Merced',108,4), ('020205','Succha',108,4), 
		('020300','Antonio Raymondi',94,3), ('020301','Llamellin',114,4), ('020302','Aczo',114,4), ('020303','Chaccho',114,4), ('020304','Chingas',114,4), ('020305','Mirgas',114,4), ('020306','San Juan de Rontoy',114,4), 
		('020400','Asuncion',94,3), ('020401','Chacas',121,4), ('020402','Acochaca',121,4), 
		('020500','Bolognesi',94,3), ('020501','Chiquian',124,4), ('020502','Abelardo Pardo Lezameta',124,4), ('020503','Antonio Raymondi',124,4), ('020504','Aquia',124,4), ('020505','Cajacay',124,4), ('020506','Canis',124,4), ('020507','Colquioc',124,4), ('020508','Huallanca',124,4), ('020509','Huasta',124,4), ('020510','Huayllacayan',124,4), ('020511','La Primavera',124,4), ('020512','Mangas',124,4), ('020513','Pacllon',124,4), ('020514','San Miguel de Corpanqui',124,4), ('020515','Ticllos',124,4), 
		('020600','Carhuaz',94,3), ('020601','Carhuaz',140,4), ('020602','Acopampa',140,4), ('020603','Amashca',140,4), ('020604','Anta',140,4), ('020605','Ataquero',140,4), ('020606','Marcara',140,4), ('020607','Pariahuanca',140,4), ('020608','San Miguel de Aco',140,4), ('020609','Shilla',140,4), ('020610','Tinco',140,4), ('020611','Yungar',140,4), 
		('020700','Carlos Fermin Fitzcarrald',94,3), ('020701','San Luis',152,4), ('020702','San Nicolas',152,4), ('020703','Yauya',152,4), 
		('020800','Casma',94,3), ('020801','Casma',156,4), ('020802','Buena Vista Alta',156,4), ('020803','Comandante Noel',156,4), ('020804','Yautan',156,4), 
		('020900','Corongo',94,3), ('020901','Corongo',161,4), ('020902','Aco',161,4), ('020903','Bambas',161,4), ('020904','Cusca',161,4), ('020905','La Pampa',161,4), ('020906','Yanac',161,4), ('020907','Yupan',161,4), 
		('021000','Huari',94,3), ('021001','Huari',169,4), ('021002','Anra',169,4), ('021003','Cajay',169,4), ('021004','Chavin de Huantar',169,4), ('021005','Huacachi',169,4), ('021006','Huacchis',169,4), ('021007','Huachis',169,4), ('021008','Huantar',169,4), ('021009','Masin',169,4), ('021010','Paucas',169,4), ('021011','Ponto',169,4), ('021012','Rahuapampa',169,4), ('021013','Rapayan',169,4), ('021014','San Marcos',169,4), ('021015','San Pedro de Chana',169,4), ('021016','Uco',169,4), 
		('021100','Huarmey',94,3), ('021101','Huarmey',186,4), ('021102','Cochapeti',186,4), ('021103','Culebras',186,4), ('021104','Huayan',186,4), ('021105','Malvas',186,4), 
		('021200','Huaylas',94,3), ('021201','Caraz',192,4), ('021202','Huallanca',192,4), ('021203','Huata',192,4), ('021204','Huaylas',192,4), ('021205','Mato',192,4), ('021206','Pamparomas',192,4), ('021207','Pueblo Libre',192,4), ('021208','Santa Cruz',192,4), ('021209','Santo Toribio',192,4), ('021210','Yuracmarca',192,4), 
		('021300','Mariscal Luzuriaga',94,3), ('021301','Piscobamba',203,4), ('021302','Casca',203,4), ('021303','Eleazar Guzman Barron',203,4), ('021304','Fidel Olivas Escudero',203,4), ('021305','Llama',203,4), ('021306','Llumpa',203,4), ('021307','Lucma',203,4), ('021308','Musga',203,4), 
		('021400','Ocros',94,3), ('021401','Ocros',212,4), ('021402','Acas',212,4), ('021403','Cajamarquilla',212,4), ('021404','Carhuapampa',212,4), ('021405','Cochas',212,4), ('021406','Congas',212,4), ('021407','Llipa',212,4), ('021408','San Cristobal de Rajan',212,4), ('021409','San Pedro',212,4), ('021410','Santiago de Chilcas',212,4), 
		('021500','Pallasca',94,3), ('021501','Cabana',223,4), ('021502','Bolognesi',223,4), ('021503','Conchucos',223,4), ('021504','Huacaschuque',223,4), ('021505','Huandoval',223,4), ('021506','Lacabamba',223,4), ('021507','Llapo',223,4), ('021508','Pallasca',223,4), ('021509','Pampas',223,4), ('021510','Santa Rosa',223,4), ('021511','Tauca',223,4), 
		('021600','Pomabamba',94,3), ('021601','Pomabamba',235,4), ('021602','Huayllan',235,4), ('021603','Parobamba',235,4), ('021604','Quinuabamba',235,4), 
		('021700','Recuay',94,3), ('021701','Recuay',240,4), ('021702','Catac',240,4), ('021703','Cotaparaco',240,4), ('021704','Huayllapampa',240,4), ('021705','Llacllin',240,4), ('021706','Marca',240,4), ('021707','Pampas Chico',240,4), ('021708','Pararin',240,4), ('021709','Tapacocha',240,4), ('021710','Ticapampa',240,4), 
		('021800','Santa',94,3), ('021801','Chimbote',251,4), ('021802','Caceres del Peru',251,4), ('021803','Coishco',251,4), ('021804','Macate',251,4), ('021805','Moro',251,4), ('021806','Nepeña',251,4), ('021807','Samanco',251,4), ('021808','Santa',251,4), ('021809','Nuevo Chimbote',251,4), 
		('021900','Sihuas',94,3), ('021901','Sihuas',261,4), ('021902','Acobamba',261,4), ('021903','Alfonso Ugarte',261,4), ('021904','Cashapampa',261,4), ('021905','Chingalpo',261,4), ('021906','Huayllabamba',261,4), ('021907','Quiches',261,4), ('021908','Ragash',261,4), ('021909','San Juan',261,4), ('021910','Sicsibamba',261,4), 
		('022000','Yungay',94,3), ('022001','Yungay',272,4), ('022002','Cascapara',272,4), ('022003','Mancos',272,4), ('022004','Matacoto',272,4), ('022005','Quillo',272,4), ('022006','Ranrahirca',272,4), ('022007','Shupluy',272,4), ('022008','Yanama',272,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('030000','Apurimac',1,2),
		('030100','Abancay',94,3), ('030101','Abancay',282,4), ('030102','Chacoche',282,4), ('030103','Circa',282,4), ('030104','Curahuasi',282,4), ('030105','Huanipaca',282,4), ('030106','Lambrama',282,4), ('030107','Pichirhua',282,4), ('030108','San Pedro de Cachora',282,4), ('030109','Tamburco',282,4), 	
		('030200','Andahuaylas',94,3), ('030201','Andahuaylas',292,4), ('030202','Andarapa',292,4), ('030203','Chiara',292,4), ('030204','Huancarama',292,4), ('030205','Huancaray',292,4), ('030206','Huayana',292,4), ('030207','Kishuara',292,4), ('030208','Pacobamba',292,4), ('030209','Pacucha',292,4), ('030210','Pampachiri',292,4), ('030211','Pomacocha',292,4), ('030212','San Antonio de Cachi',292,4), ('030213','San Jeronimo',292,4), ('030214','San Miguel de Chaccrampa',292,4), ('030215','Santa Maria de Chicmo',292,4), ('030216','Talavera',292,4), ('030217','Tumay Huaraca',292,4), ('030218','Turpo',292,4), ('030219','Kaquiabamba',292,4), 
		('030300','Antabamba',94,3), ('030301','Antabamba',312,4), ('030302','El Oro',312,4), ('030303','Huaquirca',312,4), ('030304','Juan Espinoza Medrano',312,4), ('030305','Oropesa',312,4), ('030306','Pachaconas',312,4), ('030307','Sabaino',312,4), 
		('030400','Aymaraes',94,3), ('030401','Chalhuanca',320,4), ('030402','Capaya',320,4), ('030403','Caraybamba',320,4), ('030404','Chapimarca',320,4), ('030405','Colcabamba',320,4), ('030406','Cotaruse',320,4), ('030407','Huayllo',320,4), ('030408','Justo Apu Sahuaraura',320,4), ('030409','Lucre',320,4), ('030410','Pocohuanca',320,4), ('030411','San Juan de Chacña',320,4), ('030412','Sañayca',320,4), ('030413','Soraya',320,4), ('030414','Tapairihua',320,4), ('030415','Tintay',320,4), ('030416','Toraya',320,4), ('030417','Yanaca',320,4), 
		('030500','Cotabambas',94,3), ('030501','Tambobamba',338,4), ('030502','Cotabambas',338,4), ('030503','Coyllurqui',338,4), ('030504','Haquira',338,4), ('030505','Mara',338,4), ('030506','Challhuahuacho',338,4), 
		('030600','Chincheros',94,3), ('030601','Chincheros',345,4), ('030602','Anco-Huallo',345,4), ('030603','Cocharcas',345,4), ('030604','Huaccana',345,4), ('030605','Ocobamba',345,4), ('030606','Ongoy',345,4), ('030607','Uranmarca',345,4), ('030608','Ranracancha',345,4), 
		('030700','Grau',94,3), ('030701','Chuquibambilla',354,4), ('030702','Curpahuasi',354,4), ('030703','Gamarra',354,4), ('030704','Huayllati',354,4), ('030705','Mamara',354,4), ('030706','Micaela Bastidas',354,4), ('030707','Pataypampa',354,4), ('030708','Progreso',354,4), ('030709','San Antonio',354,4), ('030710','Santa Rosa',354,4), ('030711','Turpay',354,4), ('030712','Vilcabamba',354,4), ('030713','Virundo',354,4), ('030714','Curasco',354,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('040000','Arequipa',1,2),		
		('040100','Arequipa',369,3), ('040101','Arequipa',370,4), ('040102','Alto Selva Alegre',370,4), ('040103','Cayma',370,4), ('040104','Cerro Colorado',370,4), ('040105','Characato',370,4), ('040106','Chiguata',370,4), ('040107','Jacobo Hunter',370,4), ('040108','La Joya',370,4), ('040109','Mariano Melgar',370,4), ('040110','Miraflores',370,4), ('040111','Mollebaya',370,4), ('040112','Paucarpata',370,4), ('040113','Pocsi',370,4), ('040114','Polobaya',370,4), ('040115','Quequeña',370,4), ('040116','Sabandia',370,4), ('040117','Sachaca',370,4), ('040118','San Juan de Siguas',370,4), ('040119','San Juan de Tarucani',370,4), ('040120','Santa Isabel de Siguas',370,4), ('040121','Santa Rita de Siguas',370,4), ('040122','Socabaya',370,4), ('040123','Tiabaya',370,4), ('040124','Uchumayo',370,4), ('040125','Vitor',370,4), ('040126','Yanahuara',370,4), ('040127','Yarabamba',370,4), ('040128','Yura',370,4), ('040129','Jose Luis Bustamante y Rivero',370,4), 
		('040200','Camana',369,3), ('040201','Camana',400,4), ('040202','Jose Maria Quimper',400,4), ('040203','Mariano Nicolas Valcarcel',400,4), ('040204','Mariscal Caceres',400,4), ('040205','Nicolas de Pierola',400,4), ('040206','Ocoña',400,4), ('040207','Quilca',400,4), ('040208','Samuel Pastor',400,4), 
		('040300','Caraveli',369,3), ('040301','Caraveli',409,4), ('040302','Acari',409,4), ('040303','Atico',409,4), ('040304','Atiquipa',409,4), ('040305','Bella Union',409,4), ('040306','Cahuacho',409,4), ('040307','Chala',409,4), ('040308','Chaparra',409,4), ('040309','Huanuhuanu',409,4), ('040310','Jaqui',409,4), ('040311','Lomas',409,4), ('040312','Quicacha',409,4), ('040313','Yauca',409,4), 
		('040400','Castilla',369,3), ('040401','Aplao',423,4), ('040402','Andagua',423,4), ('040403','Ayo',423,4), ('040404','Chachas',423,4), ('040405','Chilcaymarca',423,4), ('040406','Choco',423,4), ('040407','Huancarqui',423,4), ('040408','Machaguay',423,4), ('040409','Orcopampa',423,4), ('040410','Pampacolca',423,4), ('040411','Tipan',423,4), ('040412','Uñon',423,4), ('040413','Uraca',423,4), ('040414','Viraco',423,4), 
		('040500','Caylloma',369,3), ('040501','Chivay',438,4), ('040502','Achoma',438,4), ('040503','Cabanaconde',438,4), ('040504','Callalli',438,4), ('040505','Caylloma',438,4), ('040506','Coporaque',438,4), ('040507','Huambo',438,4), ('040508','Huanca',438,4), ('040509','Ichupampa',438,4), ('040510','Lari',438,4), ('040511','Lluta',438,4), ('040512','Maca',438,4), ('040513','Madrigal',438,4), ('040514','San Antonio de Chuca',438,4), ('040515','Sibayo',438,4), ('040516','Tapay',438,4), ('040517','Tisco',438,4), ('040518','Tuti',438,4), ('040519','Yanque',438,4), ('040520','Majes',438,4), 
		('040600','Condesuyos',369,3), ('040601','Chuquibamba',459,4), ('040602','Andaray',459,4), ('040603','Cayarani',459,4), ('040604','Chichas',459,4), ('040605','Iray',459,4), ('040606','Rio Grande',459,4), ('040607','Salamanca',459,4), ('040608','Yanaquihua',459,4), 
		('040700','Islay',369,3), ('040701','Mollendo',468,4), ('040702','Cocachacra',468,4), ('040703','Dean Valdivia',468,4), ('040704','Islay',468,4), ('040705','Mejia',468,4), ('040706','Punta de Bombon',468,4), 
		('040800','La Union',369,3), ('040801','Cotahuasi',475,4), ('040802','Alca',475,4), ('040803','Charcana',475,4), ('040804','Huaynacotas',475,4), ('040805','Pampamarca',475,4), ('040806','Puyca',475,4), ('040807','Quechualla',475,4), ('040808','Sayla',475,4), ('040809','Tauria',475,4), ('040810','Tomepampa',475,4), ('040811','Toro',475,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('050000','Ayacucho',1,2),
		('050100','Huamanga',487,3), ('050101','Ayacucho',488,4),  ('050102','Acocro',488,4),  ('050103','Acos Vinchos',488,4),  ('050104','Carmen Alto',488,4),  ('050105','Chiara',488,4),  ('050106','Ocros',488,4),  ('050107','Pacaycasa',488,4),  ('050108','Quinua',488,4),  ('050109','San Jose de Ticllas',488,4),  ('050110','San Juan Bautista',488,4),  ('050111','Santiago de Pischa',488,4), ('050112','Socos',488,4), ('050113','Tambillo',488,4), ('050114','Vinchos',488,4), ('050115','Jesús Nazareno',488,4), ('050116','Andrés Avelino Cáceres Dorregay',488,4), 
		('050200','Cangallo',487,3), ('050201','Cangallo',505,4), ('050202','Chuschi',505,4), ('050203','Los Morochucos',505,4), ('050204','Maria Parado de Bellido',505,4), ('050205','Paras',505,4), ('050206','Totos',505,4),
		('050300','Huanca Sancos',487,3), ('050301','Sancos',512,4), ('050302','Carapo',512,4), ('050303','Sacsamarca',512,4), ('050304','Santiago de Lucanamarca',512,4),
		('050400','Huanta',487,3), ('050401','Huanta',517,4), ('050402','Ayahuanco',517,4), ('050403','Huamanguilla',517,4), ('050404','Iguain',517,4), ('050405','Luricocha',517,4), ('050406','Santillana',517,4), ('050407','Sivia',517,4), ('050408','Llochegua',517,4), ('050409','Canayre',517,4), ('050410','Uchuraccay',517,4), ('050411','Pucacolpa',517,4),
		('050500','La Mar',487,3), ('050501','San Miguel',529,4), ('050502','Anco',529,4), ('050503','Ayna',529,4), ('050504','Chilcas',529,4), ('050505','Chungui',529,4), ('050506','Luis Carranza',529,4), ('050507','Santa Rosa',529,4), ('050508','Tambo',529,4), ('050509','Samugari',529,4), ('050510','Anchihuay',529,4),
		('050600','Lucanas',487,3), ('050601','Puquio',540,4), ('050602','Aucara',540,4), ('050603','Cabana',540,4), ('050604','Carmen Salcedo',540,4), ('050605','Chaviña',540,4), ('050606','Chipao',540,4), ('050607','Huac-Huas',540,4), ('050608','Laramate',540,4), ('050609','Leoncio Prado',540,4), ('050610','Llauta',540,4), ('050611','Lucanas',540,4), ('050612','Ocaña',540,4), ('050613','Otoca',540,4), ('050614','Saisa',540,4), ('050615','San Cristobal',540,4), ('050616','San Juan',540,4), ('050617','San Pedro',540,4), ('050618','San Pedro de Palco',540,4), ('050619','Sancos',540,4), ('050620','Santa Ana de Huaycahuacho',540,4), ('050621','Santa Lucia',540,4),
		('050700','Parinacochas',487,3), ('050701','Coracora',562,4), ('050702','Chumpi',562,4), ('050703','Coronel Castañeda',562,4), ('050704','Pacapausa',562,4), ('050705','Pullo',562,4), ('050706','Puyusca',562,4), ('050707','San Francisco de Ravacayco',562,4), ('050708','Upahuacho',562,4),
		('050800','Paucar del Sara Sara',487,3), ('050801','Pausa',571,4), ('050802','Colta',571,4), ('050803','Corculla',571,4), ('050804','Lampa',571,4), ('050805','Marcabamba',571,4), ('050806','Oyolo',571,4), ('050807','Pararca',571,4), ('050808','San Javier de Alpabamba',571,4), ('050809','San Jose de Ushua',571,4), ('050810','Sara Sara',571,4),
		('050900','Sucre',487,3), ('050901','Querobamba',582,4), ('050902','Belen',582,4), ('050903','Chalcos',582,4), ('050904','Chilcayoc',582,4), ('050905','Huacaña',582,4), ('050906','Morcolla',582,4), ('050907','Paico',582,4), ('050908','San Pedro de Larcay',582,4), ('050909','San Salvador de Quije',582,4), ('050910','Santiago de Paucaray',582,4), ('050911','Soras',582,4),
		('051000','Victor Fajardo',487,3), ('051001','Huancapi',594,4), ('051002','Alcamenca',594,4), ('051003','Apongo',594,4), ('051004','Asquipata',594,4), ('051005','Canaria',594,4), ('051006','Cayara',594,4), ('051007','Colca',594,4), ('051008','Huamanquiquia',594,4), ('051009','Huancaraylla',594,4), ('051010','Huaya',594,4), ('051011','Sarhua',594,4), ('051012','Vilcanchos',594,4),
		('051100','Vilcas Huaman',487,3), ('051101','Vilcas Huaman',607,4), ('051102','Accomarca',607,4), ('051103','Carhuanca',607,4), ('051104','Concepcion',607,4), ('051105','Huambalpa',607,4), ('051106','Independencia',607,4), ('051107','Saurama',607,4), ('051108','Vischongo',607,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('060000','Cajamarca',1,2),
		('060100','Cajamarca',616,3), ('060101','Cajamarca',617,4), ('060102','Asuncion',617,4), ('060103','Chetilla',617,4), ('060104','Cospan',617,4), ('060105','Encañada',617,4), ('060106','Jesus',617,4), ('060107','Llacanora',617,4), ('060108','Los Baños del Inca',617,4), ('060109','Magdalena',617,4), ('060110','Matara',617,4), ('060111','Namora',617,4), ('060112','San Juan',617,4),
		('060200','Cajabamba',616,3), ('060201','Cajabamba',630,4), ('060202','Cachachi',630,4), ('060203','Condebamba',630,4), ('060204','Sitacocha',630,4),
		('060300','Celendin',616,3), ('060301','Celendin',635,4), ('060302','Chumuch',635,4), ('060303','Cortegana',635,4), ('060304','Huasmin',635,4), ('060305','Jorge Chavez',635,4), ('060306','Jose Galvez',635,4), ('060307','Miguel Iglesias',635,4), ('060308','Oxamarca',635,4), ('060309','Sorochuco',635,4), ('060310','Sucre',635,4), ('060311','Utco',635,4), ('060312','La Libertad de Pallan',635,4),
		('060400','Chota',616,3), ('060401','Chota',648,4), ('060402','Anguia',648,4), ('060403','Chadin',648,4), ('060404','Chiguirip',648,4), ('060405','Chimban',648,4), ('060406','Choropampa',648,4), ('060407','Cochabamba',648,4), ('060408','Conchan',648,4), ('060409','Huambos',648,4), ('060410','Lajas',648,4), ('060411','Llama',648,4), ('060412','Miracosta',648,4), ('060413','Paccha',648,4), ('060414','Pion',648,4), ('060415','Querocoto',648,4), ('060416','San Juan de Licupis',648,4), ('060417','Tacabamba',648,4), ('060418','Tocmoche',648,4), ('060419','Chalamarca',648,4),
		('060500','Contumaza',616,3), ('060501','Contumaza',668,4), ('060502','Chilete',668,4), ('060503','Cupisnique',668,4), ('060504','Guzmango',668,4), ('060505','San Benito',668,4), ('060506','Santa Cruz de Toled',668,4), ('060507','Tantarica',668,4), ('060508','Yonan',668,4),
		('060600','Cutervo',616,3), ('060601','Cutervo',677,4), ('060602','Callayuc',677,4), ('060603','Choros',677,4), ('060604','Cujillo',677,4), ('060605','La Ramada',677,4), ('060606','Pimpingos',677,4), ('060607','Querocotillo',677,4), ('060608','San Andres de Cutervo',677,4), ('060609','San Juan de Cutervo',677,4), ('060610','San Luis de Lucma',677,4), ('060611','Santa Cruz',677,4), ('060612','Santo Domingo de la Capilla',677,4), ('060613','Santo Tomas',677,4), ('060614','Socota',677,4), ('060615','Toribio Casanova',677,4),
		('060700','Hualgayoc',616,3), ('060701','Bambamarca',693,4), ('060702','Chugur',693,4), ('060703','Hualgayoc',693,4),
		('060800','Jaen',616,3), ('060801','Jaen',697,4), ('060802','Bellavista',697,4), ('060803','Chontali',697,4), ('060804','Colasay',697,4), ('060805','Huabal',697,4), ('060806','Las Pirias',697,4), ('060807','Pomahuaca',697,4), ('060808','Pucara',697,4), ('060809','Sallique',697,4), ('060810','San Felipe',697,4), ('060811','San Jose del Alto',697,4), ('060812','Santa Rosa',697,4),
		('060900','San Ignacio',616,3), ('060901','San Ignacio',710,4), ('060902','Chirinos',710,4), ('060903','Huarango',710,4), ('060904','La Coipa',710,4), ('060905','Namballe',710,4), ('060906','San Jose de Lourdes',710,4), ('060907','Tabaconas',710,4),
		('061000','San Marcos',616,3), ('061001','Pedro Galvez',718,4), ('061002','Chancay',718,4), ('061003','Eduardo Villanueva',718,4), ('061004','Gregorio Pita',718,4), ('061005','Ichocan',718,4), ('061006','Jose Manuel Quiroz',718,4), ('061007','Jose Sabogal',718,4),
		('061100','San Miguel',616,3), ('061101','San Miguel',726,4), ('061102','Bolivar',726,4), ('061103','Calquis',726,4), ('061104','Catilluc',726,4), ('061105','El Prado',726,4), ('061106','La Florida',726,4), ('061107','Llapa',726,4), ('061108','Nanchoc',726,4), ('061109','Niepos',726,4), ('061110','San Gregorio',726,4), ('061111','San Silvestre de Cochan',726,4), ('061112','Tongod',726,4), ('061113','Union Agua Blanca',726,4),
		('061200','San Pablo',616,3), ('061201','San Pablo',740,4), ('061202','San Bernardino',740,4), ('061203','San Luis',740,4), ('061204','Tumbaden',740,4),
		('061300','Santa Cruz',616,3), ('061301','Santa Cruz',745,4), ('061302','Andabamba',745,4), ('061303','Catache',745,4), ('061304','Chancaybaños',745,4), ('061305','La Esperanza',745,4), ('061306','Ninabamba',745,4), ('061307','Pulan',745,4), ('061308','Saucepampa',745,4), ('061309','Sexi',745,4), ('061310','Uticyacu',745,4), ('061311','Yauyucan',745,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('070000','Callao',1,2),
		('070100','Prov. Const. del Callao',757,3), ('070101','Callao',758,4), ('070102','Bellavista',758,4), ('070103','Carmen de la Legua Reynoso',758,4), ('070104','La Perla',758,4), ('070105','La Punta',758,4), ('070106','Ventanilla',758,4), ('070107','Mi Perú',758,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('080000','Cusco',1,2),
		('080100','Cusco',766,3), ('080101','Cusco',767,4), ('080102','Ccorca',767,4), ('080103','Poroy',767,4), ('080104','San Jeronimo',767,4), ('080105','San Sebastian',767,4), ('080106','Santiago',767,4), ('080107','Saylla',767,4), ('080108','Wanchaq',767,4),
		('080200','Acomayo',766,3), ('080201','Acomayo',776,4), ('080202','Acopia',776,4), ('080203','Acos',776,4), ('080204','Mosoc Llacta',776,4), ('080205','Pomacanchi',776,4), ('080206','Rondocan',776,4), ('080207','Sangarara',776,4),
		('080300','Anta',766,3), ('080301','Anta',784,4), ('080302','Ancahuasi',784,4), ('080303','Cachimayo',784,4), ('080304','Chinchaypujio',784,4), ('080305','Huarocondo',784,4), ('080306','Limatambo',784,4), ('080307','Mollepata',784,4), ('080308','Pucyura',784,4), ('080309','Zurite',784,4),
		('080400','Calca',766,3), ('080401','Calca',794,4), ('080402','Coya',794,4), ('080403','Lamay',794,4), ('080404','Lares',794,4), ('080405','Pisac',794,4), ('080406','San Salvador',794,4), ('080407','Taray',794,4), ('080408','Yanatile',794,4),
		('080500','Canas',766,3), ('080501','Yanaoca',803,4), ('080502','Checca',803,4), ('080503','Kunturkanki',803,4), ('080504','Langui',803,4), ('080505','Layo',803,4), ('080506','Pampamarca',803,4), ('080507','Quehue',803,4), ('080508','Tupac Amaru',803,4),
		('080600','Canchis',766,3), ('080601','Sicuani',812,4), ('080602','Checacupe',812,4), ('080603','Combapata',812,4), ('080604','Marangani',812,4), ('080605','Pitumarca',812,4), ('080606','San Pablo',812,4), ('080607','San Pedro',812,4), ('080608','Tinta',812,4),
		('080700','Chumbivilcas',766,3), ('080701','Santo Tomas',821,4), ('080702','Capacmarca',821,4), ('080703','Chamaca',821,4), ('080704','Colquemarca',821,4), ('080705','Livitaca',821,4), ('080706','Llusco',821,4), ('080707','Quiñota',821,4), ('080708','Velille',821,4),
		('080800','Espinar',766,3), ('080801','Espinar',830,4), ('080802','Condoroma',830,4), ('080803','Coporaque',830,4), ('080804','Ocoruro',830,4), ('080805','Pallpata',830,4), ('080806','Pichigua',830,4), ('080807','Suyckutambo',830,4), ('080808','Alto Pichigua',830,4),
		('080900','La Convencion',766,3), ('080901','Santa Ana',839,4), ('080902','Echarate',839,4), ('080903','Huayopata',839,4), ('080904','Maranura',839,4), ('080905','Ocobamba',839,4), ('080906','Quellouno',839,4), ('080907','Kimbiri',839,4), ('080908','Santa Teresa',839,4), ('080909','Vilcabamba',839,4), ('080910','Pichari',839,4), ('080911','Inkawasi',839,4), ('080912','Villa Virgen',839,4),
		('081000','Paruro',766,3), ('081001','Paruro',852,4), ('081002','Accha',852,4), ('081003','Ccapi',852,4), ('081004','Colcha',852,4), ('081005','Huanoquite',852,4), ('081006','Omacha',852,4), ('081007','Paccaritambo',852,4), ('081008','Pillpinto',852,4), ('081009','Yaurisque',852,4),
		('081100','Paucartambo',766,3), ('081101','Paucartambo',862,4), ('081102','Caicay',862,4), ('081103','Challabamba',862,4), ('081104','Colquepata',862,4), ('081105','Huancarani',862,4), ('081106','Kosñipata',862,4),
		('081200','Quispicanchi',766,3), ('081201','Urcos',869,4), ('081202','Andahuaylillas',869,4), ('081203','Camanti',869,4), ('081204','Ccarhuayo',869,4), ('081205','Ccatca',869,4), ('081206','Cusipata',869,4), ('081207','Huaro',869,4), ('081208','Lucre',869,4), ('081209','Marcapata',869,4), ('081210','Ocongate',869,4), ('081211','Oropesa',869,4), ('081212','Quiquijana',869,4),
		('081300','Urubamba',766,3), ('081301','Urubamba',882,4), ('081302','Chinchero',882,4), ('081303','Huayllabamba',882,4), ('081304','Machupicchu',882,4), ('081305','Maras',882,4), ('081306','Ollantaytambo',882,4), ('081307','Yucay',882,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('090000','Huancavelica',1,2),
		('090100','Huancavelica',890,3), ('090101','Huancavelica',891,4), ('090102','Acobambilla',891,4), ('090103','Acoria',891,4), ('090104','Conayca',891,4), ('090105','Cuenca',891,4), ('090106','Huachocolpa',891,4), ('090107','Huayllahuara',891,4), ('090108','Izcuchaca',891,4), ('090109','Laria',891,4), ('090110','Manta',891,4), ('090111','Mariscal Caceres',891,4), ('090112','Moya',891,4), ('090113','Nuevo Occoro',891,4), ('090114','Palca',891,4), ('090115','Pilchaca',891,4), ('090116','Vilca',891,4), ('090117','Yauli',891,4), ('090118','Ascensión',891,4), ('090119','Huando',891,4),
		('090200','Acobamba',890,3), ('090201','Acobamba',911,4), ('090202','Andabamba',911,4), ('090203','Anta',911,4), ('090204','Caja',911,4), ('090205','Marcas',911,4), ('090206','Paucara',911,4), ('090207','Pomacocha',911,4), ('090208','Rosario',911,4),
		('090300','Angaraes',890,3), ('090301','Lircay',920,4), ('090302','Anchonga',920,4), ('090303','Callanmarca',920,4), ('090304','Ccochaccasa',920,4), ('090305','Chincho',920,4), ('090306','Congalla',920,4), ('090307','Huanca-Huanca',920,4), ('090308','Huayllay Grande',920,4), ('090309','Julcamarca',920,4), ('090310','San Antonio de Antaparco',920,4), ('090311','Santo Tomas de Pata',920,4), ('090312','Secclla',920,4),
		('090400','Castrovirreyna',890,3), ('090401','Castrovirreyna',933,4), ('090402','Arma',933,4), ('090403','Aurahua',933,4), ('090404','Capillas',933,4), ('090405','Chupamarca',933,4), ('090406','Cocas',933,4), ('090407','Huachos',933,4), ('090408','Huamatambo',933,4), ('090409','Mollepampa',933,4), ('090410','San Juan',933,4), ('090411','Santa Ana',933,4), ('090412','Tantara',933,4), ('090413','Ticrapo',933,4),
		('090500','Churcampa',890,3), ('090501','Churcampa',947,4), ('090502','Anco',947,4), ('090503','Chinchihuasi',947,4), ('090504','El Carmen',947,4), ('090505','La Merced',947,4), ('090506','Locroja',947,4), ('090507','Paucarbamba',947,4), ('090508','San Miguel de Mayocc',947,4), ('090509','San Pedro de Coris',947,4), ('090510','Pachamarca',947,4), ('090511','Cosme',947,4),
		('090600','Huaytara',890,3), ('090601','Huaytara',959,4), ('090602','Ayavi',959,4), ('090603','Cordova',959,4), ('090604','Huayacundo Arma',959,4), ('090605','Laramarca',959,4), ('090606','Ocoyo',959,4), ('090607','Pilpichaca',959,4), ('090608','Querco',959,4), ('090609','Quito-Arma',959,4), ('090610','San Antonio de Cusicancha',959,4), ('090611','San Francisco de Sangayaico',959,4), ('090612','San Isidro',959,4), ('090613','Santiago de Chocorvos',959,4), ('090614','Santiago de Quirahuara',959,4), ('090615','Santo Domingo de Capillas',959,4), ('090616','Tambo',959,4),
		('090700','Tayacaja',890,3), ('090701','Pampas',976,4), ('090702','Acostambo',976,4), ('090703','Acraquia',976,4), ('090704','Ahuaycha',976,4), ('090705','Colcabamba',976,4), ('090706','Daniel Hernandez',976,4), ('090707','Huachocolpa',976,4), ('090709','Huaribamba',976,4), ('090710','Ñahuimpuquio',976,4), ('090711','Pazos',976,4), ('090713','Quishuar',976,4), ('090714','Salcabamba',976,4), ('090715','Salcahuasi',976,4), ('090716','San Marcos de Rocchac',976,4), ('090717','Surcubamba',976,4), ('090718','Tintay Puncu',976,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('100000','Huanuco',1,2),
		('100100','Huanuco',993,3), ('100101','Huanuco',994,4), ('100102','Amarilis',994,4), ('100103','Chinchao',994,4), ('100104','Churubamba',994,4), ('100105','Margos',994,4), ('100106','Quisqui',994,4), ('100107','San Francisco de Cayran',994,4), ('100108','San Pedro de Chaulan',994,4), ('100109','Santa Maria del Valle',994,4), ('100110','Yarumayo',994,4), ('100111','Pillco Marca',994,4), ('100112','Yacus',994,4),
		('100200','Ambo',993,3), ('100201','Ambo',1007,4), ('100202','Cayna',1007,4), ('100203','Colpas',1007,4), ('100204','Conchamarca',1007,4), ('100205','Huacar',1007,4), ('100206','San Francisco',1007,4), ('100207','San Rafael',1007,4), ('100208','Tomay Kichwa',1007,4),
		('100300','Dos de Mayo',993,3), ('100301','La Union',1016,4), ('100307','Chuquis',1016,4), ('100311','Marias',1016,4), ('100313','Pachas',1016,4), ('100316','Quivilla',1016,4), ('100317','Ripan',1016,4), ('100321','Shunqui',1016,4), ('100322','Sillapata',1016,4), ('100323','Yanas',1016,4),
		('100400','Huacaybamba',993,3), ('100401','Huacaybamba',1026,4), ('100402','Canchabamba',1026,4), ('100403','Cochabamba',1026,4), ('100404','Pinra',1026,4),
		('100500','Huamalies',993,3), ('100501','Llata',1031,4), ('100502','Arancay',1031,4), ('100503','Chavin de Pariarca',1031,4), ('100504','Jacas Grande',1031,4), ('100505','Jircan',1031,4), ('100506','Miraflores',1031,4), ('100507','Monzon',1031,4), ('100508','Punchao',1031,4), ('100509','Puños',1031,4), ('100510','Singa',1031,4), ('100511','Tantamayo',1031,4),
		('100600','Leoncio Prado',993,3), ('100601','Rupa-Rupa',1043,4), ('100602','Daniel Alomias Robles',1043,4), ('100603','Hermilio Valdizan',1043,4), ('100604','Jose Crespo y Castillo',1043,4), ('100605','Luyando',1043,4), ('100606','Mariano Damaso Beraun',1043,4),
		('100700','Marañon',993,3), ('100701','Huacrachuco',1050,4), ('100702','Cholon',1050,4), ('100703','San Buenaventura',1050,4),
		('100800','Pachitea',993,3), ('100801','Panao',1054,4), ('100802','Chaglla',1054,4), ('100803','Molino',1054,4), ('100804','Umari',1054,4),
		('100900','Puerto Inca',993,3), ('100901','Puerto Inca',1059,4), ('100902','Codo del Pozuzo',1059,4), ('100903','Honoria',1059,4), ('100904','Tournavista',1059,4), ('100905','Yuyapichis',1059,4),
		('101000','Lauricocha',993,3), ('101001','Jesus',1065,4), ('101002','Baños',1065,4), ('101003','Jivia',1065,4), ('101004','Queropalca',1065,4), ('101005','Rondos',1065,4), ('101006','San Francisco de Asis',1065,4), ('101007','San Miguel de Cauri',1065,4),
		('101100','Yarowilca',993,3), ('101101','Chavinillo',1073,4), ('101102','Cahuac',1073,4), ('101103','Chacabamba',1073,4), ('101104','Chupan',1073,4), ('101105','Jacas Chico',1073,4), ('101106','Obas',1073,4), ('101107','Pampamarca',1073,4), ('101108','Choras',1073,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('110000','Ica',1,2),
		('110100','Ica',1082,3), ('110101','Ica',1083,4), ('110102','La Tinguiña',1083,4), ('110103','Los Aquijes',1083,4), ('110104','Ocucaje',1083,4), ('110105','Pachacutec',1083,4), ('110106','Parcona',1083,4), ('110107','Pueblo Nuevo',1083,4), ('110108','Salas',1083,4), ('110109','San Jose de los Molinos',1083,4), ('110110','San Juan Bautista',1083,4), ('110111','Santiago',1083,4), ('110112','Subtanjalla',1083,4), ('110113','Tate',1083,4), ('110114','Yauca del Rosario',1083,4),
		('110200','Chincha',1082,3), ('110201','Chincha Alta',1098,4), ('110202','Alto Laran',1098,4), ('110203','Chavin',1098,4), ('110204','Chincha Baja',1098,4), ('110205','El Carmen',1098,4), ('110206','Grocio Prado',1098,4), ('110207','Pueblo Nuevo',1098,4), ('110208','San Juan de Yanac',1098,4), ('110209','San Pedro de Huacarpana',1098,4), ('110210','Sunampe',1098,4), ('110211','Tambo de Mora',1098,4),
		('110300','Nazca',1082,3), ('110301','Nazca',1110,4), ('110302','Changuillo',1110,4), ('110303','El Ingenio',1110,4), ('110304','Marcona',1110,4), ('110305','Vista Alegre',1110,4),
		('110400','Palpa',1082,3), ('110401','Palpa',1116,4), ('110402','Llipata',1116,4), ('110403','Rio Grande',1116,4), ('110404','Santa Cruz',1116,4), ('110405','Tibillo',1116,4),
		('110500','Pisco',1082,3), ('110501','Pisco',1122,4), ('110502','Huancano',1122,4), ('110503','Humay',1122,4), ('110504','Independencia',1122,4), ('110505','Paracas',1122,4), ('110506','San Andres',1122,4), ('110507','San Clemente',1122,4), ('110508','Tupac Amaru Inca',1122,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('120000','Junin',1,2),
		('120100','Huancayo',1131,3), ('120101','Huancayo',1132,4), ('120104','Carhuacallanga',1132,4), ('120105','Chacapampa',1132,4), ('120106','Chicche',1132,4), ('120107','Chilca',1132,4), ('120108','Chongos Alto',1132,4), ('120111','Chupuro',1132,4), ('120112','Colca',1132,4), ('120113','Cullhuas',1132,4), ('120114','El Tambo',1132,4), ('120116','Huacrapuquio',1132,4), ('120117','Hualhuas',1132,4), ('120119','Huancan',1132,4), ('120120','Huasicancha',1132,4), ('120121','Huayucachi',1132,4), ('120122','Ingenio',1132,4), ('120124','Pariahuanca',1132,4), ('120125','Pilcomayo',1132,4), ('120126','Pucara',1132,4), ('120127','Quichuay',1132,4), ('120128','Quilcas',1132,4), ('120129','San Agustin',1132,4), ('120130','San Jeronimo de Tunan',1132,4), ('120132','Saño',1132,4), ('120133','Sapallanga',1132,4), ('120134','Sicaya',1132,4), ('120135','Santo Domingo de Acobamba',1132,4), ('120136','Viques',1132,4),
		('120200','Concepcion',1131,3), ('120201','Concepcion',1161,4), ('120202','Aco',1161,4), ('120203','Andamarca',1161,4), ('120204','Chambara',1161,4), ('120205','Cochas',1161,4), ('120206','Comas',1161,4), ('120207','Heroinas Toledo',1161,4), ('120208','Manzanares',1161,4), ('120209','Mariscal Castilla',1161,4), ('120210','Matahuasi',1161,4), ('120211','Mito',1161,4), ('120212','Nueve de Julio',1161,4), ('120213','Orcotuna',1161,4), ('120214','San Jose de Quero',1161,4), ('120215','Santa Rosa de Ocopa',1161,4),
		('120300','Chanchamayo',1131,3), ('120301','Chanchamayo',1177,4), ('120302','Perene',1177,4), ('120303','Pichanaqui',1177,4), ('120304','San Luis de Shuaro',1177,4), ('120305','San Ramon',1177,4), ('120306','Vitoc',1177,4),
		('120400','Jauja',1131,3), ('120401','Jauja',1184,4), ('120402','Acolla',1184,4), ('120403','Apata',1184,4), ('120404','Ataura',1184,4), ('120405','Canchayllo',1184,4), ('120406','Curicaca',1184,4), ('120407','El Mantaro',1184,4), ('120408','Huamali',1184,4), ('120409','Huaripampa',1184,4), ('120410','Huertas',1184,4), ('120411','Janjaillo',1184,4), ('120412','Julcan',1184,4), ('120413','Leonor Ordoñez',1184,4), ('120414','Llocllapampa',1184,4), ('120415','Marco',1184,4), ('120416','Masma',1184,4), ('120417','Masma Chicche',1184,4), ('120418','Molinos',1184,4), ('120419','Monobamba',1184,4), ('120420','Muqui',1184,4), ('120421','Muquiyauyo',1184,4), ('120422','Paca',1184,4), ('120423','Paccha',1184,4), ('120424','Pancan',1184,4), ('120425','Parco',1184,4), ('120426','Pomacancha',1184,4), ('120427','Ricran',1184,4), ('120428','San Lorenzo',1184,4), ('120429','San Pedro de Chunan',1184,4), ('120430','Sausa',1184,4), ('120431','Sincos',1184,4), ('120432','Tunan Marca',1184,4), ('120433','Yauli',1184,4), ('120434','Yauyos',1184,4),
		('120500','Junin',1131,3), ('120501','Junin',1219,4), ('120502','Carhuamayo',1219,4), ('120503','Ondores',1219,4), ('120504','Ulcumayo',1219,4),
		('120600','Satipo',1131,3), ('120601','Satipo',1224,4), ('120602','Coviriali',1224,4), ('120603','Llaylla',1224,4), ('120604','Mazamari',1224,4), ('120605','Pampa Hermosa',1224,4), ('120606','Pangoa',1224,4), ('120607','Rio Negro',1224,4), ('120608','Rio Tambo',1224,4), ('120699','Mazamari-Pangoa',1224,4),
		('120700','Tarma',1131,3), ('120701','Tarma',1234,4), ('120702','Acobamba',1234,4), ('120703','Huaricolca',1234,4), ('120704','Huasahuasi',1234,4), ('120705','La Union',1234,4), ('120706','Palca',1234,4), ('120707','Palcamayo',1234,4), ('120708','San Pedro de Cajas',1234,4), ('120709','Tapo',1234,4),
		('120800','Yauli',1131,3), ('120801','La Oroya',1244,4), ('120802','Chacapalpa',1244,4), ('120803','Huay-Huay',1244,4), ('120804','Marcapomacocha',1244,4), ('120805','Morococha',1244,4), ('120806','Paccha',1244,4), ('120807','Santa Barbara de Carhuacayan',1244,4), ('120808','Santa Rosa de Sacco',1244,4), ('120809','Suitucancha',1244,4), ('120810','Yauli',1244,4),
		('120900','Chupaca',1131,3), ('120901','Chupaca',1255,4), ('120902','Ahuac',1255,4), ('120903','Chongos Bajo',1255,4), ('120904','Huachac',1255,4), ('120905','Huamancaca Chico',1255,4), ('120906','San Juan de Iscos',1255,4), ('120907','San Juan de Jarpa',1255,4), ('120908','3 de Diciembre',1255,4), ('120909','Yanacancha',1255,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('130000','La Libertad',1,2),
		('130100','Trujillo',1265,3), ('130101','Trujillo',1266,4), ('130102','El Porvenir',1266,4), ('130103','Florencia de Mora',1266,4), ('130104','Huanchaco',1266,4), ('130105','La Esperanza',1266,4), ('130106','Laredo',1266,4), ('130107','Moche',1266,4), ('130108','Poroto',1266,4), ('130109','Salaverry',1266,4), ('130110','Simbal',1266,4), ('130111','Victor Larco Herrera',1266,4),
		('130200','Ascope',1265,3), ('130201','Ascope',1278,4), ('130202','Chicama',1278,4), ('130203','Chocope',1278,4), ('130204','Magdalena de Cao',1278,4), ('130205','Paijan',1278,4), ('130206','Razuri',1278,4), ('130207','Santiago de Cao',1278,4), ('130208','Casa Grande',1278,4),
		('130300','Bolivar',1265,3), ('130301','Bolivar',1287,4), ('130302','Bambamarca',1287,4), ('130303','Condormarca',1287,4), ('130304','Longotea',1287,4), ('130305','Uchumarca',1287,4), ('130306','Ucuncha',1287,4),
		('130400','Chepen',1265,3), ('130401','Chepen',1294,4), ('130402','Pacanga',1294,4), ('130403','Pueblo Nuevo',1294,4),
		('130500','Julcan',1265,3), ('130501','Julcan',1298,4), ('130502','Calamarca',1298,4), ('130503','Carabamba',1298,4), ('130504','Huaso',1298,4),
		('130600','Otuzco',1265,3), ('130601','Otuzco',1303,4), ('130602','Agallpampa',1303,4), ('130604','Charat',1303,4), ('130605','Huaranchal',1303,4), ('130606','La Cuesta',1303,4), ('130608','Mache',1303,4), ('130610','Paranday',1303,4), ('130611','Salpo',1303,4), ('130613','Sinsicap',1303,4), ('130614','Usquil',1303,4),
		('130700','Pacasmayo',1265,3), ('130701','San Pedro de Lloc',1314,4), ('130702','Guadalupe',1314,4), ('130703','Jequetepeque',1314,4), ('130704','Pacasmayo',1314,4), ('130705','San Jose',1314,4),
		('130800','Pataz',1265,3), ('130801','Tayabamba',1320,4), ('130802','Buldibuyo',1320,4), ('130803','Chillia',1320,4), ('130804','Huancaspata',1320,4), ('130805','Huaylillas',1320,4), ('130806','Huayo',1320,4), ('130807','Ongon',1320,4), ('130808','Parcoy',1320,4), ('130809','Pataz',1320,4), ('130810','Pias',1320,4), ('130811','Santiago de Challas',1320,4), ('130812','Taurija',1320,4), ('130813','Urpay',1320,4),
		('130900','Sanchez Carrion',1265,3), ('130901','Huamachuco',1334,4), ('130902','Chugay',1334,4), ('130903','Cochorco',1334,4), ('130904','Curgos',1334,4), ('130905','Marcabal',1334,4), ('130906','Sanagoran',1334,4), ('130907','Sarin',1334,4), ('130908','Sartimbamba',1334,4),
		('131000','Santiago de Chuco',1265,3), ('131001','Santiago de Chuco',1343,4), ('131002','Angasmarca',1343,4), ('131003','Cachicadan',1343,4), ('131004','Mollebamba',1343,4), ('131005','Mollepata',1343,4), ('131006','Quiruvilca',1343,4), ('131007','Santa Cruz de Chuca',1343,4), ('131008','Sitabamba',1343,4),
		('131100','Gran Chimu',1265,3), ('131101','Cascas',1352,4), ('131102','Lucma',1352,4), ('131103','Marmot',1352,4), ('131104','Sayapullo',1352,4),
		('131200','Viru',1265,3), ('131201','Viru',1357,4), ('131202','Chao',1357,4), ('131203','Guadalupito',1357,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('140000','Lambayeque',1,2),
		('140100','Chiclayo',1361,3), ('140101','Chiclayo',1362,4), ('140102','Chongoyape',1362,4), ('140103','Eten',1362,4), ('140104','Eten Puerto',1362,4), ('140105','Jose Leonardo Ortiz',1362,4), ('140106','La Victoria',1362,4), ('140107','Lagunas',1362,4), ('140108','Monsefu',1362,4), ('140109','Nueva Arica',1362,4), ('140110','Oyotun',1362,4), ('140111','Picsi',1362,4), ('140112','Pimentel',1362,4), ('140113','Reque',1362,4), ('140114','Santa Rosa',1362,4), ('140115','Saña',1362,4), ('140116','Cayaltí',1362,4), ('140117','Patapo',1362,4), ('140118','Pomalca',1362,4), ('140119','Pucalá',1362,4), ('140120','Tumán',1362,4),
		('140200','Ferreñafe',1361,3), ('140201','Ferreñafe',1383,4), ('140202','Cañaris',1383,4), ('140203','Incahuasi',1383,4), ('140204','Manuel Antonio Mesones Muro',1383,4), ('140205','Pitipo',1383,4), ('140206','Pueblo Nuevo',1383,4),
		('140300','Lambayeque',1361,3), ('140301','Lambayeque',1390,4), ('140302','Chochope',1390,4), ('140303','Illimo',1390,4), ('140304','Jayanca',1390,4), ('140305','Mochumi',1390,4), ('140306','Morrope',1390,4), ('140307','Motupe',1390,4), ('140308','Olmos',1390,4), ('140309','Pacora',1390,4), ('140310','Salas',1390,4), ('140311','San Jose',1390,4), ('140312','Tucume',1390,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('150000','Lima',1,2),
		('150100','Lima',1403,3), ('150101','Lima',1404,4), ('150102','Ancon',1404,4), ('150103','Ate',1404,4), ('150104','Barranco',1404,4), ('150105','Breña',1404,4), ('150106','Carabayllo',1404,4), ('150107','Chaclacayo',1404,4), ('150108','Chorrillos',1404,4), ('150109','Cieneguilla',1404,4), ('150110','Comas',1404,4), ('150111','El Agustino',1404,4), ('150112','Independencia',1404,4), ('150113','Jesus Maria',1404,4), ('150114','La Molina',1404,4), ('150115','La Victoria',1404,4), ('150116','Lince',1404,4), ('150117','Los Olivos',1404,4), ('150118','Lurigancho',1404,4), ('150119','Lurin',1404,4), ('150120','Magdalena del Mar',1404,4), ('150121','Pueblo Libre (Magdalena Vieja)',1404,4), ('150122','Miraflores',1404,4), ('150123','Pachacamac',1404,4), ('150124','Pucusana',1404,4), ('150125','Puente Piedra',1404,4), ('150126','Punta Hermosa',1404,4), ('150127','Punta Negra',1404,4), ('150128','Rimac',1404,4), ('150129','San Bartolo',1404,4), ('150130','San Borja',1404,4), ('150131','San Isidro',1404,4), ('150132','San Juan de Lurigancho',1404,4), ('150133','San Juan de Miraflores',1404,4), ('150134','San Luis',1404,4), ('150135','San Martin de Porres',1404,4), ('150136','San Miguel',1404,4), ('150137','Santa Anita',1404,4), ('150138','Santa Maria del Mar',1404,4), ('150139','Santa Rosa',1404,4), ('150140','Santiago de Surco',1404,4), ('150141','Surquillo',1404,4), ('150142','Villa El Salvador',1404,4), ('150143','Villa Maria del Triunfo',1404,4),
		('150200','Barranca',1403,3), ('150201','Barranca',1448,4), ('150202','Paramonga',1448,4), ('150203','Pativilca',1448,4), ('150204','Supe',1448,4), ('150205','Supe Puerto',1448,4),
		('150300','Cajatambo',1403,3), ('150301','Cajatambo',1454,4), ('150302','Copa',1454,4), ('150303','Gorgor',1454,4), ('150304','Huancapon',1454,4), ('150305','Manas',1454,4),
		('150400','Canta',1403,3), ('150401','Canta',1460,4), ('150402','Arahuay',1460,4), ('150403','Huamantanga',1460,4), ('150404','Huaros',1460,4), ('150405','Lachaqui',1460,4), ('150406','San Buenaventura',1460,4), ('150407','Santa Rosa de Quives',1460,4),
		('150500','Cañete',1403,3), ('150501','San Vicente de Cañete',1468,4), ('150502','Asia',1468,4), ('150503','Calango',1468,4), ('150504','Cerro Azul',1468,4), ('150505','Chilca',1468,4), ('150506','Coayllo',1468,4), ('150507','Imperial',1468,4), ('150508','Lunahuana',1468,4), ('150509','Mala',1468,4), ('150510','Nuevo Imperial',1468,4), ('150511','Pacaran',1468,4), ('150512','Quilmana',1468,4), ('150513','San Antonio',1468,4), ('150514','San Luis',1468,4), ('150515','Santa Cruz de Flores',1468,4), ('150516','Zuñiga',1468,4),
		('150600','Huaral',1403,3), ('150601','Huaral',1485,4), ('150602','Atavillos Alto',1485,4), ('150603','Atavillos Bajo',1485,4), ('150604','Aucallama',1485,4), ('150605','Chancay',1485,4), ('150606','Ihuari',1485,4), ('150607','Lampian',1485,4), ('150608','Pacaraos',1485,4), ('150609','San Miguel de Acos',1485,4), ('150610','Santa Cruz de Andamarca',1485,4), ('150611','Sumbilca',1485,4), ('150612','Veintisiete de Noviembre',1485,4),
		('150700','Huarochiri',1403,3), ('150701','Matucana',1498,4), ('150702','Antioquia',1498,4), ('150703','Callahuanca',1498,4), ('150704','Carampoma',1498,4), ('150705','Chicla',1498,4), ('150706','Cuenca',1498,4), ('150707','Huachupampa',1498,4), ('150708','Huanza',1498,4), ('150709','Huarochiri',1498,4), ('150710','Lahuaytambo',1498,4), ('150711','Langa',1498,4), ('150712','Laraos',1498,4), ('150713','Mariatana',1498,4), ('150714','Ricardo Palma',1498,4), ('150715','San Andres de Tupicocha',1498,4), ('150716','San Antonio',1498,4), ('150717','San Bartolome',1498,4), ('150718','San Damian',1498,4), ('150719','San Juan de Iris',1498,4), ('150720','San Juan de Tantaranche',1498,4), ('150721','San Lorenzo de Quinti',1498,4), ('150722','San Mateo',1498,4), ('150723','San Mateo de Otao',1498,4), ('150724','San Pedro de Casta',1498,4), ('150725','San Pedro de Huancayre',1498,4), ('150726','Sangallaya',1498,4), ('150727','Santa Cruz de Cocachacra',1498,4), ('150728','Santa Eulalia',1498,4), ('150729','Santiago de Anchucaya',1498,4), ('150730','Santiago de Tuna',1498,4), ('150731','Santo Domingo de los Olleros',1498,4), ('150732','Surco',1498,4),
		('150800','Huaura',1403,3), ('150801','Huacho',1531,4), ('150802','Ambar',1531,4), ('150803','Caleta de Carquin',1531,4), ('150804','Checras',1531,4), ('150805','Hualmay',1531,4), ('150806','Huaura',1531,4), ('150807','Leoncio Prado',1531,4), ('150808','Paccho',1531,4), ('150809','Santa Leonor',1531,4), ('150810','Santa Maria',1531,4), ('150811','Sayan',1531,4), ('150812','Vegueta',1531,4),
		('150900','Oyon',1403,3), ('150901','Oyon',1544,4), ('150902','Andajes',1544,4), ('150903','Caujul',1544,4), ('150904','Cochamarca',1544,4), ('150905','Navan',1544,4), ('150906','Pachangara',1544,4),
		('151000','Yauyos',1403,3), ('151001','Yauyos',1551,4), ('151002','Alis',1551,4), ('151003','Ayauca',1551,4), ('151004','Ayaviri',1551,4), ('151005','Azangaro',1551,4), ('151006','Cacra',1551,4), ('151007','Carania',1551,4), ('151008','Catahuasi',1551,4), ('151009','Chocos',1551,4), ('151010','Cochas',1551,4), ('151011','Colonia',1551,4), ('151012','Hongos',1551,4), ('151013','Huampara',1551,4), ('151014','Huancaya',1551,4), ('151015','Huangascar',1551,4), ('151016','Huantan',1551,4), ('151017','Huañec',1551,4), ('151018','Laraos',1551,4), ('151019','Lincha',1551,4), ('151020','Madean',1551,4), ('151021','Miraflores',1551,4), ('151022','Omas',1551,4), ('151023','Putinza',1551,4), ('151024','Quinches',1551,4), ('151025','Quinocay',1551,4), ('151026','San Joaquin',1551,4), ('151027','San Pedro de Pilas',1551,4), ('151028','Tanta',1551,4), ('151029','Tauripampa',1551,4), ('151030','Tomas',1551,4), ('151031','Tupe',1551,4), ('151032','Viñac',1551,4), ('151033','Vitis',1551,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('160000','Loreto',1,2),
		('160100','Maynas',1585,3), ('160101','Iquitos',1586,4), ('160102','Alto Nanay',1586,4), ('160103','Fernando Lores',1586,4), ('160104','Indiana',1586,4), ('160105','Las Amazonas',1586,4), ('160106','Mazan',1586,4), ('160107','Napo',1586,4), ('160108','Punchana',1586,4), ('160109','Putumayo',1586,4), ('160110','Torres Causana',1586,4), ('160112','Belén',1586,4), ('160113','San Juan Bautista',1586,4), ('160114','Teniente Manuel Clavero',1586,4),				
		('160200','Alto Amazonas',1585,3), ('160201','Yurimaguas',1600,4), ('160202','Balsapuerto',1600,4), ('160205','Jeberos',1600,4), ('160206','Lagunas',1600,4), ('160210','Santa Cruz',1600,4), ('160211','Teniente Cesar Lopez Rojas',1600,4),				
		('160300','Loreto',1585,3), ('160301','Nauta',1607,4), ('160302','Parinari',1607,4), ('160303','Tigre',1607,4), ('160304','Trompeteros',1607,4), ('160305','Urarinas',1607,4),				
		('160400','Mariscal Ramon Castilla',1585,3), ('160401','Ramon Castilla',1613,4), ('160402','Pebas',1613,4), ('160403','Yavari',1613,4), ('160404','San Pablo',1613,4),				
		('160500','Requena',1585,3), ('160501','Requena',1618,4), ('160502','Alto Tapiche',1618,4), ('160503','Capelo',1618,4), ('160504','Emilio San Martin',1618,4), ('160505','Maquia',1618,4), ('160506','Puinahua',1618,4), ('160507','Saquena',1618,4), ('160508','Soplin',1618,4), ('160509','Tapiche',1618,4), ('160510','Jenaro Herrera',1618,4), ('160511','Yaquerana',1618,4),				
		('160600','Ucayali',1585,3), ('160601','Contamana',1630,4), ('160602','Inahuaya',1630,4), ('160603','Padre Marquez',1630,4), ('160604','Pampa Hermosa',1630,4), ('160605','Sarayacu',1630,4), ('160606','Vargas Guerra',1630,4),				
		('160700','Datem del Marañón',1585,3), ('160701','Barranca',1637,4), ('160702','Cahuapanas',1637,4), ('160703','Manseriche',1637,4), ('160704','Morona',1637,4), ('160705','Pastaza',1637,4), ('160706','Andoas',1637,4),				
		('160800','Putumayo',1585,3), ('160801','Putumayo',1644,4), ('160802','Rosa Panduro',1644,4), ('160803','Teniente Manuel Clavero',1644,4), ('160804','Yaguas',1644,4);		
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('170000','Madre de Dios',1,2),		
		('170100','Tambopata',1649,3), ('170101','Tambopata',1650,4), ('170102','Inambari',1650,4), ('170103','Las Piedras',1650,4), ('170104','Laberinto',1650,4),
		('170200','Manu',1649,3), ('170201','Manu',1655,4), ('170202','Fitzcarrald',1655,4), ('170203','Madre de Dios',1655,4), ('170204','Huepetuhe',1655,4),
		('170300','Tahuamanu',1649,3), ('170301','Iñapari',1660,4), ('170302','Iberia',1660,4), ('170303','Tahuamanu',1660,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('180000','Moquegua',1,2),
		('180100','Mariscal Nieto',1664,3), ('180101','Moquegua',1665,4), ('180102','Carumas',1665,4), ('180103','Cuchumbaya',1665,4), ('180104','Samegua',1665,4), ('180105','San Cristobal',1665,4), ('180106','Torata',1665,4),
		('180200','General Sanchez Cerro',1664,3), ('180201','Omate',1672,4), ('180202','Chojata',1672,4), ('180203','Coalaque',1672,4), ('180204','Ichuña',1672,4), ('180205','La Capilla',1672,4), ('180206','Lloque',1672,4), ('180207','Matalaque',1672,4), ('180208','Puquina',1672,4), ('180209','Quinistaquillas',1672,4), ('180210','Ubinas',1672,4), ('180211','Yunga',1672,4),
		('180300','Ilo',1664,3), ('180301','Ilo',1684,4), ('180302','El Algarrobal',1684,4), ('180303','Pacocha',1684,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('190000','Pasco',1,2),
		('190100','Pasco',1688,3), ('190101','Chaupimarca',1689,4), ('190102','Huachon',1689,4), ('190103','Huariaca',1689,4), ('190104','Huayllay',1689,4), ('190105','Ninacaca',1689,4), ('190106','Pallanchacra',1689,4), ('190107','Paucartambo',1689,4), ('190108','San Fco. de Asís de Yarusyacán',1689,4), ('190109','Simon Bolivar',1689,4), ('190110','Ticlacayan',1689,4), ('190111','Tinyahuarco',1689,4), ('190112','Vicco',1689,4), ('190113','Yanacancha',1689,4),
		('190200','Daniel Alcides Carrion',1688,3), ('190201','Yanahuanca',1703,4), ('190202','Chacayan',1703,4), ('190203','Goyllarisquizga',1703,4), ('190204','Paucar',1703,4), ('190205','San Pedro de Pillao',1703,4), ('190206','Santa Ana de Tusi',1703,4), ('190207','Tapuc',1703,4), ('190208','Vilcabamba',1703,4),
		('190300','Oxapampa',1688,3), ('190301','Oxapampa',1712,4), ('190302','Chontabamba',1712,4), ('190303','Huancabamba',1712,4), ('190304','Palcazu',1712,4), ('190305','Pozuzo',1712,4), ('190306','Puerto Bermudez',1712,4), ('190307','Villa Rica',1712,4), ('190308','Constitucion',1712,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('200000','Piura',1,2),
		('200100','Piura',1721,3), ('200101','Piura',1722,4), ('200104','Castilla',1722,4), ('200105','Catacaos',1722,4), ('200107','Cura Mori',1722,4), ('200108','El Tallan',1722,4), ('200109','La Arena',1722,4), ('200110','La Union',1722,4), ('200111','Las Lomas',1722,4), ('200114','Tambo Grande',1722,4), ('200115','Veintiséis de Octubre',1722,4),	
		('200200','Ayabaca',1721,3), ('200201','Ayabaca',1733,4), ('200202','Frias',1733,4), ('200203','Jilili',1733,4), ('200204','Lagunas',1733,4), ('200205','Montero',1733,4), ('200206','Pacaipampa',1733,4), ('200207','Paimas',1733,4), ('200208','Sapillica',1733,4), ('200209','Sicchez',1733,4), ('200210','Suyo',1733,4),	
		('200300','Huancabamba',1721,3), ('200301','Huancabamba',1744,4), ('200302','Canchaque',1744,4), ('200303','El Carmen de la Frontera',1744,4), ('200304','Huarmaca',1744,4), ('200305','Lalaquiz',1744,4), ('200306','San Miguel de El Faique',1744,4), ('200307','Sondor',1744,4), ('200308','Sondorillo',1744,4),	
		('200400','Morropon',1721,3), ('200401','Chulucanas',1753,4), ('200402','Buenos Aires',1753,4), ('200403','Chalaco',1753,4), ('200404','La Matanza',1753,4), ('200405','Morropon',1753,4), ('200406','Salitral',1753,4), ('200407','San Juan de Bigote',1753,4), ('200408','Santa Catalina de Mossa',1753,4), ('200409','Santo Domingo',1753,4), ('200410','Yamango',1753,4),	
		('200500','Paita',1721,3), ('200501','Paita',1764,4), ('200502','Amotape',1764,4), ('200503','Arenal',1764,4), ('200504','Colan',1764,4), ('200505','La Huaca',1764,4), ('200506','Tamarindo',1764,4), ('200507','Vichayal',1764,4),	
		('200600','Sullana',1721,3), ('200601','Sullana',1772,4), ('200602','Bellavista',1772,4), ('200603','Ignacio Escudero',1772,4), ('200604','Lancones',1772,4), ('200605','Marcavelica',1772,4), ('200606','Miguel Checa',1772,4), ('200607','Querecotillo',1772,4), ('200608','Salitral',1772,4),	
		('200700','Talara',1721,3), ('200701','Pariñas',1781,4), ('200702','El Alto',1781,4), ('200703','La Brea',1781,4), ('200704','Lobitos',1781,4), ('200705','Los Organos',1781,4), ('200706','Mancora',1781,4),	
		('200800','Sechura',1721,3), ('200801','Sechura',1788,4), ('200802','Bellavista de la Union',1788,4), ('200803','Bernal',1788,4), ('200804','Cristo Nos Valga',1788,4), ('200805','Vice',1788,4), ('200806','Rinconada Llicuar',1788,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('210000','Puno',1,2),
		('210100','Puno',1795,3), ('210101','Puno',1796,4), ('210102','Acora',1796,4), ('210103','Amantani',1796,4), ('210104','Atuncolla',1796,4), ('210105','Capachica',1796,4), ('210106','Chucuito',1796,4), ('210107','Coata',1796,4), ('210108','Huata',1796,4), ('210109','Mañazo',1796,4), ('210110','Paucarcolla',1796,4), ('210111','Pichacani',1796,4), ('210112','Plateria',1796,4), ('210113','San Antonio',1796,4), ('210114','Tiquillaca',1796,4), ('210115','Vilque',1796,4),	
		('210200','Azangaro',1795,3), ('210201','Azangaro',1812,4), ('210202','Achaya',1812,4), ('210203','Arapa',1812,4), ('210204','Asillo',1812,4), ('210205','Caminaca',1812,4), ('210206','Chupa',1812,4), ('210207','Jose Domingo Choquehuanca',1812,4), ('210208','Muñani',1812,4), ('210209','Potoni',1812,4), ('210210','Saman',1812,4), ('210211','San Anton',1812,4), ('210212','San Jose',1812,4), ('210213','San Juan de Salinas',1812,4), ('210214','Santiago de Pupuja',1812,4), ('210215','Tirapata',1812,4),	
		('210300','Carabaya',1795,3), ('210301','Macusani',1828,4), ('210302','Ajoyani',1828,4), ('210303','Ayapata',1828,4), ('210304','Coasa',1828,4), ('210305','Corani',1828,4), ('210306','Crucero',1828,4), ('210307','Ituata',1828,4), ('210308','Ollachea',1828,4), ('210309','San Gaban',1828,4), ('210310','Usicayos',1828,4),	
		('210400','Chucuito',1795,3), ('210401','Juli',1839,4), ('210402','Desaguadero',1839,4), ('210403','Huacullani',1839,4), ('210404','Kelluyo',1839,4), ('210405','Pisacoma',1839,4), ('210406','Pomata',1839,4), ('210407','Zepita',1839,4),	
		('210500','El Collao',1795,3), ('210501','Ilave',1847,4), ('210502','Capaso',1847,4), ('210503','Pilcuyo',1847,4), ('210504','Santa Rosa',1847,4), ('210505','Conduriri',1847,4),	
		('210600','Huancane',1795,3), ('210601','Huancane',1853,4), ('210602','Cojata',1853,4), ('210603','Huatasani',1853,4), ('210604','Inchupalla',1853,4), ('210605','Pusi',1853,4), ('210606','Rosaspata',1853,4), ('210607','Taraco',1853,4), ('210608','Vilque Chico',1853,4),	
		('210700','Lampa',1795,3), ('210701','Lampa',1862,4), ('210702','Cabanilla',1862,4), ('210703','Calapuja',1862,4), ('210704','Nicasio',1862,4), ('210705','Ocuviri',1862,4), ('210706','Palca',1862,4), ('210707','Paratia',1862,4), ('210708','Pucara',1862,4), ('210709','Santa Lucia',1862,4), ('210710','Vilavila',1862,4),	
		('210800','Melgar',1795,3), ('210801','Ayaviri',1873,4), ('210802','Antauta',1873,4), ('210803','Cupi',1873,4), ('210804','Llalli',1873,4), ('210805','Macari',1873,4), ('210806','Nuñoa',1873,4), ('210807','Orurillo',1873,4), ('210808','Santa Rosa',1873,4), ('210809','Umachiri',1873,4),	
		('210900','Moho',1795,3), ('210901','Moho',1883,4), ('210902','Conima',1883,4), ('210903','Huayrapata',1883,4), ('210904','Tilali',1883,4),	
		('211000','San Antonio de Putina',1795,3), ('211001','Putina',1888,4), ('211002','Ananea',1888,4), ('211003','Pedro Vilca Apaza',1888,4), ('211004','Quilcapuncu',1888,4), ('211005','Sina',1888,4),	
		('211100','San Roman',1795,3), ('211101','Juliaca',1894,4), ('211102','Cabana',1894,4), ('211103','Cabanillas',1894,4), ('211104','Caracoto',1894,4),	
		('211200','Sandia',1795,3), ('211201','Sandia',1899,4), ('211202','Cuyocuyo',1899,4), ('211203','Limbani',1899,4), ('211204','Patambuco',1899,4), ('211205','Phara',1899,4), ('211206','Quiaca',1899,4), ('211207','San Juan del Oro',1899,4), ('211208','Yanahuaya',1899,4), ('211209','Alto Inambari',1899,4), ('211210','San Pedro de Putina Punco',1899,4),	
		('211300','Yunguyo',1795,3), ('211301','Yunguyo',1910,4), ('211302','Anapia',1910,4), ('211303','Copani',1910,4), ('211304','Cuturapi',1910,4), ('211305','Ollaraya',1910,4), ('211306','Tinicachi',1910,4), ('211307','Unicachi',1910,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('220000','San Martin',1,2),
		('220100','Moyobamba',1918,3), ('220101','Moyobamba',1919,4), ('220102','Calzada',1919,4), ('220103','Habana',1919,4), ('220104','Jepelacio',1919,4), ('220105','Soritor',1919,4), ('220106','Yantalo',1919,4),
		('220200','Bellavista',1918,3), ('220201','Bellavista',1926,4), ('220202','Alto Biavo',1926,4), ('220203','Bajo Biavo',1926,4), ('220204','Huallaga',1926,4), ('220205','San Pablo',1926,4), ('220206','San Rafael',1926,4),
		('220300','El Dorado',1918,3), ('220301','San Jose de Sisa',1933,4), ('220302','Agua Blanca',1933,4), ('220303','San Martin',1933,4), ('220304','Santa Rosa',1933,4), ('220305','Shatoja',1933,4),
		('220400','Huallaga',1918,3), ('220401','Saposoa',1939,4), ('220402','Alto Saposoa',1939,4), ('220403','El Eslabon',1939,4), ('220404','Piscoyacu',1939,4), ('220405','Sacanche',1939,4), ('220406','Tingo de Saposoa',1939,4),
		('220500','Lamas',1918,3), ('220501','Lamas',1946,4), ('220502','Alonso de Alvarado',1946,4), ('220503','Barranquita',1946,4), ('220504','Caynarachi',1946,4), ('220505','Cuñumbuqui',1946,4), ('220506','Pinto Recodo',1946,4), ('220507','Rumisapa',1946,4), ('220508','San Roque de Cumbaza',1946,4), ('220509','Shanao',1946,4), ('220510','Tabalosos',1946,4), ('220511','Zapatero',1946,4),
		('220600','Mariscal Caceres',1918,3), ('220601','Juanjui',1958,4), ('220602','Campanilla',1958,4), ('220603','Huicungo',1958,4), ('220604','Pachiza',1958,4), ('220605','Pajarillo',1958,4),
		('220700','Picota',1918,3), ('220701','Picota',1964,4), ('220702','Buenos Aires',1964,4), ('220703','Caspisapa',1964,4), ('220704','Pilluana',1964,4), ('220705','Pucacaca',1964,4), ('220706','San Cristobal',1964,4), ('220707','San Hilarion',1964,4), ('220708','Shamboyacu',1964,4), ('220709','Tingo de Ponasa',1964,4), ('220710','Tres Unidos',1964,4),
		('220800','Rioja',1918,3), ('220801','Rioja',1975,4), ('220802','Awajun',1975,4), ('220803','Elias Soplin Vargas',1975,4), ('220804','Nueva Cajamarca',1975,4), ('220805','Pardo Miguel',1975,4), ('220806','Posic',1975,4), ('220807','San Fernando',1975,4), ('220808','Yorongos',1975,4), ('220809','Yuracyacu',1975,4),				
		('220900','San Martin',1918,3), ('220901','Tarapoto',1985,4), ('220902','Alberto Leveau',1985,4), ('220903','Cacatachi',1985,4), ('220904','Chazuta',1985,4), ('220905','Chipurana',1985,4), ('220906','El Porvenir',1985,4), ('220907','Huimbayoc',1985,4), ('220908','Juan Guerra',1985,4), ('220909','La Banda de Shilcayo',1985,4), ('220910','Morales',1985,4), ('220911','Papaplaya',1985,4), ('220912','San Antonio',1985,4), ('220913','Sauce',1985,4), ('220914','Shapaja',1985,4),
		('221000','Tocache',1918,3), ('221001','Tocache',2000,4), ('221002','Nuevo Progreso',2000,4), ('221003','Polvora',2000,4), ('221004','Shunte',2000,4), ('221005','Uchiza',2000,4);
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('230000','Tacna',1,2),
		('230100','Tacna',2006,3), ('230101','Tacna',2007,4), ('230102','Alto de la Alianza',2007,4), ('230103','Calana',2007,4), ('230104','Ciudad Nueva',2007,4), ('230105','Inclan',2007,4), ('230106','Pachia',2007,4), ('230107','Palca',2007,4), ('230108','Pocollay',2007,4), ('230109','Sama',2007,4), ('230110','Coronel Gregorio Albarracín L',2007,4), 
		('230200','Candarave',2006,3), ('230201','Candarave',2018,4), ('230202','Cairani',2018,4), ('230203','Camilaca',2018,4), ('230204','Curibaya',2018,4), ('230205','Huanuara',2018,4), ('230206','Quilahuani',2018,4), 
		('230300','Jorge Basadre',2006,3), ('230301','Locumba',2025,4), ('230302','Ilabaya',2025,4), ('230303','Ite',2025,4), 
		('230400','Tarata',2006,3), ('230401','Tarata',2029,4), ('230402','Chucatamani',2029,4), ('230403','Estique',2029,4), ('230404','Estique-Pampa',2029,4), ('230405','Sitajara',2029,4), ('230406','Susapaya',2029,4), ('230407','Tarucachi',2029,4), ('230408','Ticaco',2029,4);		
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('240000','Tumbes',1,2),	
		('240100','Tumbes',2038,3), ('240101','Tumbes',2039,4), ('240102','Corrales',2039,4), ('240103','La Cruz',2039,4), ('240104','Pampas de Hospital',2039,4), ('240105','San Jacinto',2039,4), ('240106','San Juan de la Virgen',2039,4), 
		('240200','Contralmirante Villar',2038,3), ('240201','Zorritos',2046,4), ('240202','Casitas',2046,4), ('240203','Canoas de Punta Sal',2046,4), 
		('240300','Zarumilla',2038,3), ('240301','Zarumilla',2050,4), ('240302','Aguas Verdes',2050,4), ('240303','Matapalo',2050,4), ('240304','Papayal',2050,4);			
		INSERT INTO [dbo].[ubigeo] ([cod], [name], [up], [type]) VALUES
		('250000','Ucayali',1,2),	
		('250100','Coronel Portillo',2055,3), ('250101','Callaria',2056,4), ('250102','Campoverde',2056,4), ('250103','Iparia',2056,4), ('250104','Masisea',2056,4), ('250105','Yarinacocha',2056,4), ('250106','Nueva Requena',2056,4), ('250107','Manantay',2056,4), 
		('250200','Atalaya',2055,3), ('250201','Raymondi',2064,4), ('250202','Sepahua',2064,4), ('250203','Tahuania',2064,4), ('250204','Yurua',2064,4), 
		('250300','Padre Abad',2055,3), ('250301','Padre Abad',2069,4), ('250302','Irazola',2069,4), ('250303','Curimana',2069,4), 
		('250400','Purus',2055,3), ('250401','Purus',2073,4);

	--= Job
		INSERT INTO [dbo].[job] ([name], [active]) VALUES
		('Administrador del sistema', 1),
		('Analista comercial', 1),
		('Analista contable', 1),
		('Analista de rrhh', 1),
		('Analista de sistemas', 1),
		('Analista de bienestar social', 1),
		('Analista de consultoria', 1),
		('Analista de costo', 1),
		('Analista de costos', 1),
		('Analista de crédito y cobranzas', 1),
		('Analista de gestion financiera', 1),
		('Analista de gestion financiera', 1),
		('Analista de nomina', 1),
		('Analista de operaciones', 1),
		('Analista de planillas', 1),
		('Analista de proceso', 1),
		('Analista de rrhh', 1),
		('Analista de selección', 1),
		('Analista de selección y desarrollo', 1),
		('Analista de sistema SAP', 1),
		('Analista de sistemas', 1),
		('Analista de tesoreria', 1),
		('Analista financiero', 1),
		('Analista funcional', 1),
		('Analista funcional SAP', 1),
		('Analista TI', 1),
		('Analista NIIF', 1),
		('Analista programador', 1),
		('Analista programador sistemas', 1),
		('Analista tributario', 1),
		('Asesor', 1),
		('Asesor de licitaciones', 1),
		('Asistente consultoría', 1),
		('Asistente administrativo', 1),
		('Asistente administrativo- custodio', 1),
		('Asistente comercial', 1),
		('Asistente contable', 1),
		('Asistente contable NIIF', 1),
		('Asistente de administración', 1),
		('Asistente de compras', 1),
		('Asistente de consultoría', 1),
		('Asistente de costos', 1),
		('Asistente de crédito y cobranza', 1),
		('Asistente de facturación y cobranza', 1),
		('Asistente de inteligencia comercial', 1),
		('Asistente de logística', 1),
		('Asistente de mantenimiento', 1),
		('Asistente de marketing', 1),
		('Asistente de planillas', 1),
		('Asistente de selección', 1),
		('Asistente de servicio al cliente', 1),
		('Asistente de sistemas', 1),
		('Asistente de tesoreria', 1),
		('Asistente de tesoreria-custodio', 1),
		('Asistente funcional SAP', 1),
		('Asistente tributario', 1),
		('Auditor financiero', 1),
		('Auditor interno', 1),
		('Auxiliar administrativa', 1),
		('Auxiliar administrativo', 1),
		('Auxiliar archivo', 1),
		('Auxiliar contable', 1),
		('Auxiliar de atención al cliente', 1),
		('Auxiliar de cobranzas', 1),
		('Auxiliar de consultoría', 1),
		('Auxiliar de limpieza', 1),
		('Auxiliar de logística', 1),
		('Auxiliar de mantenimiento', 1),
		('Auxiliar de oficina', 1),
		('Auxiliar de seguridad de la información', 1),
		('Auxiliar de seguridad de la información', 1),
		('Auxiliar de sistemas', 1),
		('Auxiliar de soporte ti', 1),
		('Auxiliar de tesorería', 1),
		('Auxiliar RR.HH.', 1),
		('Auxiliar sistemas', 1),
		('Consultor gestión contable', 1),
		('Consultor SAP', 1),
		('Contador encargado de NIIF', 1),
		('Contador encargado', 1),
		('Contador encargado de consultoria', 1),
		('Contador encargado de costos', 1),
		('Contador encargado tributario', 1),
		('Contador encargado tributos', 1),
		('Contador', 1),
		('Contador encargado de NIIF', 1),
		('Coordinador de atención al cliente', 1),
		('Coordinador de compensaciones y beneficios', 1),
		('Coordinador de consultoria', 1),
		('Coordinador de inteligencia de negocios', 1),
		('Coordinador de logística', 1),
		('Coordinador de marketing', 1),
		('Coordinador de planillas', 1),
		('Coordinador de redes y comunicaciones', 1),
		('Coordinador de sistemas', 1),
		('Coordinador SAP', 1),
		('Coordinador de capacitación', 1),
		('Coordinador de desarrollo', 1),
		('Coordinador de selección y desarrollo', 1),
		('Coordinador de selección externa', 1),
		('Digitador', 1),
		('Digitador contable', 1),
		('Director de administración y finanzas', 1),
		('Director de contabilidad', 1),
		('Director de finanzas', 1),
		('Director de administración', 1),
		('Director de negocios y consultoría', 1),
		('Director de contabilidad', 1),
		('Director de operaciones', 1),
		('Director de contabilidad', 1),
		('Ejecutivo comercial', 1),
		('Ejecutivo de capacitación', 1),
		('Ejecutivo senior comercial', 1),
		('Ejecutivo comercial', 1),
		('Ejecutivo de consultoría', 1),
		('Generalista de recursos humanos', 1),
		('Generalista de RR.HH.', 1),
		('Gerente comercial', 1),
		('Gerente contabilidad', 1),
		('Gerente de BPO y TI', 1),
		('Gerente de calidad y marketing', 1),
		('Gerente de consultoría', 1),
		('Gerente de contabilidad', 1),
		('Gerente de contabilidad', 1),
		('Gerente de contabilidad', 1),
		('Gerente de gestión contable y tributaria', 1),
		('Gerente de gestión del talento', 1),
		('Gerente de impuestos', 1),
		('Gerente de NIIF', 1),
		('Gerente de NIIF - jefe', 1),
		('Gerente de nuevos negocios', 1),
		('Gerente de Payroll', 1),
		('Gerente de producto', 1),
		('Gerente de selección externa y capacitación', 1),
		('Gerente de selección y calidad externa', 1),
		('Gerente finanzas y administración', 1),
		('Gerente general', 1),
		('Implementador SAP', 1),
		('Jefe comercial', 1),
		('Jefe contable', 1),
		('Jefe de calidad y legal', 1),
		('Jefe de capacitación', 1),
		('Jefe de capacitación y ventas', 1),
		('Jefe de facturación cobranzas y compras', 1),
		('Jefe de facturación cobranzas y compras-supervisor', 1),
		('Jefe de impuestos', 1),
		('Jefe de proyecto', 1),
		('Jefe de proyecto de inventarios', 1),
		('Jefe de recursos humanos', 1),
		('Jefe de selección', 1),
		('Jefe de servicios de planillas', 1),
		('Jefe de servicios de planillas- supervisor', 1),
		('Jefe de sistemas', 1),
		('Jefe de telemarketing', 1),
		('Jefe de tesorería', 1),
		('Mantenimiento', 1),
		('Postulante', 1),
		('Practicante', 1),
		('Practicante pre profesional', 1),
		('Practicante profesional', 1),
		('Procurador', 1),
		('Product manager', 1),
		('Programador', 1),
		('Recepcionista', 1),
		('Secretaria', 1),
		('Secretaria de consultoría', 1),
		('Secretaria de gerencia', 1),
		('Secretaria de gerencia', 1),
		('Secretaria de gerencia de contabilidad', 1),
		('Sub gerente comercial', 1),
		('Sub gerente de contabilidad y finanzas', 1),
		('Sub gerente de administración', 1),
		('Supervisor', 1),
		('Supervisor contable', 1),
		('Supervisor de consultoría', 1),
		('Supervisor de contabilidad', 1),
		('Supervisor de nómina', 1),
		('Supervisor de planillas', 1),
		('Supervisor de procesos contables', 1),
		('Supervisor de telemarketing', 1),
		('Supervisor de tesorería y gestión financiera', 1),
		('Supervisor NIIF', 1),
		('Supervisor SAP', 1),
		('Supervisor semisenior', 1),
		('Supervisor tributario', 1),
		('Supervisor', 1),
		('Supervisor NIIF', 1),
		('Tesorero', 1),
		('Trabajadora social', 1),
		('Coordinador de TI', 1),
		('Coordinador de Desarrollo', 1),
		('Coordinador de Base de datos', 1),
		('Agente ServiceDesk', 1),
		('Programador', 1),
		('Analista base de datos', 1),
		('Soporte Tecnico', 1),
		('Encargado de Ingenieria', 1),
		('Ingenierio Electronico', 1),
		('Ingeniero', 1),
		('Vicepresidente ejecutivo', 1);

	--= Company
		INSERT INTO [dbo].[company]([name], [numberDoc], [address], [ubigeo], [active]) VALUES
		('SigloBPO', '20457875089', 'Dirección 1', 1405, 1),
		('ARCHROMA Perú S.A.', '20552611307', 'Dirección ARCHOMA', 1405, 1),
		('ENGIE Energia Pérú S.A.', '20333363900', 'Dirección 2', 1405, 1);
	--= Office
		INSERT INTO [dbo].[office]([name], [comId], [type] ) VALUES
		('Atención al cliente', 1, 1),	-- Area 1 -- SigloBPO -- 1
		('TI', 1, 1),	-- Area 2 -- SigloBPO -- 1
		('Desarrollo', 1, 1), -- Area 3 -- SigloBPO -- 1
		('Base de datos', 1, 1), -- Area 4 -- SigloBPO -- 1
		('Contabilidad', 1, 0), -- Area 5 -- SigloBPO -- 1
		('Tributos', 1, 0), -- Area 6 -- SigloBPO -- 1
		('Marketing', 1, 0), -- Area 7 -- SigloBPO -- 1
		('Comercial', 1, 0), -- Area 8 -- SigloBPO -- 1
		('Recursos Humanos', 1, 0), -- Area 9 -- SigloBPO -- 1
		('Planta', 2, 0), -- Area 10 -- ARCHOMA Peru -- 2
		('Gerencia', 2, 0), -- Area 11 -- ARCHOMA Peru -- 2
		('Tributos', 3, 0), -- Area 12 -- ENGIE -- 3
		('Ingenieria', 3, 0), -- Area 13 -- ENGIE -- 3
		('Marketing', 3, 0); -- Area 14 -- ENGIE -- 3
		
	--= UserType
		INSERT INTO [dbo].[userType]([name], [active]) VALUES
		('Adm. Sistema', 1), -- Adm. sistema
		('Jefe de área', 1), -- Jefe de área (operativa)
		('Soporte especializado', 1), -- Soporte especializado
		('Agente', 1), -- Soporte primera linea
		('Contacto de empresa', 1), -- Soporte especializado
		('Cliente', 1); -- Usuario final

	--= Impact
		INSERT INTO [dbo].[impact]([name]) VALUES
		('Bajo - Una persona'),
		('Medio - Un Servicio'),
		('Alto - Un Departamento');
	--= Urgency
		INSERT INTO [dbo].[urgency] ([name]) VALUES
		('Bajo'),
		('Medio'),
		('Alto');
	--= Priority
		INSERT INTO [dbo].[priority] ([name]) VALUES
		('Planificación'),
		('Bajo'),
		('Medio'),
		('Alto'),
		('Crítico');


	--= ServiceType
		INSERT INTO [dbo].[serviceType] ([name]) VALUES
		('Hardware'),
		('Software'),
		('Gestión'),
		('Otros');

	--= Service
		INSERT INTO [dbo].[service] ([name], [Type]) VALUES
		('Soporte Tecnico', 1), -- Servicio 1
		('Ocont', 2), -- Servicio 2
		('GLE', 2), -- Servicio 3
		('Caja Chica', 2), -- Servicio 4
		('Carpeta Digital', 2), -- Servicio 5
		('Planillas', 3), -- Servicio 6
		('Selección', 3), -- Servicio 7
		('Comercial', 3), -- Servicio 8 
		('Cartera', 3), -- Servicio 9
		('Otros', 4); -- Servicio 10

	--= SLAType
		INSERT INTO [dbo].[slaType] ([name]) VALUES
		('Tiempo'),
		('Disponibilidad');
	--= SLA
		INSERT INTO [dbo].[sla] ([name], [Type]) VALUES
		('Tiempo de resolución < ', 1),
		('Disponibilidad interumpida', 2),
		('Rendimiento', 2);

	--=Company Service
		INSERT INTO [dbo].[comSvc] ([comId], [serId]) VALUES
		(1,1), (1,2), (1,3), (1,4), (1,5),
		(2,1), (2,2),
		(3,2), (3,3), (3,4);

	--=Company Service ComSvcSla
		INSERT INTO [dbo].[comSvcSla] ([comSvc], [sla], [data]) VALUES
		(1 ,1, '4:00 Horas'),
		(2 ,3, ''),
		(3 ,3, ''),
		(4 ,3, ''),
		(5 ,3, ''),
		(6 ,1, '2:00 Horas'),
		(7 ,3, ''),
		(8 ,3, ''),
		(9 ,3, ''),
		(10 ,3, ''),
		(8 ,2, ''),
		(9 ,2, ''),
		(10 ,2, '');

	--= People
						-- @dni,			@name,					@nameP,				@nameM,			@pass,								 @birth,				 @email,					@telf1, @telf2, @note,			@comId, @offId, @jobId, @typeId
																																																																-- SigloBPO
		EXEC uspPeopleIns '11111110',		'Gian',					'Farfan',			'Correa',		'QYwETmVIr+gI3JNhSbJL9g==',			'14-07-1994',			'gianb.fc2@gmail.com',		'',		'',		'',				1,		3,		163,	1;				-- Adm. Sistema					//48350110
		EXEC uspPeopleIns '11111111',		'Mario',				'Orrego',			'Zarate',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		1,		193,	4;				-- Agente						//72491727
																																																																	-- TI
		EXEC uspPeopleIns '11111112',		'Alonso',				'Morote',			'Cuadrado',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		2,		190,	2;				-- Jefa de area
		EXEC uspPeopleIns '11111113',		'Luis',					'Vidal',			'Jimenez',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		2,		196,	3;				-- Soporte Especializado		//70272479
		EXEC uspPeopleIns '11111114',		'Henderson',			'Miraval',			'Cabanillas',	'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		2,		196,	3;				-- Soporte Especializado		//45626293
																																																																	-- Desarrollo
		EXEC uspPeopleIns '11111115',		'Carmela',				'Contreras',		'Roque',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		3,		191,	2;				-- Jefa de area					//72445895
		EXEC uspPeopleIns '11111116',		'Ricardo',				'Malca',			'Herencia',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		3,		194,	3;				-- Soporte Especializado		//45309284
		EXEC uspPeopleIns '11111117',		'Walter',				'Carrasas',			'Curay',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		3,		194,	3;				-- Soporte Especializado		//70452921
																																																																	-- Base de datos
		EXEC uspPeopleIns '11111118',		'Carla',				'Benavides',		'Salas',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		4,		192,	2;				-- Jefa de area					//47273197
		EXEC uspPeopleIns '11111119',		'Josue',				'Alvarez',			'Romero',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				1,		4,		195,	3;				-- Soporte Especializado		//72461262

																																																																-- ARCHROMA Perú S.A.
		EXEC uspPeopleIns '11111120',		'ARCHOMA01',			'ARCHOMA01',		'ARCHOMA01',	'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				2,		11,		118,	5;			-- Contacto
																																																																	-- Planta
		EXEC uspPeopleIns '11111121',		'ARCHOMA02',			'ARCHOMA02',		'ARCHOMA02',	'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				2,		10,		165,	6;				-- Soporte Especializado		
																																																																	-- Gerencia
		EXEC uspPeopleIns '11111122',		'ARCHOMA03',			'ARCHOMA03',		'ARCHOMA03',	'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				2,		11,		140,	6;				-- Soporte Especializado		
		
																																																																-- ENGIE Energia Pérú S.A.
		EXEC uspPeopleIns '11111130',		'ENGIE01',				'ENGIE01',			'ENGIE01',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				3,		13,		197,	5;			-- Contacto
																																																																	-- Tributos
		EXEC uspPeopleIns '11111131',		'ENGIE02',				'ENGIE02',			'ENGIE02',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				3,		12,		30,		6;				-- Soporte Especializado		
																																																																	-- Ingenieria
		EXEC uspPeopleIns '11111132',		'ENGIE03',				'ENGIE03',			'ENGIE03',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				3,		13,		198,	6;				-- Soporte Especializado		
																																																																	-- Marketing
		EXEC uspPeopleIns '11111133',		'ENGIE04',				'ENGIE04',			'ENGIE04',		'QYwETmVIr+gI3JNhSbJL9g==',			'01-01-1990',			'correo@gmail.com',			'',		'',		'',				3,		14,		92,		6;				-- Soporte Especializado		
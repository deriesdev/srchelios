using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entIncident : entBase<entIncident>
    {
        #region Propiedades
        [DataMember(Name ="Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name ="Subject", Order = 3)]
        public string subject { set; get; } = string.Empty;
        [DataMember(Name ="Origin", Order = 4)]
        public int origin { set; get; } = 0;
        [DataMember(Name ="State", Order = 5)]
        public int state { set; get; } = 0;
        [DataMember(Name ="OffId", Order = 7)]
        public int offId { set; get; } = 0;
        [DataMember(Name ="SerId", Order = 9)]
        public int serId { set; get; } = 0;
        [DataMember(Name ="UrgId", Order = 10)]
        public int urgId { set; get; } = 0;
        [DataMember(Name ="ImpId", Order = 11)]
        public int impId { set; get; } = 0;
        [DataMember(Name ="PriId", Order = 12)]
        public int priId { set; get; } = 0;
        [DataMember(Name ="Scale", Order = 13)]
        public int? scale { set; get; } = null;
        [DataMember(Name = "ScaleType", Order = 14)]
        public bool? scaleType { set; get; } = null;
        [DataMember(Name ="Expert", Order = 15)]
        public int? expert { set; get; } = null;
        [DataMember(Name = "Likert", Order = 16)]
        public int? likert { set; get; } = null;

        [DataMember(Name = "Min", Order = 17)]
        public int? min { set; get; } = 0;
        [DataMember(Name = "Date", Order = 18)]
        public DateTime? date { set; get; } = DateTime.MinValue;
        [DataMember(Name = "Sla", Order = 19)]
        public bool sla { set; get; } = true;
        #endregion

        #region Constructor
        public entIncident () { }
        public entIncident (int _id) { id = _id; }
        public entIncident(int _id, string _name, string _subject, int _origin, int _state, int _comId, int _offId, int _peoId, int _serId, int _urgId, int _impId, int _priId, int _scale, int _expert, int _likert)
        {
            id = _id;
            name = _name;
            subject = _subject;
            origin = _origin;
            state = _state;
            comId = _comId;
            offId = _offId;
            peoId = _peoId;
            serId = _serId;
            urgId = _urgId;
            impId = _impId;
            priId = _priId;
            scale = _scale;
            expert = _expert;
            likert = _likert;
        }
        #endregion
        }
    }

using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entOffice : entBase<entOffice>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name = "Type", Order = 4)]
        public bool type { set; get; } = true;
        #endregion

        #region Constructor
        public entOffice() { }
        public entOffice(int _id) { id = _id; }
        public entOffice(int _id, string _name, int _comId, bool _type, bool _active)
        {
            id = _id;
            name = _name;
            comId = _comId;
            type = _type;
            active = _active;
        }
        #endregion
    }
}

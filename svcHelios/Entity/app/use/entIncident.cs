using DataAccess;
using System;
using System.Runtime.Serialization;
namespace Entity
{
    public partial class entIncident : entBase<entIncident>
    {
        public bool updLikert()
        {
            try
            {
                using (var con = new Connection())
                {
                    int req = con.getExecute("uspIncidentUpdLikert", this); ;
                    return (id = req) > 0;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public bool updExpert()
        {
            try
            {
                using (var con = new Connection())
                {
                    int req = con.getExecute("uspIncidentUpdExpert", this); ;
                    return (id = req) > 0;
                }
            }
            catch (Exception ex) { throw ex; }
        }


    }
}

using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcJob
    {
        public entJob add(entJob _obj)
        {
            try
            {
                logJob oEnt = new logJob();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entJob upd(entJob _obj)
        {
            try
            {
                logJob oEnt = new logJob();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entJob del(entJob _obj)
        {
            try
            {
                logJob oEnt = new logJob();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entJob sel(entJob _obj)
        {
            try
            {
                logJob oEnt = new logJob();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entJob> lis(entJob _obj)
        {
            try
            {
                logJob oEnt = new logJob();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcJob")]
    public partial interface intSvcJob
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addJob",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addJob")]
        entJob add(entJob _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updJob",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updJob")]
        entJob upd(entJob _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delJob",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delJob")]
        entJob del(entJob _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selJob",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selJob")]
        entJob sel(entJob _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisJob",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisJob")]
        IList<entJob> lis(entJob _obj);
    }
}


﻿using DataAccess;
using System;

namespace Entity
{
    public partial class entPeople : entBase<entPeople>
    {

        public bool auth()
        {
            try
            {
                bool rpt = false;
                //Select user in case exists
                using (var con = new Connection()) { rpt = con.getObj<entPeople>("uspPeopleAuth", this); }
                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
﻿using System.Collections.Generic;
using static DataAccess.DB;

namespace DataAccess
{
    internal class entDB
    {
        #region Variables
        internal string nom { set; get; } = string.Empty;
        internal string svr { set; get; } = string.Empty;
        internal string port { set; get; } = string.Empty;
        internal string db { set; get; } = string.Empty;
        internal string user { set; get; } = string.Empty;
        internal string pass { set; get; } = string.Empty;
        internal Motor motor { set; get; } = Motor.MsSQL;
        #endregion

        #region Builder
        internal entDB() { }
        internal entDB(string _nom, string _svr, string _port, string _db, string _user, string _pass, Motor _motor)
        {
            nom = _nom; svr = _svr; port = _port; db = _db; user = _user; pass = _pass; motor = _motor;
        }
        #endregion
    }

    internal static class DB
    {
        internal enum Motor { MsSQL, MySQL, PostgreSQL, DB2, Oracle }
        internal enum list { Default, Notebook }

        private static IList<entDB> oList = new List<entDB>();

        static DB()
        {
            oList.Add(new entDB("System Helios", "SERVER\\SQL2014EXPRESS", "-", "dbHelios", "gfarfanc711", "123bocnar", Motor.MsSQL));
            oList.Add(new entDB("System Helios", "SICAMP12-DES001\\MSSQL2014", "-", "dbSigloPortal", "sa", "Passw0rd", Motor.MsSQL));
        }

        internal static entDB get(list db = list.Default)
        {
            entDB rpt = null;
            switch (db)
            {
                case list.Default: rpt = oList[0]; break;
                case list.Notebook: rpt = oList[1]; break;
                default: rpt = oList[0]; break;
            }
            return rpt;
        }
    }


}
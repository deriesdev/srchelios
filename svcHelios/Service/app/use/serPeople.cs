﻿using Entity;
using Logic;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Service
{
    public partial class Service : intSvcPeople
    {
        public entPeople auth(entPeople _obj)
        {
            try
            {
                logPeople oPeople = new logPeople();
                return oPeople.auth(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    public partial interface intSvcPeople
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/authPeople",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "authPeople")]
        entPeople auth(entPeople _obj);
    }
}
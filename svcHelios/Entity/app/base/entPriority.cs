using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entPriority : entBase<entPriority>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        #endregion

        #region Constructor
        public entPriority() { }
        public entPriority(int _id) { id = _id; }
        public entPriority(int _id, string _name)
        {
            id = _id;
            name = _name;
        }
        #endregion
    }
}

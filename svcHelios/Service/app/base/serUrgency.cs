using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcUrgency
    {
        public entUrgency add(entUrgency _obj)
        {
            try
            {
                logUrgency oEnt = new logUrgency();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUrgency upd(entUrgency _obj)
        {
            try
            {
                logUrgency oEnt = new logUrgency();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUrgency del(entUrgency _obj)
        {
            try
            {
                logUrgency oEnt = new logUrgency();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUrgency sel(entUrgency _obj)
        {
            try
            {
                logUrgency oEnt = new logUrgency();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entUrgency> lis(entUrgency _obj)
        {
            try
            {
                logUrgency oEnt = new logUrgency();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcUrgency")]
    public partial interface intSvcUrgency
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addUrgency",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addUrgency")]
        entUrgency add(entUrgency _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updUrgency",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updUrgency")]
        entUrgency upd(entUrgency _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delUrgency",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delUrgency")]
        entUrgency del(entUrgency _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selUrgency",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selUrgency")]
        entUrgency sel(entUrgency _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisUrgency",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisUrgency")]
        IList<entUrgency> lis(entUrgency _obj);
    }
}


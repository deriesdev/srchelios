﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using web.intern;
using web.Service;

namespace web.app
{
    public partial class Method : Page
    {
        protected void Page_Load(object sender, EventArgs e) { }

        #region People
        [System.Web.Services.WebMethod]
        public static string people_Obj()
        {
            return JsonConvert.SerializeObject(new entPeople());
        }
        [System.Web.Services.WebMethod]
        public static string people_Set(entPeople entObj)
        {
            try
            {
                entPeople req = null;
                if (entObj.Id < 1) { req = new svcPeopleClient().addPeople(entObj); }
                else { req = new svcPeopleClient().updPeople(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string people_Del(entPeople entObj)
        {
            try
            {
                var req = new svcPeopleClient().delPeople(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string people_Sel(entPeople entObj)
        {
            try
            {
                var req = new svcPeopleClient().selPeople(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string people_Lis(entPeople entObj)
        {
            try
            {
                var req = new svcPeopleClient().lisPeople(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod(EnableSession = true)]
        public static string people_Auth(entPeople entObj)
        {
            try
            {
                var req = new svcPeopleClient().authPeople(entObj);

                if (req.Id > 0) //Exito
                {
                    _Session.set(_Session.list.id, req.Id);
                    _Session.set(_Session.list.dni, req.Dni);
                    _Session.set(_Session.list.name, req.Name);
                    _Session.set(_Session.list.surname, $"{req.NameP} {req.NameM}");
                    _Session.set(_Session.list.email, req.Email);
                    _Session.set(_Session.list.com, req.ComId);
                    _Session.set(_Session.list.office, req.OffId);
                    _Session.set(_Session.list.job, req.JobId);
                    _Session.set(_Session.list.type, req.TypeId);

                    req.Id = 1;
                }

                return resolve(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }

            string resolve(entPeople _obj)
            {
                IList<entPeople> oList = new List<entPeople>() { _obj };
                var rpt = (from x in oList select new { x.Id, x.Err, x.ErrCatch }).ToList();
                return JsonConvert.SerializeObject(rpt[0]);
            }
        }
        #endregion

        #region UserType
        [System.Web.Services.WebMethod]
        public static string userType_Obj()
        {
            return JsonConvert.SerializeObject(new entUserType());
        }
        [System.Web.Services.WebMethod]
        public static string userType_Set(entUserType entObj)
        {
            try
            {
                entUserType req = null;
                if (entObj.Id < 1) { req = new svcUserTypeClient().addUserType(entObj); }
                else { req = new svcUserTypeClient().updUserType(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string userType_Del(entUserType entObj)
        {
            try
            {
                var req = new svcUserTypeClient().delUserType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string userType_Sel(entUserType entObj)
        {
            try
            {
                var req = new svcUserTypeClient().selUserType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string userType_Lis(entUserType entObj)
        {
            try
            {
                var req = new svcUserTypeClient().lisUserType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Job
        [System.Web.Services.WebMethod]
        public static string job_Obj()
        {
            return JsonConvert.SerializeObject(new entJob());
        }
        [System.Web.Services.WebMethod]
        public static string job_Set(entJob entObj)
        {
            try
            {
                entJob req = null;
                if (entObj.Id < 1) { req = new svcJobClient().addJob(entObj); }
                else { req = new svcJobClient().updJob(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string job_Del(entJob entObj)
        {
            try
            {
                var req = new svcJobClient().delJob(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string job_Sel(entJob entObj)
        {
            try
            {
                var req = new svcJobClient().selJob(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string job_Lis(entJob entObj)
        {
            try
            {
                var req = new svcJobClient().lisJob(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Ubigeo
        [System.Web.Services.WebMethod]
        public static string ubigeo_Obj()
        {
            return JsonConvert.SerializeObject(new entUbigeo());
        }
        [System.Web.Services.WebMethod]
        public static string ubigeo_Children(entUbigeo entObj)
        {
            try
            {
                var req = new svcUbigeoClient().childrenUbigeo(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string ubigeo_Reverse(entUbigeo entObj)
        {
            try
            {
                var req = new svcUbigeoClient().reverseUbigeo(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Company
        [System.Web.Services.WebMethod]
        public static string company_Obj()
        {
            return JsonConvert.SerializeObject(new entCompany());
        }
        [System.Web.Services.WebMethod]
        public static string company_Set(entCompany entObj)
        {
            try
            {
                entCompany req = null;
                if (entObj.Id < 1) { req = new svcCompanyClient().addCompany(entObj); }
                else { req = new svcCompanyClient().updCompany(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string company_Del(entCompany entObj)
        {
            try
            {
                var req = new svcCompanyClient().delCompany(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string company_Sel(entCompany entObj)
        {
            try
            {
                var req = new svcCompanyClient().selCompany(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string company_Lis(entCompany entObj)
        {
            try
            {
                var req = new svcCompanyClient().lisCompany(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Office
        [System.Web.Services.WebMethod]
        public static string office_Obj()
        {
            return JsonConvert.SerializeObject(new entOffice());
        }
        [System.Web.Services.WebMethod]
        public static string office_Set(entOffice entObj)
        {
            try
            {
                entOffice req = null;
                if (entObj.Id < 1) { req = new svcOfficeClient().addOffice(entObj); }
                else { req = new svcOfficeClient().updOffice(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string office_Del(entOffice entObj)
        {
            try
            {
                var req = new svcOfficeClient().delOffice(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string office_Sel(entOffice entObj)
        {
            try
            {
                var req = new svcOfficeClient().selOffice(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string office_Lis(entOffice entObj)
        {
            try
            {
                var req = new svcOfficeClient().lisOffice(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Impact
        [System.Web.Services.WebMethod]
        public static string impact_Obj()
        {
            return JsonConvert.SerializeObject(new entImpact());
        }
        [System.Web.Services.WebMethod]
        public static string impact_Set(entImpact entObj)
        {
            try
            {
                entImpact req = null;
                if (entObj.Id < 1) { req = new svcImpactClient().addImpact(entObj); }
                else { req = new svcImpactClient().updImpact(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string impact_Del(entImpact entObj)
        {
            try
            {
                var req = new svcImpactClient().delImpact(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string impact_Sel(entImpact entObj)
        {
            try
            {
                var req = new svcImpactClient().selImpact(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string impact_Lis(entImpact entObj)
        {
            try
            {
                var req = new svcImpactClient().lisImpact(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Priority
        [System.Web.Services.WebMethod]
        public static string priority_Obj()
        {
            return JsonConvert.SerializeObject(new entImpact());
        }
        [System.Web.Services.WebMethod]
        public static string priority_Set(entPriority entObj)
        {
            try
            {
                entPriority req = null;
                if (entObj.Id < 1) { req = new svcPriorityClient().addPriority(entObj); }
                else { req = new svcPriorityClient().updPriority(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string priority_Del(entPriority entObj)
        {
            try
            {
                var req = new svcPriorityClient().delPriority(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string priority_Sel(entPriority entObj)
        {
            try
            {
                var req = new svcPriorityClient().selPriority(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string priority_Lis(entPriority entObj)
        {
            try
            {
                var req = new svcPriorityClient().lisPriority(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion
        
        #region Urgency
        [System.Web.Services.WebMethod]
        public static string urgency_Obj()
        {
            return JsonConvert.SerializeObject(new entUrgency());
        }
        [System.Web.Services.WebMethod]
        public static string urgency_Set(entUrgency entObj)
        {
            try
            {
                entUrgency req = null;
                if (entObj.Id < 1) { req = new svcUrgencyClient().addUrgency(entObj); }
                else { req = new svcUrgencyClient().updUrgency(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string urgency_Del(entUrgency entObj)
        {
            try
            {
                var req = new svcUrgencyClient().delUrgency(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string urgency_Sel(entUrgency entObj)
        {
            try
            {
                var req = new svcUrgencyClient().selUrgency(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string urgency_Lis(entUrgency entObj)
        {
            try
            {
                var req = new svcUrgencyClient().lisUrgency(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region ServiceType
        [System.Web.Services.WebMethod]
        public static string serviceType_Obj()
        {
            return JsonConvert.SerializeObject(new entServiceType());
        }
        [System.Web.Services.WebMethod]
        public static string serviceType_Set(entServiceType entObj)
        {
            try
            {
                entServiceType req = null;
                if (entObj.Id < 1) { req = new svcServiceTypeClient().addServiceType(entObj); }
                else { req = new svcServiceTypeClient().updServiceType(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string serviceType_Del(entServiceType entObj)
        {
            try
            {
                var req = new svcServiceTypeClient().delServiceType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string serviceType_Sel(entServiceType entObj)
        {
            try
            {
                var req = new svcServiceTypeClient().selServiceType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string serviceType_Lis(entServiceType entObj)
        {
            try
            {
                var req = new svcServiceTypeClient().lisServiceType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Service
        [System.Web.Services.WebMethod]
        public static string service_Obj()
        {
            return JsonConvert.SerializeObject(new entService());
        }
        [System.Web.Services.WebMethod]
        public static string service_Set(entService entObj)
        {
            try
            {
                entService req = null;
                if (entObj.Id < 1) { req = new svcServiceClient().addService(entObj); }
                else { req = new svcServiceClient().updService(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string service_Del(entService entObj)
        {
            try
            {
                var req = new svcServiceClient().delService(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string service_Sel(entService entObj)
        {
            try
            {
                var req = new svcServiceClient().selService(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string service_Lis(entService entObj)
        {
            try
            {
                var req = new svcServiceClient().lisService(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion
        
        #region SLAType
        [System.Web.Services.WebMethod]
        public static string slaType_Obj()
        {
            return JsonConvert.SerializeObject(new entSlaType());
        }
        [System.Web.Services.WebMethod]
        public static string slaType_Set(entSlaType entObj)
        {
            try
            {
                entSlaType req = null;
                if (entObj.Id < 1) { req = new svcSlaTypeClient().addSlaType(entObj); }
                else { req = new svcSlaTypeClient().updSlaType(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string slaType_Del(entSlaType entObj)
        {
            try
            {
                var req = new svcSlaTypeClient().delSlaType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string slaType_Sel(entSlaType entObj)
        {
            try
            {
                var req = new svcSlaTypeClient().selSlaType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string slaType_Lis(entSlaType entObj)
        {
            try
            {
                var req = new svcSlaTypeClient().lisSlaType(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region SLA
        [System.Web.Services.WebMethod]
        public static string sla_Obj()
        {
            return JsonConvert.SerializeObject(new entSla());
        }
        [System.Web.Services.WebMethod]
        public static string sla_Set(entSla entObj)
        {
            try
            {
                entSla req = null;
                if (entObj.Id < 1) { req = new svcSlaClient().addSla(entObj); }
                else { req = new svcSlaClient().updSla(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string sla_Del(entSla entObj)
        {
            try
            {
                var req = new svcSlaClient().delSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string sla_Sel(entSla entObj)
        {
            try
            {
                var req = new svcSlaClient().selSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string sla_Lis(entSla entObj)
        {
            try
            {
                var req = new svcSlaClient().lisSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region ComSvc
        [System.Web.Services.WebMethod]
        public static string comSvc_Obj()
        {
            return JsonConvert.SerializeObject(new entComSvc());
        }
        [System.Web.Services.WebMethod]
        public static string comSvc_Set(entComSvc entObj)
        {
            try
            {
                entComSvc req = null;
                if (entObj.Id < 1) { req = new svcComSvcClient().addComSvc(entObj); }
                else { req = new svcComSvcClient().updComSvc(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string comSvc_Del(entComSvc entObj)
        {
            try
            {
                var req = new svcComSvcClient().delComSvc(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string comSvc_Sel(entComSvc entObj)
        {
            try
            {
                var req = new svcComSvcClient().selComSvc(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string comSvc_Lis(entComSvc entObj)
        {
            try
            {
                var req = new svcComSvcClient().lisComSvc(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region ComSvcSla
        [System.Web.Services.WebMethod]
        public static string comSvcSla_Obj()
        {
            return JsonConvert.SerializeObject(new entComSvcSla());
        }
        [System.Web.Services.WebMethod]
        public static string comSvcSla_Set(entComSvcSla entObj)
        {
            try
            {
                entComSvcSla req = null;
                if (entObj.Id < 1) { req = new svcComSvcSlaClient().addComSvcSla(entObj); }
                else { req = new svcComSvcSlaClient().updComSvcSla(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string comSvcSla_Del(entComSvcSla entObj)
        {
            try
            {
                var req = new svcComSvcSlaClient().delComSvcSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string comSvcSla_Sel(entComSvcSla entObj)
        {
            try
            {
                var req = new svcComSvcSlaClient().selComSvcSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string comSvcSla_Lis(entComSvcSla entObj)
        {
            try
            {
                var req = new svcComSvcSlaClient().lisComSvcSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region Incident
        [System.Web.Services.WebMethod]
        public static string incident_Obj()
        {
            return JsonConvert.SerializeObject(new entIncident());
        }
        [System.Web.Services.WebMethod]
        public static string incident_Set(entIncident entObj)
        {
            try
            {
                entIncident req = null;
                if (entObj.Id < 1) { req = new svcIncidentClient().addIncident(entObj); }
                else { req = new svcIncidentClient().updIncident(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incident_Likert(entIncident entObj)
        {
            try
            {
                var req = new svcIncidentClient().updLikertIncident(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incident_Expert(entIncident entObj)
        {
            try
            {
                var req = new svcIncidentClient().updExpertIncident(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incident_Del(entIncident entObj)
        {
            try
            {
                var req = new svcIncidentClient().delIncident(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incident_Sel(entIncident entObj)
        {
            try
            {
                var req = new svcIncidentClient().selIncident(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incident_Lis(entIncident entObj)
        {
            try
            {
                var req = new svcIncidentClient().lisIncident(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region IncidentSla
        [System.Web.Services.WebMethod]
        public static string incidentSla_Obj()
        {
            return JsonConvert.SerializeObject(new entIncidentSla());
        }
        [System.Web.Services.WebMethod]
        public static string incidentSla_Set(entIncidentSla entObj)
        {
            try
            {
                entIncidentSla req = null;
                if (entObj.Id < 1) { req = new svcIncidentSlaClient().addIncidentSla(entObj); }
                else { req = new svcIncidentSlaClient().updIncidentSla(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incidentSla_Del(entIncidentSla entObj)
        {
            try
            {
                var req = new svcIncidentSlaClient().delIncidentSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incidentSla_Sel(entIncidentSla entObj)
        {
            try
            {
                var req = new svcIncidentSlaClient().selIncidentSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incidentSla_Lis(entIncidentSla entObj)
        {
            try
            {
                var req = new svcIncidentSlaClient().lisIncidentSla(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion

        #region IncidentExpert
        [System.Web.Services.WebMethod]
        public static string incidentExpert_Obj()
        {
            return JsonConvert.SerializeObject(new entIncidentExpert());
        }
        [System.Web.Services.WebMethod]
        public static string incidentExpert_Set(entIncidentExpert entObj)
        {
            try
            {
                entIncidentExpert req = null;
                if (entObj.Id < 1) { req = new svcIncidentExpertClient().addIncidentExpert(entObj); }
                else { req = new svcIncidentExpertClient().updIncidentExpert(entObj); }
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incidentExpert_Del(entIncidentExpert entObj)
        {
            try
            {
                var req = new svcIncidentExpertClient().delIncidentExpert(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incidentExpert_Sel(entIncidentExpert entObj)
        {
            try
            {
                var req = new svcIncidentExpertClient().selIncidentExpert(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionObj(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionObj(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionObj(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        [System.Web.Services.WebMethod]
        public static string incidentExpert_Lis(entIncidentExpert entObj)
        {
            try
            {
                var req = new svcIncidentExpertClient().lisIncidentExpert(entObj);
                return JsonConvert.SerializeObject(req);
            }
            catch (TimeoutException ex) { return ExceptionLis(err.TimeOut, ex.ToString()); }
            catch (CommunicationException ex) { return ExceptionLis(err.Communication, ex.ToString()); }
            catch (Exception ex) { return ExceptionLis(err.General, ex.ToString()); }
            finally { GC.Collect(); }
        }
        #endregion





        #region Extra

        internal enum err { TimeOut, Communication, General };
        internal static string errString(err type)
        {
            string _Err = string.Empty;
            switch (type)
            {
                case err.TimeOut: _Err = "Error: Tiempo agotado."; break;
                case err.Communication: _Err = "Error: En Comunicación."; break;
                case err.General: _Err = "Error: No identificado."; break;
                default: _Err = "Error: Default."; break;
            }
            return _Err;
        }

        private static string ExceptionObj(string _Err, string _ErrCatch)
        {
            var rpt = new entException() { Id = -1, Err = _Err, ErrCatch = "Exception Web to WCF:\n" + _ErrCatch };
            return JsonConvert.SerializeObject(rpt);
        }
        private static string ExceptionObj(err type, string _ErrCatch)
        {
            var rpt = new entException() { Id = -1, Err = errString(type), ErrCatch = "Exception Web to WCF:\n" + _ErrCatch };
            return JsonConvert.SerializeObject(rpt);
        }
        private static string ExceptionLis(string _Err, string _ErrCatch)
        {
            var rpt = new List<entException>() { new entException() { Id = -1, Err = _Err, ErrCatch = "Exception Web to WCF:\n" + _ErrCatch } };
            return JsonConvert.SerializeObject(rpt);
        }
        private static string ExceptionLis(err type, string _ErrCatch)
        {
            var rpt = new List<entException>() { new entException() { Id = -1, Err = errString(type), ErrCatch = "Exception Web to WCF:\n" + _ErrCatch } };
            return JsonConvert.SerializeObject(rpt);
        }

        #endregion

    }
}
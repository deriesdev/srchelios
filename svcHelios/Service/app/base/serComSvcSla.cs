using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcComSvcSla
    {
        public entComSvcSla add(entComSvcSla _obj)
        {
            try
            {
                logComSvcSla oEnt = new logComSvcSla();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entComSvcSla upd(entComSvcSla _obj)
        {
            try
            {
                logComSvcSla oEnt = new logComSvcSla();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entComSvcSla del(entComSvcSla _obj)
        {
            try
            {
                logComSvcSla oEnt = new logComSvcSla();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entComSvcSla sel(entComSvcSla _obj)
        {
            try
            {
                logComSvcSla oEnt = new logComSvcSla();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entComSvcSla> lis(entComSvcSla _obj)
        {
            try
            {
                logComSvcSla oEnt = new logComSvcSla();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcComSvcSla")]
    public partial interface intSvcComSvcSla
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addComSvcSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addComSvcSla")]
        entComSvcSla add(entComSvcSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updComSvcSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updComSvcSla")]
        entComSvcSla upd(entComSvcSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delComSvcSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delComSvcSla")]
        entComSvcSla del(entComSvcSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selComSvcSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selComSvcSla")]
        entComSvcSla sel(entComSvcSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisComSvcSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisComSvcSla")]
        IList<entComSvcSla> lis(entComSvcSla _obj);
    }
}


﻿using System.Web.Routing;

namespace web.intern
{
    public class _Route
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //Acceso
            routes.MapPageRoute("Login", "Login", "~/app/html/Login.aspx");
            routes.MapPageRoute("LockScreen", "LockScreen", "~/app/html/LockScreen.aspx");

            //Inicio
            routes.MapPageRoute("Default", "", "~/app/html/Dashboard/Dashboard.aspx");
            routes.MapPageRoute("Dashboard", "Dashboard", "~/app/html/Dashboard/Dashboard.aspx");

            /* Incidente */
            routes.MapPageRoute("Incident", "Incident", "~/app/html/Incident/Incident.aspx"); // Incidente
            routes.MapPageRoute("Incident-Add", "Incident/Add", "~/app/html/Incident/IncidentAdd.aspx"); // IncidenteAdd
            routes.MapPageRoute("Incident-Upd", "Incident/Upd/{cod}", "~/app/html/Incident/IncidentUpd.aspx"); // IncidenteUpd
            routes.MapPageRoute("Incident-Sel", "Incident/Sel/{cod}", "~/app/html/Incident/IncidentSel.aspx"); // IncidenteSel

            /* Administracion de la configuración */
            routes.MapPageRoute("Job", "Administration/Config/Job", "~/app/html/Job/Job.aspx"); // Job
            routes.MapPageRoute("Company", "Administration/Config/Company", "~/app/html/Company/Company.aspx"); // Company
            routes.MapPageRoute("Company-Service", "Administration/Config/Company/Service", "~/app/html/Company/CompanyService.aspx"); // Company
            routes.MapPageRoute("Office", "Administration/Config/Office", "~/app/html/Office/Office.aspx"); // Office
            routes.MapPageRoute("Urgency", "Administration/Config/Urgency", "~/app/html/Urgency/Urgency.aspx"); // Urgency
            routes.MapPageRoute("Impact", "Administration/Config/Impact", "~/app/html/Impact/Impact.aspx"); // Impact
            routes.MapPageRoute("Priority", "Administration/Config/Priority", "~/app/html/Priority/Priority.aspx"); // Priority
            routes.MapPageRoute("Service", "Administration/Config/Service", "~/app/html/Service/Service.aspx"); // Service
            routes.MapPageRoute("SLA", "Administration/Config/SLA", "~/app/html/SLA/SLA.aspx"); // Service

            /* Configuración */
            routes.MapPageRoute("UserType", "Administration/Config/UserType", "~/app/html/UserType/UserType.aspx"); // Priority
            routes.MapPageRoute("People", "Administration/Config/People", "~/app/html/People/People.aspx"); // Priority




            //Error
            routes.MapPageRoute("404", "404", "~/app/html/404.aspx");
            routes.MapPageRoute("500", "500", "~/app/html/500.aspx");
            routes.MapPageRoute("Error", "Error", "~/app/html/Error.aspx");
        }
    }
}
﻿CREATE TABLE [dbo].[userType]
(
	[id]		INT NOT NULL PRIMARY KEY IDENTITY,
	[name]		varchar(200) NOT NULL,
	[active]	bit NULL DEFAULT 1
)

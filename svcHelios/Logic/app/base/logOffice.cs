using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logOffice : ILogic<entOffice>
    {
        public entOffice add(entOffice _obj)
        {
            try
            {
                if (!_obj.add()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entOffice upd(entOffice _obj)
        {
            try
            {
                if (!_obj.upd()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entOffice del(entOffice _obj)
        {
            try
            {
                if (!_obj.del()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entOffice sel(entOffice _obj)
        {
            try
            {
                if (!_obj.sel()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entOffice> lis(entOffice _obj)
        {
            try { return _obj.lis(); }
            catch (Exception ex) { throw ex; }
        }
    }
}


using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcUbigeoType
    {
        public entUbigeoType add(entUbigeoType _obj)
        {
            try
            {
                logUbigeoType oEnt = new logUbigeoType();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUbigeoType upd(entUbigeoType _obj)
        {
            try
            {
                logUbigeoType oEnt = new logUbigeoType();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUbigeoType del(entUbigeoType _obj)
        {
            try
            {
                logUbigeoType oEnt = new logUbigeoType();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUbigeoType sel(entUbigeoType _obj)
        {
            try
            {
                logUbigeoType oEnt = new logUbigeoType();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entUbigeoType> lis(entUbigeoType _obj)
        {
            try
            {
                logUbigeoType oEnt = new logUbigeoType();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcUbigeoType")]
    public partial interface intSvcUbigeoType
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addUbigeoType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addUbigeoType")]
        entUbigeoType add(entUbigeoType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updUbigeoType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updUbigeoType")]
        entUbigeoType upd(entUbigeoType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delUbigeoType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delUbigeoType")]
        entUbigeoType del(entUbigeoType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selUbigeoType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selUbigeoType")]
        entUbigeoType sel(entUbigeoType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisUbigeoType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisUbigeoType")]
        IList<entUbigeoType> lis(entUbigeoType _obj);
    }
}


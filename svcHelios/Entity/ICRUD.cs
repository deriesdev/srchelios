﻿using System.Collections.Generic;

namespace Entity
{
    public interface ICRUD<T>
    {
        bool add();
        bool upd();
        bool del();
        bool sel();
        IList<T> lis();

    }

}

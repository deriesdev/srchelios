﻿CREATE TABLE [dbo].[comSvcSla]
(
	[id]		INT NOT NULL PRIMARY KEY IDENTITY,
	[data]		VARCHAR(20) NOT NULL,
	[comSvc]	INT NULL REFERENCES[comSvc](id),
	[sla]		INT NULL REFERENCES[sla](id),
	[active]	BIT NOT NULL DEFAULT 1
)

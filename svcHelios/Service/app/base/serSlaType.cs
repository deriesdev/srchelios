using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcSlaType
    {
        public entSlaType add(entSlaType _obj)
        {
            try
            {
                logSlaType oEnt = new logSlaType();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entSlaType upd(entSlaType _obj)
        {
            try
            {
                logSlaType oEnt = new logSlaType();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entSlaType del(entSlaType _obj)
        {
            try
            {
                logSlaType oEnt = new logSlaType();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entSlaType sel(entSlaType _obj)
        {
            try
            {
                logSlaType oEnt = new logSlaType();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entSlaType> lis(entSlaType _obj)
        {
            try
            {
                logSlaType oEnt = new logSlaType();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcSlaType")]
    public partial interface intSvcSlaType
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addSlaType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addSlaType")]
        entSlaType add(entSlaType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updSlaType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updSlaType")]
        entSlaType upd(entSlaType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delSlaType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delSlaType")]
        entSlaType del(entSlaType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selSlaType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selSlaType")]
        entSlaType sel(entSlaType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisSlaType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisSlaType")]
        IList<entSlaType> lis(entSlaType _obj);
    }
}


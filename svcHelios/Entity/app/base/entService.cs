using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entService : entBase<entService>
    {
        #region Propiedades
        [DataMember(Name ="Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name ="Type", Order = 3)]
        public int type { set; get; } = 0;
        #endregion

        #region Constructor
        public entService () { }
        public entService (int _id) { id = _id; }
        public entService(int _id, string _name, int _type, bool _active)
        {
            id = _id;
            name = _name;
            type = _type;
            active = _active;
        }
        #endregion
        }
    }

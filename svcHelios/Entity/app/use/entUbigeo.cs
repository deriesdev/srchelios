using DataAccess;
using System;
using System.Collections.Generic;

namespace Entity
{
    public partial class entUbigeo : entBase<entUbigeo>
    {

        public IList<entUbigeo> children()
        {
            try
            {
                var rpt = new List<entUbigeo>();
                using (var con = new Connection())
                {
                    rpt = con.getList<entUbigeo>("uspUbigeoChildren", this);
                }
                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entUbigeo> reverse()
        {
            try
            {
                var rpt = new List<entUbigeo>();
                using (var con = new Connection())
                {
                    rpt = con.getList<entUbigeo>("uspUbigeoReverse", this);
                }
                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}

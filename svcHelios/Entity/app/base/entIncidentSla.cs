using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entIncidentSla : entBase<entIncidentSla>
    {
        #region Propiedades
        [DataMember(Name ="Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name ="State", Order = 3)]
        public bool state { set; get; } = true;
        [DataMember(Name ="IncId", Order = 4)]
        public int incId { set; get; } = 0;
        #endregion

        #region Constructor
        public entIncidentSla () { }
        public entIncidentSla (int _id) { id = _id; }
        public entIncidentSla(int _id, string _name, bool _state, int _incId)
        {
            id = _id;
            name = _name;
            state = _state;
            incId = _incId;
        }
        #endregion
        }
    }

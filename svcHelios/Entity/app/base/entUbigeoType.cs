using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entUbigeoType : entBase<entUbigeoType>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        #endregion

        #region Constructor
        public entUbigeoType() { }
        public entUbigeoType(int _id) { id = _id; }
        public entUbigeoType(int _id, string _name)
        {
            id = _id;
            name = _name;
        }
        #endregion
    }
}

using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcUserType
    {
        public entUserType add(entUserType _obj)
        {
            try
            {
                logUserType oEnt = new logUserType();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUserType upd(entUserType _obj)
        {
            try
            {
                logUserType oEnt = new logUserType();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUserType del(entUserType _obj)
        {
            try
            {
                logUserType oEnt = new logUserType();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entUserType sel(entUserType _obj)
        {
            try
            {
                logUserType oEnt = new logUserType();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entUserType> lis(entUserType _obj)
        {
            try
            {
                logUserType oEnt = new logUserType();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcUserType")]
    public partial interface intSvcUserType
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addUserType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addUserType")]
        entUserType add(entUserType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updUserType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updUserType")]
        entUserType upd(entUserType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delUserType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delUserType")]
        entUserType del(entUserType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selUserType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selUserType")]
        entUserType sel(entUserType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisUserType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisUserType")]
        IList<entUserType> lis(entUserType _obj);
    }
}


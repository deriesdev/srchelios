using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcIncident
    {

        public entIncident updLikert(entIncident _obj)
        {
            try
            {
                logIncident oEnt = new logIncident();
                return oEnt.updLikert(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncident updExpert(entIncident _obj)
        {
            try
            {
                logIncident oEnt = new logIncident();
                return oEnt.updExpert(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

    }

    public partial interface intSvcIncident
    {

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updLikertIncident",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updLikertIncident")]
        entIncident updLikert(entIncident _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updExpertIncident",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updExpertIncident")]
        entIncident updExpert(entIncident _obj);

    }
}


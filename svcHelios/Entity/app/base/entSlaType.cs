using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entSlaType : entBase<entSlaType>
    {
        #region Propiedades
        [DataMember(Name ="Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        #endregion

        #region Constructor
        public entSlaType () { }
        public entSlaType (int _id) { id = _id; }
        public entSlaType(int _id, string _name, bool _active)
        {
            id = _id;
            name = _name;
            active = _active;
        }
        #endregion
        }
    }

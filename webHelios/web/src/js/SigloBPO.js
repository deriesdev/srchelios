﻿'use strict';

/////////////////////
//  Documentation  //
/////////////////////
//
//    Variable
//    Type ::: const : '_' - interactive : '$' - system : ''
//
//    Alter -  Swal
//    Type ::: 1:Success - 2:Info - 3:Warning - 4:Error
//

/* Start */
$(function () {
    jsSecurity.Session();
    themeFeature();
    getValidate();
    getselect2();
    getDataPicker();
    SummerNote();
    setIdentity();
});

/*  Modules ::: Start  */
var jsSecurity = new function () {
    this.Session = () => {
        if (window.location.pathname != `/Login`) {
            if ($('#sDni').val()) { }
            else { jsUtil.Short.toPage('/Login'); }
        }
    }
}; // Security
function themeFeature() {
    $("body").addClass('fixed-sidebar');
    $('.sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
    });
} // Theme
function getselect2() {
    $('.select2').select2({
        placeholder: "Seleccionar"
    });
} // Select2
function getDataPicker() {

    $('.DatePYear').datepicker({
        format: 'dd/mm/yyyy',
        startView: 2,
        todayBtn: false,
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

} //DataPicker
function SummerNote() {
    $('.summernote').summernote();
}

async function setIdentity() {

    if ($('#userName').length) {

        $("#userName").text(`${$("#sSurname").val()}, ${$("#sName").val()}`);

        let com = await jsCompany.Sel($("#sCom").val());
        $("#userCompany").text(com.Name);

        let off = await jsOffice.Sel($("#sOff").val());
        $("#userOffice").text(off.Name);

        let job = await jsJob.Sel($("#sJob").val());
        $("#userJob").text(job.Name);

        let type = await jsUserType.Sel($("#sType").val());
        $("#userType").text(type.Name);

    }    
}


/* Toastr */
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    //"timeOut": "1000",
    "extendedTimeOut": "10000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
/* End Toastr*/



//Variables Generales
var jsBase = new function () {

    this.String = new function () {
        this.Title = "SigloBPO";
    }
};

/*  jsUtil  */
var jsUtil = new function () {

    this.Alert = new function () {
        this.get = (obj) => {
            var rpt;
            switch (obj) {
                case 1: rpt = ["success", "#5cb85c"]; break;
                case 2: rpt = ["info", "#5bc0de"]; break;
                case 3: rpt = ["warning", "#f0ad4e"]; break;
                case 4: rpt = ["error", "#d9534f"]; break;
            }
            return rpt;
        }
    }//Alert
    this.Color = new function () {

        const more = (_color, _level) => {
            let cc = parseInt(_color, 16) + _level;
            let c = (cc > 255) ? 255 : (cc);
            c = (c.toString(16).length > 1) ? c.toString(16) : `0${c.toString(16)}`;
            return c;
        }

        const less = (_color, _level) => {
            let cc = parseInt(_color, 16) - _level;
            let c = (cc < 0) ? 0 : (cc);
            c = (c.toString(16).length > 1) ? c.toString(16) : `0${c.toString(16)}`;
            return c;
        }

        this.More = (_color, _level) => {
            _color = (_color.indexOf("#") >= 0) ? _color.substring(1, _color.length) : _color;
            _level = parseInt((255 * _level) / 100);
            return _color = `#${more(_color.substring(0, 2), _level)}${more(_color.substring(2, 4), _level)}${more(_color.substring(4, 6), _level)}`;
        }

        this.Less = (_color, _level) => {
            _color = (_color.indexOf("#") >= 0) ? _color.substring(1, _color.length) : _color;
            _level = parseInt((255 * _level) / 100);
            return _color = `#${less(_color.substring(0, 2), _level)}${less(_color.substring(2, 4), _level)}${less(_color.substring(4, 6), _level)}`;
        }

    }//Color
    this.Default = new function () {

        this.Var = new function () {
            this.Int = 0;
            this.List = [];
            this.Obj = {};
        }
        this.Req = new function () {
            this.entObj = (obj) => {
                return { entObj: obj };
            }
        }

    }//Default
    this.Notify = new function () {
        //Type :::  1:Success - 2:Info - 3:Warning - 4:Error
        this.Show = (_type, _message, _console = '') => {

            //jsUtil.Notify.Remove();
            switch (_type) {
                case 1: toastr.success(_message, jsBase.String.Title, { timeOut: 2000 }); break;
                case 2: toastr.info(_message, jsBase.String.Title, { timeOut: 3000 }); break;
                case 3: toastr.warning(_message, jsBase.String.Title, { timeOut: 4000 }); break;
                case 4: toastr.error(_message, jsBase.String.Title, { timeOut: 5000 }); break;
            }
            if (_console !== '') { console.log(_console); }
        }
        this.Remove = () => { toastr.remove(); }
        this.Clear = () => { toastr.clear(); }
    }//Notify
    this.Short = new function () {
        this.toPage = (_text) => { location.href = _text }
        this.F5 = () => { location.reload(true); }
    }//Short
    this.Swal = new function () {

        this.Alert = (_title, _msg) => {
            swal({
                title: _title,
                text: _msg,
                confirmButtonColor: '#5bc0de'
            });
        }
        this.AlertType = (_msg, _alert) => {
            let alert = jsUtil.Alert.get(_alert);
            swal({
                title: jsBase.String.Title,
                text: _msg,
                type: alert[0],
                confirmButtonColor: alert[1]
            });
        }
        this.Question = (_msg, _alert, fn) => {
            let alert = jsUtil.Alert.get(_alert);
            swal({
                title: jsBase.String.Title,
                text: _msg,
                type: alert[0],
                confirmButtonColor: alert[1],
                cancelButtonColor: '#FFFFFF',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                showCancelButton: true,
                closeOnConfirm: true
            }, function () { fn(); });
        }
    }//Swal
    this.Validate = new function () {

        this.Ano = (_obj) => {
            let rpt = false;
            try { rpt = (_obj >= 1900 && _obj <= 2100); }
            catch (e) { rpt = false }
            return rpt;
        }
        this.Mes = (_obj) => {
            let rpt = false;
            try { rpt = (_obj >= 1 && _obj <= 12); }
            catch (e) { rpt = false }
            return rpt;
        }
        this.Dia = (_obj) => {
            let rpt = false;
            try { rpt = (_obj >= 1 && _obj <= 31); }
            catch (e) { rpt = false }
            return rpt;
        }

        this.System = new function () {

            this.List = (_List) => {
                var rpt = true;
                if (_List.length == 1) {
                    if (_List[0].Id < 0) {
                        jsUtil.Notify.Show(4, _List[0].Err, _List[0].ErrCatch);
                        rpt = false;
                    }
                }
                return rpt;
            }

            this.Obj = (_Obj) => {
                var rpt = true;
                if (Object.keys(_Obj).length > 0) {
                    if (_Obj.Id < 0) {
                        jsUtil.Notify.Show(4, _Obj.Err, _Obj.ErrCatch);
                        rpt = false;
                    }
                } else {
                    rpt = false;
                }
                return rpt;
            }
        }


    }//Validate
    this.DataTable = new function () {
        this.getSelect = (id) => {
            try {

                var tbl = $(id).DataTable();
                var trLis = $(id.concat(" tbody")).children();
                var select = 0;

                trLis.each(function (e, c) {
                    if ($(c).hasClass('selected')) { select = $(c); }
                });

                if (select != 0) {
                    var data = tbl.row(select).data();
                    return data['Id'];
                } else {
                    jsUtil.Notify.Show(3, "Seleccione un item", "");
                    return jsUtil.Default.Var.Int;
                }

            } catch (e) {
                jsUtil.Notify.Show(4, "No se pudo seleccionar - getSelect", e.message.toString());
                return jsUtil.Default.Var.Int;
            }
        }

        this.set = (table, format, data) => {

            try {
                jsUtil.Replace.Json(data, lstActive, 'Active', 'Name');
            } catch (ex) { }


            try {
                $(table).DataTable({
                    dom: '<"html5buttons"B>lTfgitp',
                    language: { "url": "/src/plugins/dataTables/Spanish.json" },
                    columns: format,
                    data: data,
                    ordering: false,
                    pageLength: 8,
                    responsive: true,
                    buttons: [
                        { extend: 'copy' }, { extend: 'csv' }, { extend: 'excel', title: 'ExampleFile' }, { extend: 'pdf', title: 'ExampleFile' },
                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ],
                    destroy: true
                });
            } catch (ex) { throw ex; }
        }
    }//DataTable
    this.Modal = new function () {
        this.Show = (modal) => {
            $(modal).modal('show');
        }
        this.Show = (modal, title) => {
            $(`${modal} h4`).text(title);
            $(modal).modal('show');
        }
        this.Close = (modal) => {
            $(modal).modal('hide');
        }
    }//Modal
    this.Select = new function () {
        this.Clear = (id) => {
            $(id).find('option').remove().end();
        }
        this.Down = async function (up, down) {
            try {
                let id = $(up).find(":selected").val();
                //$(down).find('option').remove().end().append('<option value="default">Seleccionar</option>').val('default');
                $(down).find('option').remove().end();
                if (id != 'default') {
                    let list = await jsUbigeo.Children(id);

                    $(list).each(function () {
                        $(down).append(`<option value="${this.Id}">${this.Name}</option>`);
                    });
                }
            } catch (e) { console.log(e); }
        }
        this.Country = async function (id) {
            try {
                let list = await jsUbigeo.Children(-1);

                $(id).find('option').remove().end();

                $(list).each(function () {
                    $(id).append(`<option value="${this.Id}">${this.Name}</option>`);
                });

            } catch (e) { console.log(e); }
        }
        this.SetVal = (id, val) => {
            $(id)
                .val(val)
                .trigger('change');
        }
        this.Load = function (id, data, val, param) {
            jsUtil.Select.Clear(id);
            let params = arguments;
            $(data).each(function () {
                let rpt = "";
                if (params.length > 4) {
                    for (var x = 3; x < params.length; x++) {
                        if (x != 3) { rpt = rpt + ` - `; }
                        rpt = rpt + `${this[params[x]]}`;
                    }
                } else {
                    rpt = this[param];
                }
                $(id).append(`<option value="${this[val]}">${rpt}</option>`);
            });

        }
    }
    this.Replace = new function () {

        /*
            Padre
            modelo,
            key a reemplazar padre
            key del hijo
            key empate con padre
        */
        this.Json = (b, r, c, n, k = 'Id') => {
            $.each(b, function (i, item) {
                try {
                    let qwe = b[i][c];
                    let asd = r.find(item => item[k] === qwe)[n];
                    b[i][c] = asd;
                } catch (e) {}
            });
        }
        this.Name = (l) => {
            $.each(l, function (i, item) {
                l[i]['Name'] = `${l[i]['NameP']} ${l[i]['NameM']}, ${l[i]['Name']}`;
            });
        }

        this.byId = (lis, id, sel, d) => {
            var rpt = "";
            $.each(lis, function (i, item) {
                try {
                    if (item[id] == d) {
                        rpt = item[sel];
                    }
                } catch (e) {}
            });
            return rpt;
        }

        this.getObj = (lis, id, d) => {
            var rpt = "";
            $.each(lis, function (i, item) {
                try {
                    if (item[id] == d) {
                        rpt = item;
                    }
                } catch (e) { }
            });
            return rpt;
        }



    }
    this.Date = new function () {
        this.toDate = (text) => {
            let rpt = new Date(text);
            return rpt;
        }
        this.JustDate = (text) => {
            let fecha = jsUtil.Date.toDate(text);
            //let rpt = `${fecha.getDate()}/${fecha.getMonth()}/${fecha.getFullYear()}`;
            let rpt = fecha.toLocaleDateString();
            return rpt;
        }
    }
};//jsUtils

/*  jsServer  */
var jsServer = new function () {

    this.Request = async function (_route, _data, _type) {
        try {
            let route = _route;
            let data = _data;
            let rpt = await _Ajax(route, data);
            return JSON.parse(rpt.d);
        } catch (ex) {
            jsUtil.Notify.Show(4, "Error en la solicitud.", "jsServer.Request:\n" + ex.message);
            var rptEx = null;
            switch (_type) {
                case 1: rptEx = jsUtil.Default.Var.Obj; break;
                case 2: rptEx = jsUtil.Default.Var.List; break;
                default: jsUtil.Notify.Show(4, "Error, falta definir Response.", ""); break;
            } return rptEx;
        }
    } // _type (1: Obj - 2:Lis)

    /* Internal */
    const _Path = "/app/Method.aspx/";

    const _Ajax = async function (_route, _data) {
        var rpt = null;
        await $.ajax({
            url: _Path.concat(_route),
            type: 'POST',
            async: true,
            dataType: 'json',
            data: JSON.stringify(_data),
            contentType: 'application/json; charSet=UTF-8',
            success: function (data, textStatus, jqXHR) {
                rpt = data;
            },
            error: function (jqXHR, textStatus, exception) {
                rpt = null;
                jsUtil.Notify.Show(4, "Error en Ajax.", "Ajax:\n" + exception);
            },
            beforeSend: function () {
                //console.log("Ajax Before");
                //$.blockUI();
            },
            complete: function () {
                //console.log("Ajax After");
                //$.unblockUI();
            }
        });
        return rpt;
    }
}

/*  jsOwn  */
var jsUserType = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {
                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsUserType.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#UserTypeSet_Name').val();
        ent.ComId = $('#UserTypeSet_Company').val();
        ent.Type = $('#UserTypeSet_Type').is(":checked") ? true : false;
        ent.Active = $('#UserTypeSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { /*jsUtil.Notify.Show(1, "Exito.");*/ }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }

    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("userType_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("userType_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("userType_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("userType_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formUserTypeSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsUserType.Sel(ent.Id);

                    $('#UserTypeSet_Name').val(ent.Name);

                    $('#UserTypeSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formUserTypeSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#UserTypeSet_Name').prop('disabled', false);

                $('#UserTypeSet_Active').prop('checked', true);

                $('#UserTypeSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formUserTypeSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsUserType.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#UserTypeSet_Name').val(ent.Name);
                    $('#UserTypeSet_Name').prop('disabled', false);

                    $('#UserTypeSet_Active').prop('checked', ent.Active);
                    $('#UserTypeSet_Active').prop('disabled', false);

                    $('#UserTypeSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formUserTypeSet = function () {
            try {
                $('#UserTypeSet_Name').val('');
                $('#UserTypeSet_Name').prop('disabled', true);

                $('#UserTypeSet_Active').prop('checked', false);
                $('#UserTypeSet_Active').prop('disabled', true);

                $('#UserTypeSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.Load = async (id, bool) => {

            let ent = await _Obj();
            ent.Active = bool;

            let Lis = await jsUserType.Lis(ent);

            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
}
var jsJob = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsJob.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#JobSet_Name').val();
        ent.Active = $('#JobSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { /*jsUtil.Notify.Show(1, "Exito.");*/ }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }

    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("job_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("job_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("job_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("job_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formJobSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsJob.Sel(ent.Id);
                    $('#JobSet_Name').val(ent.Name);
                    $('#JobSet_Active').prop('checked', ent.Active);
                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Ins = function (modal, form) {
            try {
                default_formJobSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)
                $('#JobSet_Name').val('');
                $('#JobSet_Name').prop('disabled', false);
                $('#JobSet_Active').prop('checked', true);
                $('#JobSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formJobSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsJob.Sel(ent.Id);
                    $('#Id').val(ent.Id);
                    $('#JobSet_Name').val(ent.Name);
                    $('#JobSet_Name').prop('disabled', false);
                    $('#JobSet_Active').prop('checked', ent.Active);
                    $('#JobSet_Active').prop('disabled', false);
                    $('#JobSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formJobSet = function () {
            try {
                $('#JobSet_Name').val('');
                $('#JobSet_Name').prop('disabled', true);
                $('#JobSet_Active').prop('checked', false);
                $('#JobSet_Active').prop('disabled', true);
                $('#JobSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.Load = async (id, bool) => {

            let ent = await _Obj();
            ent.Active = bool;

            let Lis = await jsJob.Lis(ent);

            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
}
var jsCompany = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'NumberDoc', title: 'RUC', visible: true },
        { data: 'Address', title: 'Dirección', visible: false },
        { data: 'Ubigeo', title: 'Distrito', visible: false },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsJob.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#CompanySet_Name').val();
        ent.NumberDoc = $('#CompanySet_NumberDoc').val();
        ent.Address = $('#CompanySet_Address').val();
        ent.Ubigeo = $('#ubi04').val();
        ent.Active = $('#CompanySet_Active').is(":checked") ? true : false;


        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {
        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { /*jsUtil.Notify.Show(1, "Exito.");*/ }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }


    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("company_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("company_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("company_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("company_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formCompanySet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsCompany.Sel(ent.Id);

                    $('#CompanySet_Name').val(ent.Name);

                    $('#CompanySet_NumberDoc').val(ent.NumberDoc);

                    $('#CompanySet_Address').val(ent.Address);

                    let ubigeo = await jsUbigeo.Reverse(ent.Ubigeo);

                    await jsUtil.Select.Country('#ubi01');
                    jsUtil.Select.SetVal('#ubi01', `${ubigeo[0].Id}`);

                    await jsUtil.Select.Down('#ubi01', '#ubi02');
                    jsUtil.Select.SetVal('#ubi02', `${ubigeo[1].Id}`);

                    await jsUtil.Select.Down('#ubi02', '#ubi03');
                    jsUtil.Select.SetVal('#ubi03', `${ubigeo[2].Id}`);

                    await jsUtil.Select.Down('#ubi03', '#ubi04');
                    jsUtil.Select.SetVal('#ubi04', `${ubigeo[3].Id}`);

                    $('#CompanySet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }

            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formCompanySet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#CompanySet_Name').prop('disabled', false);

                $('#CompanySet_NumberDoc').prop('disabled', false);

                $('#CompanySet_Address').prop('disabled', false);

                await jsUtil.Select.Country('#ubi01');
                $('#ubi01').prop('disabled', false);

                await jsUtil.Select.Down('#ubi01', '#ubi02');
                $('#ubi02').prop('disabled', false);

                await jsUtil.Select.Down('#ubi02', '#ubi03');
                $('#ubi03').prop('disabled', false);

                await jsUtil.Select.Down('#ubi03', '#ubi04');
                $('#ubi04').prop('disabled', false);

                $('#CompanySet_Active').prop('checked', true);

                $('#CompanySet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formCompanySet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsCompany.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#CompanySet_Name').val(ent.Name);
                    $('#CompanySet_Name').prop('disabled', false);

                    $('#CompanySet_NumberDoc').val(ent.NumberDoc);
                    $('#CompanySet_NumberDoc').prop('disabled', false);

                    $('#CompanySet_Address').val(ent.Address);
                    $('#CompanySet_Address').prop('disabled', false);

                    let ubigeo = await jsUbigeo.Reverse(ent.Ubigeo);

                    await jsUtil.Select.Country('#ubi01');
                    jsUtil.Select.SetVal('#ubi01', `${ubigeo[0].Id}`);
                    $('#ubi01').prop('disabled', false);

                    await jsUtil.Select.Down('#ubi01', '#ubi02');
                    jsUtil.Select.SetVal('#ubi02', `${ubigeo[1].Id}`);
                    $('#ubi02').prop('disabled', false);

                    await jsUtil.Select.Down('#ubi02', '#ubi03');
                    jsUtil.Select.SetVal('#ubi03', `${ubigeo[2].Id}`);
                    $('#ubi03').prop('disabled', false);

                    await jsUtil.Select.Down('#ubi03', '#ubi04');
                    jsUtil.Select.SetVal('#ubi04', `${ubigeo[3].Id}`);
                    $('#ubi04').prop('disabled', false);

                    $('#CompanySet_Active').prop('checked', ent.Active);
                    $('#CompanySet_Active').prop('disabled', false);

                    $('#CompanySet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formCompanySet = function () {
            try {
                $('#CompanySet_Name').val('');
                $('#CompanySet_Name').prop('disabled', true);

                $('#CompanySet_NumberDoc').val('');
                $('#CompanySet_NumberDoc').prop('disabled', true);

                $('#CompanySet_Address').val('');
                $('#CompanySet_Address').prop('disabled', true);

                $('#ubi01').prop('disabled', true);

                $('#ubi02').prop('disabled', true);

                $('#ubi03').prop('disabled', true);

                $('#ubi04').prop('disabled', true);

                $('#CompanySet_Active').prop('checked', false);
                $('#CompanySet_Active').prop('disabled', true);

                $('#CompanySet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    
    this.$cb = new function () {
        this.get = async (id, bool = true) => {

            let ent = await _Obj();
            ent.Active = bool;
            let Lis = await jsCompany.Lis(ent);

            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
    
}
var jsOffice = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'ComId', title: `Compañia`, visible: true },
        { data: 'Type', title: `Tipo`, visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.Replace.Json(List, lstOfficeType, 'Type', 'Name');
                jsUtil.Replace.Json(List, await jsCompany.Lis(), 'ComId', 'Name');


                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsOffice.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#OfficeSet_Name').val();
        ent.ComId = $('#OfficeSet_Company').val();
        ent.Type = $('#OfficeSet_Type').is(":checked") ? true : false;
        ent.Active = $('#OfficeSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("office_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("office_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("office_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("office_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formOfficeSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsOffice.Sel(ent.Id);

                    $('#OfficeSet_Name').val(ent.Name);

                    let comLis = await jsCompany.Lis();
                    jsUtil.Select.Load('#OfficeSet_Company', comLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#OfficeSet_Company', ent.ComId);

                    let offLis = await jsOffice.Lis();

                    $('#OfficeSet_Type').prop('checked', ent.Type);
                    $('#OfficeSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formOfficeSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#OfficeSet_Name').prop('disabled', false);

                $('#OfficeSet_Company').prop('disabled', false);
                let comLis = await jsCompany.Lis();
                jsUtil.Select.Load('#OfficeSet_Company', comLis, 'Id', 'Name');

                $('#OfficeSet_Type').prop('disabled', false);
                $('#OfficeSet_Type').prop('checked', false);

                $('#OfficeSet_Active').prop('checked', true);

                $('#OfficeSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formOfficeSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsOffice.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#OfficeSet_Name').val(ent.Name);
                    $('#OfficeSet_Name').prop('disabled', false);

                    let comLis = await jsCompany.Lis();
                    jsUtil.Select.Load('#OfficeSet_Company', comLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#OfficeSet_Company', ent.ComId);
                    $('#OfficeSet_Company').prop('disabled', false);

                    $('#OfficeSet_Type').prop('checked', ent.Type);
                    $('#OfficeSet_Type').prop('disabled', false);

                    $('#OfficeSet_Active').prop('checked', ent.Active);
                    $('#OfficeSet_Active').prop('disabled', false);

                    $('#OfficeSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formOfficeSet = function () {
            try {
                $('#OfficeSet_Name').val('');
                $('#OfficeSet_Name').prop('disabled', true);

                $('#OfficeSet_Company').prop('disabled', true);

                $('#OfficeSet_Type').prop('checked', false);
                $('#OfficeSet_Type').prop('disabled', true);

                $('#OfficeSet_Active').prop('checked', false);
                $('#OfficeSet_Active').prop('disabled', true);

                $('#OfficeSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cbOffice = new function () {
        this.get = async (f, c, op, bool = true) => {

            let ent = await _Obj();
            ent.ComId = $(f).val();
            ent.Type = op;
            ent.Active = bool;
            let Lis = await jsOffice.Lis(ent);

            jsUtil.Select.Load(c, Lis, 'Id', 'Name');
        }
        this.getOP = async (c, bool = true) => {

            let ent = await _Obj();
            ent.ComId = 1;
            ent.Type = true;
            ent.Active = bool;
            let Lis = await jsOffice.Lis(ent);

            jsUtil.Select.Load(c, Lis, 'Id', 'Name');
        }
    }
}
var jsUrgency = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsUrgency.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#UrgencySet_Name').val();

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }

    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("urgency_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("urgency_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("urgency_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("urgency_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formUrgencySet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsUrgency.Sel(ent.Id);

                    $('#UrgencySet_Name').val(ent.Name);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formUrgencySet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#UrgencySet_Name').prop('disabled', false);

                $('#UrgencySet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formUrgencySet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsUrgency.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#UrgencySet_Name').val(ent.Name);
                    $('#UrgencySet_Name').prop('disabled', false);

                    $('#UrgencySet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formUrgencySet = function () {
            try {
                $('#UrgencySet_Name').val('');
                $('#UrgencySet_Name').prop('disabled', true);

                $('#UrgencySet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (id, bool = true) => {
            let ent = await _Obj();
            let Lis = await jsUrgency.Lis(ent);
            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
}
var jsImpact = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsImpact.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#ImpactSet_Name').val();

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }

    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("impact_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("impact_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("impact_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("impact_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formImpactSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsImpact.Sel(ent.Id);

                    $('#ImpactSet_Name').val(ent.Name);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formImpactSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#ImpactSet_Name').prop('disabled', false);

                $('#ImpactSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formImpactSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsImpact.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#ImpactSet_Name').val(ent.Name);
                    $('#ImpactSet_Name').prop('disabled', false);

                    $('#ImpactSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formImpactSet = function () {
            try {
                $('#ImpactSet_Name').val('');
                $('#ImpactSet_Name').prop('disabled', true);

                $('#ImpactSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (id, bool = true) => {
            let ent = await _Obj();
            let Lis = await jsImpact.Lis(ent);
            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
}
var jsPriority = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsPriority.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#PrioritySet_Name').val();

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }

    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("priority_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("priority_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("priority_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("priority_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formPrioritySet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsPriority.Sel(ent.Id);

                    $('#PrioritySet_Name').val(ent.Name);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formPrioritySet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#PrioritySet_Name').prop('disabled', false);

                $('#PrioritySet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formPrioritySet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsPriority.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#PrioritySet_Name').val(ent.Name);
                    $('#PrioritySet_Name').prop('disabled', false);

                    $('#PrioritySet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formPrioritySet = function () {
            try {
                $('#PrioritySet_Name').val('');
                $('#PrioritySet_Name').prop('disabled', true);

                $('#PrioritySet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (id, bool = true) => {
            let ent = await _Obj();
            let Lis = await jsPriority.Lis(ent);
            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
}
var jsUbigeo = new function () {

    /* System */
    this.Children = async function (id) {
        try {
            var ent = await _Obj();
            ent.Id = id;
            var req = await _Children(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.List(req)) { return req; }
        } catch (e) { jsUtil.Notify.Show(4, "Error listar ubigeo", "jsUbigeo.Children:\n" + e.message.toString()); }
    }
    this.Reverse = async function (id) {
        try {
            var ent = await _Obj();
            ent.Id = id;
            var req = await _Reverse(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.List(req)) { return req; }
        } catch (e) { jsUtil.Notify.Show(4, "Error reverse ubigeo", "jsUbigeo.Reverse:\n" + e.message.toString()); }
    }

    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("ubigeo_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Children = async function (d) {
        return await jsServer.Request("ubigeo_Children", d, 2);
    } // Get - list of Children
    const _Reverse = async function (d) {
        return await jsServer.Request("ubigeo_Reverse", d, 2);
    } // Get - list of Children
}
var jsPeople = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Dni', title: 'Documento', visible: true },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'ComId', title: 'Compañia', visible: true },
        { data: 'OffId', title: 'Área', visible: true },
        { data: 'JobId', title: 'Puesto', visible: true },
        { data: 'TypeId', title: 'Tipo', visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);
            if (jsUtil.Validate.System.List(List)) {

                jsUtil.Replace.Name(List);
                jsUtil.Replace.Json(List, await jsUserType.Lis(), 'TypeId', 'Name');
                jsUtil.Replace.Json(List, await jsJob.Lis(), 'JobId', 'Name');
                jsUtil.Replace.Json(List, await jsCompany.Lis(), 'ComId', 'Name');
                jsUtil.Replace.Json(List, await jsOffice.Lis(), 'OffId', 'Name');

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsPeople.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {
        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Dni = $('#PeopleSet_Dni').val();
        ent.NameP = $('#PeopleSet_NameP').val();
        ent.NameM = $('#PeopleSet_NameM').val();
        ent.Name = $('#PeopleSet_Name').val();
        ent.Pass = '';
        ent.Birth = $('#PeopleSet_Birth').datepicker('getDate');
        ent.Email = $('#PeopleSet_Email').val();
        ent.Telf1 = $('#PeopleSet_Telf1').val();
        ent.Telf2 = $('#PeopleSet_Telf2').val();
        ent.ComId = $('#PeopleSet_Company').val();
        ent.OffId = $('#PeopleSet_Office').val();
        ent.JobId = $('#PeopleSet_Job').val();
        ent.TypeId = $('#PeopleSet_UserType').val();
        ent.Note = $('#PeopleSet_Note').summernote('code');
        ent.Active = $('#PeopleSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { /*jsUtil.Notify.Show(1, "Exito.");*/ }
        return rpt;
    }
    this.SelName = async function (id) {
        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) {
            return `${rpt.NameP} ${rpt.NameM}, ${rpt.NameP}`;
        }
        return "";
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    this.Auth = async function () {
        try {
            let ent = await _Obj();
            ent.Dni = $("#userLogin").val();
            ent.Pass = $("#passLogin").val();

            let data = { entObj: ent };
            let obj = await _Auth(data);

            if (jsUtil.Validate.System.Obj(obj)) {
                if (obj.Id > 0) { jsUtil.Short.toPage('/Dashboard'); }
                else { jsUtil.Notify.Show(3, obj.Err, obj.ErrCatch); }
            }
        } catch (ex) {
            jsUtil.Notify.Show(4, "Error en Login.", "jsPeople.Login\n" + ex.message.toString());
        }
    } // Login

    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("people_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("people_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("people_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("people_Lis", d, 2);
    } // Get - Object list
    const _Auth = async function (d) {
        let req = await jsServer.Request("people_Auth", d, 1);
        if (req != null) { return req; }
    }

    /*  Interactive  */
    this.$Logout = function () {
        try { jsUtil.Swal.Question("Cerrar Sesion", 2, function () { jsUtil.Short.toPage('/Login'); }); }
        catch (ex) { jsUtil.Notify.Show(4, "Error al cerrar sesison.", "jsPeople.Logout:\n" + ex.message.toString()); }
    } // Close session
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formPeopleSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {

                    ent = await jsPeople.Sel(ent.Id);

                    console.log(ent);

                    $('#PeopleSet_Dni').val(ent.Dni);
                    $('#PeopleSet_NameP').val(ent.NameP);
                    $('#PeopleSet_NameM').val(ent.NameM);
                    $('#PeopleSet_Name').val(ent.Name);

                    let date = new Date(ent.Birth);
                    $('#PeopleSet_Birth').datepicker('update', date);

                    let comLis = await jsCompany.Lis();
                    jsUtil.Select.Load('#PeopleSet_Company', comLis, 'Id', 'Name');
                    await jsUtil.Select.SetVal('#PeopleSet_Company', ent.ComId);

                    await jsOffice.$cbOffice.get('#PeopleSet_Company', '#PeopleSet_Office', false);

                    jsUtil.Select.SetVal('#PeopleSet_Office', ent.OffId);

                    await jsJob.$cb.Load('#PeopleSet_Job', false);
                    jsUtil.Select.SetVal('#PeopleSet_Job', ent.JobId);

                    await jsUserType.$cb.Load('#PeopleSet_UserType', false);
                    jsUtil.Select.SetVal('#PeopleSet_UserType', ent.TypeId);

                    $('#PeopleSet_Email').val(ent.Email);
                    $('#PeopleSet_Telf1').val(ent.Telf1);
                    $('#PeopleSet_Telf2').val(ent.Telf2);

                    $('#PeopleSet_Note').summernote('code', ent.Note);

                    $('#PeopleSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {

                var ent = await _Obj();
                console.log(ent);

                default_formPeopleSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#PeopleSet_Dni').val('');
                $('#PeopleSet_Dni').prop('disabled', false);

                $('#PeopleSet_NameP').val('');
                $('#PeopleSet_NameP').prop('disabled', false);

                $('#PeopleSet_NameM').val('');
                $('#PeopleSet_NameM').prop('disabled', false);

                $('#PeopleSet_Name').val('');
                $('#PeopleSet_Name').prop('disabled', false);

                $('#PeopleSet_Birth').prop('disabled', false);

                let comLis = await jsCompany.Lis();
                jsUtil.Select.Load('#PeopleSet_Company', comLis, 'Id', 'Name');
                $('#PeopleSet_Company').prop('disabled', false);

                await jsOffice.$cbOffice.get('#PeopleSet_Company', '#PeopleSet_Office');
                $('#PeopleSet_Office').prop('disabled', false);

                await jsJob.$cb.Load('#PeopleSet_Job', true);
                $('#PeopleSet_Job').prop('disabled', false);

                await jsUserType.$cb.Load('#PeopleSet_UserType', true);
                $('#PeopleSet_UserType').prop('disabled', false);

                $('#PeopleSet_Email').val('');
                $('#PeopleSet_Email').prop('disabled', false);

                $('#PeopleSet_Telf1').val('');
                $('#PeopleSet_Telf1').prop('disabled', false);

                $('#PeopleSet_Telf2').val('');
                $('#PeopleSet_Telf2').prop('disabled', false);

                $('#PeopleSet_Note').summernote('code', '');
                $('#PeopleSet_Note').summernote('enable');

                $('#PeopleSet_Active').prop('checked', true);

                $('#PeopleSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formPeopleSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {

                    ent = await jsPeople.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#PeopleSet_Dni').val(ent.Dni);
                    $('#PeopleSet_Dni').prop('disabled', false);

                    $('#PeopleSet_NameP').val(ent.NameP);
                    $('#PeopleSet_NameP').prop('disabled', false);

                    $('#PeopleSet_NameM').val(ent.NameM);
                    $('#PeopleSet_NameM').prop('disabled', false);

                    $('#PeopleSet_Name').val(ent.Name);
                    $('#PeopleSet_Name').prop('disabled', false);

                    let date = new Date(ent.Birth);
                    $('#PeopleSet_Birth').datepicker('update', date);
                    $('#PeopleSet_Birth').prop('disabled', false);

                    let comLis = await jsCompany.Lis();
                    jsUtil.Select.Load('#PeopleSet_Company', comLis, 'Id', 'Name');
                    await jsUtil.Select.SetVal('#PeopleSet_Company', ent.ComId);
                    $('#PeopleSet_Company').prop('disabled', false);

                    await jsOffice.$cbOffice.get('#PeopleSet_Company', '#PeopleSet_Office');
                    jsUtil.Select.SetVal('#PeopleSet_Office', ent.OffId);
                    $('#PeopleSet_Office').prop('disabled', false);

                    await jsJob.$cb.Load('#PeopleSet_Job', true);
                    jsUtil.Select.SetVal('#PeopleSet_Job', ent.JobId);
                    $('#PeopleSet_Job').prop('disabled', false);

                    await jsUserType.$cb.Load('#PeopleSet_UserType', true);
                    jsUtil.Select.SetVal('#PeopleSet_UserType', ent.TypeId);
                    $('#PeopleSet_UserType').prop('disabled', false);
                    
                    $('#PeopleSet_Email').val(ent.Email);
                    $('#PeopleSet_Email').prop('disabled', false);

                    $('#PeopleSet_Telf1').val(ent.Telf1);
                    $('#PeopleSet_Telf1').prop('disabled', false);

                    $('#PeopleSet_Telf2').val(ent.Telf2);
                    $('#PeopleSet_Telf2').prop('disabled', false);

                    $('#PeopleSet_Note').summernote('code', ent.Note);
                    $('#PeopleSet_Note').summernote('enable');

                    $('#PeopleSet_Active').prop('checked', ent.Active);
                    $('#PeopleSet_Active').prop('disabled', false);
                    
                    $('#PeopleSet_Submit').show();
                    
                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formPeopleSet = function () {
            try {
                $('#PeopleSet_Dni').val('');
                $('#PeopleSet_Dni').prop('disabled', true);

                $('#PeopleSet_NameP').val('');
                $('#PeopleSet_NameP').prop('disabled', true);

                $('#PeopleSet_NameM').val('');
                $('#PeopleSet_NameM').prop('disabled', true);

                $('#PeopleSet_Name').val('');
                $('#PeopleSet_Name').prop('disabled', true);

                //$('#PeopleSet_Birth').datepicker('update', '');
                $('#PeopleSet_Birth').prop('disabled', true);

                $('#PeopleSet_Company').prop('disabled', true);
                $('#PeopleSet_Office').prop('disabled', true);
                $('#PeopleSet_Job').prop('disabled', true);
                $('#PeopleSet_UserType').prop('disabled', true);

                $('#PeopleSet_Email').val('');
                $('#PeopleSet_Email').prop('disabled', true);

                $('#PeopleSet_Telf1').val('');
                $('#PeopleSet_Telf1').prop('disabled', true);

                $('#PeopleSet_Telf2').val('');
                $('#PeopleSet_Telf2').prop('disabled', true);

                $('#PeopleSet_Note').summernote('code', '');
                $('#PeopleSet_Note').summernote('disable');

                $('#PeopleSet_Active').prop('checked', false);
                $('#PeopleSet_Active').prop('disabled', true);

                $('#PeopleSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.getOff = async (f, c, bool = true) => {

            let ent = await _Obj();
            ent.OffId = $(f).val();
            ent.Active = bool;
            let Lis = await jsPeople.Lis(ent);

            $.each(Lis, function (i, item) {
                item['Name'] = item['NameP'] + ' ' + item['NameM'] + ', ' + item['Name'];
            });

            jsUtil.Select.Load(c, Lis, 'Id', 'Name');
        }
    }
}
var jsServiceType = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.Obj = async function () {
        return await _Obj();
    }
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsServiceType.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#ServiceTypeSet_Name').val();
        ent.Active = $('#ServiceTypeSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("serviceType_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("serviceType_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("serviceType_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("serviceType_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formServiceTypeSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsServiceType.Sel(ent.Id);

                    $('#ServiceTypeSet_Name').val(ent.Name);

                    $('#ServiceTypeSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formServiceTypeSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#ServiceTypeSet_Name').prop('disabled', false);

                $('#ServiceTypeSet_Company').prop('disabled', false);
                let comLis = await jsCompany.Lis();
                jsUtil.Select.Load('#ServiceTypeSet_Company', comLis, 'Id', 'Name');

                $('#ServiceTypeSet_Type').prop('disabled', false);
                $('#ServiceTypeSet_Type').prop('checked', false);

                $('#ServiceTypeSet_Active').prop('checked', true);

                $('#ServiceTypeSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formServiceTypeSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsServiceType.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#ServiceTypeSet_Name').val(ent.Name);
                    $('#ServiceTypeSet_Name').prop('disabled', false);

                    let comLis = await jsCompany.Lis();
                    jsUtil.Select.Load('#ServiceTypeSet_Company', comLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#ServiceTypeSet_Company', ent.ComId);
                    $('#ServiceTypeSet_Company').prop('disabled', false);

                    $('#ServiceTypeSet_Type').prop('checked', ent.Type);
                    $('#ServiceTypeSet_Type').prop('disabled', false);

                    $('#ServiceTypeSet_Active').prop('checked', ent.Active);
                    $('#ServiceTypeSet_Active').prop('disabled', false);

                    $('#ServiceTypeSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formServiceTypeSet = function () {
            try {
                $('#ServiceTypeSet_Name').val('');
                $('#ServiceTypeSet_Name').prop('disabled', true);

                $('#ServiceTypeSet_Active').prop('checked', false);
                $('#ServiceTypeSet_Active').prop('disabled', true);

                $('#ServiceTypeSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (id, bool = true) => {

            let ent = await _Obj();
            ent.Active = bool;
            let Lis = await jsServiceType.Lis(ent);

            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
}
var jsService = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'Type', title: `Tipo`, visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {
                jsUtil.Replace.Json(List, await jsServiceType.Lis(), 'Type', 'Name');
                
                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsService.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {
        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#ServiceSet_Name').val();
        ent.Type = $('#ServiceSet_Type').val();
        ent.Active = $('#ServiceSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("service_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("service_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("service_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("service_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formServiceSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsService.Sel(ent.Id);

                    $('#ServiceSet_Name').val(ent.Name);

                    let serviceTypeLis = await jsServiceType.Lis();
                    jsUtil.Select.Load('#ServiceSet_Type', serviceTypeLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#ServiceSet_Type', ent.Type);

                    $('#ServiceSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formServiceSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#ServiceSet_Name').prop('disabled', false);

                $('#ServiceSet_Type').prop('disabled', false);
                let objST = await jsServiceType.Obj();
                objST.Active = true;
                let serviceTypeLis = await jsServiceType.Lis(objST);
                jsUtil.Select.Load('#ServiceSet_Type', serviceTypeLis, 'Id', 'Name');

                $('#ServiceSet_Active').prop('checked', true);

                $('#ServiceSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formServiceSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsService.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#ServiceSet_Name').val(ent.Name);
                    $('#ServiceSet_Name').prop('disabled', false);

                    $('#ServiceSet_Type').prop('disabled', false);
                    let objST = await jsServiceType.Obj();
                    objST.Active = true;
                    let serviceTypeLis = await jsServiceType.Lis(objST);
                    jsUtil.Select.Load('#ServiceSet_Type', serviceTypeLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#ServiceSet_Type', ent.Type);

                    $('#ServiceSet_Active').prop('checked', ent.Active);
                    $('#ServiceSet_Active').prop('disabled', false);

                    $('#ServiceSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formServiceSet = function () {
            try {
                $('#ServiceSet_Name').val('');
                $('#ServiceSet_Name').prop('disabled', true);

                $('#ServiceSet_Type').prop('disabled', true);

                $('#ServiceSet_Active').prop('checked', false);
                $('#ServiceSet_Active').prop('disabled', true);

                $('#ServiceSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb= new function () {
        this.get = async (f, c, bool = true) => {

            let ent = await _Obj();
            ent.Type = ($(f).val() == null) ? 0 : $(f).val();
            ent.Active = bool;
            let Lis = await jsService.Lis(ent);

            jsUtil.Select.Load(c, Lis, 'Id', 'Name');
        }
    }
}
var jsSlaType = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.Obj = async function () {
        return await _Obj();
    }
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsSlaType.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#SlaTypeSet_Name').val();
        ent.Active = $('#SlaTypeSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("slaType_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("slaType_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("slaType_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("slaType_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formSlaTypeSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsSlaType.Sel(ent.Id);

                    $('#SlaTypeSet_Name').val(ent.Name);

                    $('#SlaTypeSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formSlaTypeSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#SlaTypeSet_Name').prop('disabled', false);

                $('#SlaTypeSet_Company').prop('disabled', false);
                let comLis = await jsCompany.Lis();
                jsUtil.Select.Load('#SlaTypeSet_Company', comLis, 'Id', 'Name');

                $('#SlaTypeSet_Type').prop('disabled', false);
                $('#SlaTypeSet_Type').prop('checked', false);

                $('#SlaTypeSet_Active').prop('checked', true);

                $('#SlaTypeSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formSlaTypeSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsSlaType.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#SlaTypeSet_Name').val(ent.Name);
                    $('#SlaTypeSet_Name').prop('disabled', false);

                    let comLis = await jsCompany.Lis();
                    jsUtil.Select.Load('#SlaTypeSet_Company', comLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#SlaTypeSet_Company', ent.ComId);
                    $('#SlaTypeSet_Company').prop('disabled', false);

                    $('#SlaTypeSet_Type').prop('checked', ent.Type);
                    $('#SlaTypeSet_Type').prop('disabled', false);

                    $('#SlaTypeSet_Active').prop('checked', ent.Active);
                    $('#SlaTypeSet_Active').prop('disabled', false);

                    $('#SlaTypeSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formSlaTypeSet = function () {
            try {
                $('#SlaTypeSet_Name').val('');
                $('#SlaTypeSet_Name').prop('disabled', true);

                $('#SlaTypeSet_Active').prop('checked', false);
                $('#SlaTypeSet_Active').prop('disabled', true);

                $('#SlaTypeSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (id, bool = true) => {

            let ent = await _Obj();
            ent.Active = bool;
            let Lis = await jsSlaType.Lis(ent);

            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }
}
var jsSla = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'Type', title: `Tipo`, visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {
                jsUtil.Replace.Json(List, await jsSlaType.Lis(), 'Type', 'Name');

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsSla.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {
        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Name = $('#SlaSet_Name').val();
        ent.Type = $('#SlaSet_Type').val();
        ent.Active = $('#SlaSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("sla_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("sla_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("sla_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("sla_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formSlaSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsSla.Sel(ent.Id);

                    $('#SlaSet_Name').val(ent.Name);

                    let SlaTypeLis = await jsSlaType.Lis();
                    jsUtil.Select.Load('#SlaSet_Type', SlaTypeLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#SlaSet_Type', ent.Type);

                    $('#SlaSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formSlaSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#SlaSet_Name').prop('disabled', false);

                $('#SlaSet_Type').prop('disabled', false);
                let objST = await jsSlaType.Obj();
                objST.Active = true;
                let SlaTypeLis = await jsSlaType.Lis(objST);
                jsUtil.Select.Load('#SlaSet_Type', SlaTypeLis, 'Id', 'Name');

                $('#SlaSet_Active').prop('checked', true);

                $('#SlaSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formSlaSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsSla.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#SlaSet_Name').val(ent.Name);
                    $('#SlaSet_Name').prop('disabled', false);

                    $('#SlaSet_Type').prop('disabled', false);
                    let objST = await jsSlaType.Obj();
                    objST.Active = true;
                    let SlaTypeLis = await jsSlaType.Lis(objST);
                    jsUtil.Select.Load('#SlaSet_Type', SlaTypeLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#SlaSet_Type', ent.Type);

                    $('#SlaSet_Active').prop('checked', ent.Active);
                    $('#SlaSet_Active').prop('disabled', false);

                    $('#SlaSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formSlaSet = function () {
            try {
                $('#SlaSet_Name').val('');
                $('#SlaSet_Name').prop('disabled', true);

                $('#SlaSet_Type').prop('disabled', true);

                $('#SlaSet_Active').prop('checked', false);
                $('#SlaSet_Active').prop('disabled', true);

                $('#SlaSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (f, c, bool = true) => {

            let ent = await _Obj();
            ent.Type = ($(f).val() == null) ? 0 : $(f).val();
            ent.Active = bool;
            let Lis = await jsSla.Lis(ent);

            jsUtil.Select.Load(c, Lis, 'Id', 'Name');
        }
    }
}
var jsComSvc = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'SerId', title: 'Servicio', visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table, cbo) {
        try {

            let com = $(cbo).val();
            $('#Com').val(com);
            if (com != null) {
                let ent = await _Obj();
                ent.ComId = $(cbo).val();
                var data = jsUtil.Default.Req.entObj(ent);
                var List = await _Lis(data);

                if (jsUtil.Validate.System.List(List)) {

                    jsUtil.Replace.Json(List, await jsService.Lis(), 'SerId', 'Name');

                    jsUtil.DataTable.set(table, _entTbl, List);
                    jsUtil.Notify.Show(1, "Listado Exitoso");

                } else {
                    jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
                }
            } else {
                jsUtil.Notify.Show(3, "Seleccionar una empresa");
            }
            


        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsSla.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {
        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.ComId = $('#Com').val();
        ent.SerId = $('#ComSvcSet_Service').val();
        ent.Active = $('#ComSvcSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar?` : `¿Seguro de actualizar?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("comSvc_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("comSvc_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("comSvc_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("comSvc_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formComSvcSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsComSvc.Sel(ent.Id);

                    let ComSvcLis = await jsService.Lis();
                    jsUtil.Select.Load('#ComSvcSet_Service', ComSvcLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#ComSvcSet_Service', ent.SerId);
                    
                    $('#ComSvcSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formComSvcSet();

                $('.opc').show();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#ComSvcSet_ServiceType').prop('disabled', false);
                await jsServiceType.$cb.get('#ComSvcSet_ServiceType');

                $('#ComSvcSet_Service').prop('disabled', false);
                await jsService.$cb.get('#ComSvcSet_ServiceType', '#ComSvcSet_Service');

                $('#ComSvcSet_Active').prop('checked', true);
                $('#ComSvcSet_Active').prop('disabled', true);

                $('#ComSvcSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formComSvcSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsComSvc.Sel(ent.Id);

                    $('#Id').val(ent.Id);
                    
                    let ComSvcLis = await jsService.Lis();
                    jsUtil.Select.Load('#ComSvcSet_Service', ComSvcLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#ComSvcSet_Service', ent.SerId);

                    $('#ComSvcSet_Active').prop('checked', ent.Active);
                    $('#ComSvcSet_Active').prop('disabled', false);

                    $('#ComSvcSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formComSvcSet = function () {
            try {
                $('.opc').hide();
                $('#ComSvcSet_ServiceType').prop('disabled', true);

                $('#ComSvcSet_Service').prop('disabled', true);

                $('#ComSvcSet_Active').prop('checked', false);
                $('#ComSvcSet_Active').prop('disabled', true);

                $('#ComSvcSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (f, c, bool = true) => {

            let ent = await _Obj();
            ent.ComId = $(f).val();
            ent.Active = bool;
            let Lis = await jsComSvc.Lis(ent);

            $.each(Lis, function (i, item) {
                item['ComId'] = item['SerId'];
            });

            jsUtil.Replace.Json(Lis, await jsService.Lis(), 'ComId', 'Name');

            jsUtil.Select.Load(c, Lis, 'SerId', 'ComId');
        }
    }
}
var jsComSvcSla = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Sla', title: 'SLA', visible: true },
        { data: 'Data', title: 'Valor', visible: true },
        { data: 'Active', title: 'Estado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table, tbl) {
        try {
            let Svc = jsUtil.DataTable.getSelect(tbl);

            if (Svc > 0) {

                $('#Svc').val(Svc);

                if ($('#Com').val() != null) {

                    let ent = await _Obj();
                    ent.ComSvc = Svc;
                    var data = jsUtil.Default.Req.entObj(ent);
                    var List = await _Lis(data);

                    if (jsUtil.Validate.System.List(List)) {

                        jsUtil.Replace.Json(List, await jsSla.Lis(), 'Sla', 'Name');

                        jsUtil.DataTable.set(table, _entTbl, List);
                        jsUtil.Notify.Show(1, "Listado Exitoso");

                    } else {
                        jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
                    }

                } else {
                    jsUtil.Notify.Show(3, "Seleccionar una empresa");
                }
            }

        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsSla.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {
        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.ComSvc = $('#Svc').val();
        ent.Data = $('#ComSvcSlaSet_Data').val();
        ent.Sla = $('#ComSvcSlaSet_Sla').val();
        ent.Active = $('#ComSvcSlaSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar?` : `¿Seguro de actualizar?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("comSvcSla_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("comSvcSla_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("comSvcSla_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("comSvcSla_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Ins = async function (modal, form) {
            try {

                default_formComSvcSlaSet();

                $('.opc').show();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#ComSvcSlaSet_SlaType').prop('disabled', false);
                await jsSlaType.$cb.get('#ComSvcSlaSet_SlaType');

                $('#ComSvcSlaSet_Sla').prop('disabled', false);
                await jsSla.$cb.get('#ComSvcSlaSet_SlaType', '#ComSvcSlaSet_Sla');

                $('#ComSvcSlaSet_Data').prop('disabled', false);

                $('#ComSvcSlaSet_Active').prop('checked', true);
                $('#ComSvcSlaSet_Active').prop('disabled', true);

                $('#ComSvcSlaSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formComSvcSlaSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsComSvcSla.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    let ComSvcSlaLis = await jsSla.Lis();
                    jsUtil.Select.Load('#ComSvcSlaSet_Sla', ComSvcSlaLis, 'Id', 'Name');
                    jsUtil.Select.SetVal('#ComSvcSlaSet_Sla', ent.Sla);

                    $('#ComSvcSlaSet_Data').prop('disabled', false);
                    $('#ComSvcSlaSet_Data').val(ent.Data);

                    $('#ComSvcSlaSet_Active').prop('checked', ent.Active);
                    $('#ComSvcSlaSet_Active').prop('disabled', false);

                    $('#ComSvcSlaSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formComSvcSlaSet = function () {
            try {
                $('.opc').hide();
                $('#ComSvcSlaSet_SlaType').prop('disabled', true);
                $('#ComSvcSlaSet_Sla').prop('disabled', true);

                $('#ComSvcSlaSet_Data').val();
                $('#ComSvcSlaSet_Data').prop('disabled', true);

                $('#ComSvcSlaSet_Active').prop('checked', false);
                $('#ComSvcSlaSet_Active').prop('disabled', true);

                $('#ComSvcSlaSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
    this.$cb = new function () {
        this.get = async (f, c, op, bool = true) => {

            let ent = await _Obj();
            ent.Type = $(f).val();
            ent.Active = bool;
            let Lis = await jsSla.Lis(ent);

            jsUtil.Select.Load(c, Lis, 'Id', 'Name');
        }
    }
}

var jsIncident = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Cod.', visible: true },
        { data: 'Subject', title: 'Asunto', visible: true },
        { data: 'State', title: 'Estado', visible: true },
        { data: 'ComId', title: 'Empresa', visible: true },
        { data: 'PeoId', title: 'Reportador', visible: true },
        { data: 'SerId', title: 'Servicio', visible: true },
        { data: 'UrgId', title: 'Urgencia', visible: true },
        { data: 'ImpId', title: 'Importancia', visible: true },
        { data: 'PriId', title: 'Prioridad', visible: true },
        { data: 'Scale', title: 'Área', visible: true },
        { data: 'ScaleType', title: 'Jerárquico', visible: true },
        { data: 'Expert', title: 'Encargado', visible: true },
        { data: 'Sla', title: 'Cumple', visible: true },
        { data: 'Min', title: 'Minutos', visible: true },
        { data: 'Likert', title: 'Satisfacción', visible: true },
        { data: 'Date', title: 'Creado', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.Replace.Json(List, await jsOffice.Lis(), 'Scale', 'Name');
                jsUtil.Replace.Json(List, await jsUrgency.Lis(), 'UrgId', 'Name');
                jsUtil.Replace.Json(List, await jsPriority.Lis(), 'PriId', 'Name');
                jsUtil.Replace.Json(List, await jsImpact.Lis(), 'ImpId', 'Name');
                jsUtil.Replace.Json(List, await jsCompany.Lis(), 'ComId', 'Name');
                jsUtil.Replace.Json(List, await jsService.Lis(), 'SerId', 'Name');
                jsUtil.Replace.Json(List, await jsPeople.Lis(), 'PeoId', 'Dni');
                jsUtil.Replace.Json(List, await jsPeople.Lis(), 'Expert', 'Dni');
                jsUtil.Replace.Json(List, lstStatusAll, 'State', 'Name');
                jsUtil.Replace.Json(List, lstSiNo, 'Sla', 'Name');
                jsUtil.Replace.Json(List, lstSiNo, 'ScaleType', 'Name');

                jsUtil.Replace.Json(List, lstLikert, 'Likert', 'Name');



                $.each(List, function (i, item) {
                    let date = new Date(item['Date']);
                    item['Date'] = `${addZero(date.getDate())}/${addZero(date.getMonth() + 1)}/${addZero(date.getFullYear())} ` + formatAMPM(date);
                });


                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsIncident.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Subject = $('#Incident_Asunto').val();
        ent.Origin = $('#Incident_Origen').val();
        ent.State = $('#Incident_Status').val();
        ent.ComId = $('#Incident_Company').val();
        ent.OffId = $('#Incident_Office').val();
        ent.PeoId = $('#Incident_People').val();
        ent.SerId = $('#Incident_Service').val();
        ent.UrgId = $('#Incident_Urgency').val();
        ent.ImpId = $('#Incident_Impact').val();
        ent.PriId = $('#Incident_Priority').val();

        if (ent.Id > 0) {
            ent.Origin = 0;
            ent.ComId = 0;
            ent.OffId = 0;
            ent.PeoId = 0;
            ent.SerId = 0;
        }


        ent.Expert = null;

        var entDel = await jsIncidentExpert.Obj();
        entDel.Id = 0;
        entDel.NoteInt = $("#Incident_NoteInt").summernote('code');
        entDel.NoteExt = $("#Incident_NoteExt").summernote('code');
        entDel.Time = $("#Incident_Time").val();
        entDel.PeoId = $("#sId").val();

        if (ent.State == 3) {
            ent.Scale = null;
            ent.ScaleType = false;
        } else {
            if ($('#Incident_ScaleType').is(":checked")) {
                ent.Scale = $('#sOff').val();
                ent.ScaleType = true;
            } else {
                ent.Scale = $('#Incident_Scale').val();
                ent.ScaleType = false;
            }
        }

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Subject}?` : `¿Seguro de actualizar ${ent.Subject}?`);

        jsUtil.Swal.Question(msg, 2, async function () {

            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
                     
            entDel.IncId = (ent.Id > 0) ? ent.Id : rpt.Id;

            jsIncidentExpert.Set(entDel);
            if (jsUtil.Validate.System.Obj(rpt)) {
                jsUtil.Short.toPage("/");
            }
        });
    }
    this.Sel = async function (id) {
        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { /*jsUtil.Notify.Show(1, "Exito.");*/ }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }

    this.Likert = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Subject = "";
        ent.Origin = 0;
        ent.ComId = 0;
        ent.PeoId = 0;
        ent.SerId = 0;
        ent.UrgId = 0;
        ent.ImpId = 0;
        ent.PriId = 0;

        ent.Origin = 0;
        ent.ComId = 0;
        ent.PeoId = 0;
        ent.SerId = 0;

        ent.OffId = 0;
        ent.State = 4;
        ent.Likert = $('#IncidentLikertSet_Likert').data('from') + 1 ;


        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Subject}?` : `¿Seguro de actualizar ${ent.Subject}?`);

        jsUtil.Swal.Question(msg, 2, async function () {

            let rpt = await _Likert(jsUtil.Default.Req.entObj(ent));

            if (jsUtil.Validate.System.Obj(rpt)) {
                jsUtil.Short.F5();
            }
        });
    }

    this.Expert = async function () {

        var ent = await _Obj();
        ent.Id = $('#Id').val();
        ent.Subject = "";
        ent.Origin = 0;
        ent.SerId = 0;
        ent.UrgId = 0;
        ent.ImpId = 0;
        ent.PriId = 0;
        ent.Origin = 0;
        ent.PeoId = 0;
        ent.SerId = 0;
        ent.State = 0;
        ent.Likert = 0;
        ent.ComId = 0;

        ent.Expert = $('#sId').val();
        ent.Scale = $('#sOff').val();
        ent.ScaleType = false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Subject}?` : `¿Seguro de actualizar ${ent.Subject}?`);

        jsUtil.Swal.Question(msg, 2, async function () {

            let rpt = await _Expert(jsUtil.Default.Req.entObj(ent));

            if (jsUtil.Validate.System.Obj(rpt)) {
                jsUtil.Short.F5();
            }
        });
    }


    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("incident_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("incident_Set", d, 1);
    } // Get - Selected object 
    const _Likert = async function (d) {
        return await jsServer.Request("incident_Likert", d, 1);
    } // Get - Selected object 
    const _Expert = async function (d) {
        return await jsServer.Request("incident_Expert", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("incident_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("incident_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formIncidentSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsIncident.Sel(ent.Id);

                    $('#IncidentSet_Name').val(ent.Name);

                    $('#IncidentSet_NumberDoc').val(ent.NumberDoc);

                    $('#IncidentSet_Address').val(ent.Address);

                    let ubigeo = await jsUbigeo.Reverse(ent.Ubigeo);

                    await jsUtil.Select.Country('#ubi01');
                    jsUtil.Select.SetVal('#ubi01', `${ubigeo[0].Id}`);

                    await jsUtil.Select.Down('#ubi01', '#ubi02');
                    jsUtil.Select.SetVal('#ubi02', `${ubigeo[1].Id}`);

                    await jsUtil.Select.Down('#ubi02', '#ubi03');
                    jsUtil.Select.SetVal('#ubi03', `${ubigeo[2].Id}`);

                    await jsUtil.Select.Down('#ubi03', '#ubi04');
                    jsUtil.Select.SetVal('#ubi04', `${ubigeo[3].Id}`);

                    $('#IncidentSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }

            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formIncidentSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#IncidentSet_Name').prop('disabled', false);

                $('#IncidentSet_NumberDoc').prop('disabled', false);

                $('#IncidentSet_Address').prop('disabled', false);

                await jsUtil.Select.Country('#ubi01');
                $('#ubi01').prop('disabled', false);

                await jsUtil.Select.Down('#ubi01', '#ubi02');
                $('#ubi02').prop('disabled', false);

                await jsUtil.Select.Down('#ubi02', '#ubi03');
                $('#ubi03').prop('disabled', false);

                await jsUtil.Select.Down('#ubi03', '#ubi04');
                $('#ubi04').prop('disabled', false);

                $('#IncidentSet_Active').prop('checked', true);

                $('#IncidentSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }
        this.Upd = async function (modal, form, tbl) {
            try {
                default_formIncidentSet();

                $(`${modal} h4`).text('Editar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsIncident.Sel(ent.Id);

                    $('#Id').val(ent.Id);

                    $('#IncidentSet_Name').val(ent.Name);
                    $('#IncidentSet_Name').prop('disabled', false);

                    $('#IncidentSet_NumberDoc').val(ent.NumberDoc);
                    $('#IncidentSet_NumberDoc').prop('disabled', false);

                    $('#IncidentSet_Address').val(ent.Address);
                    $('#IncidentSet_Address').prop('disabled', false);

                    let ubigeo = await jsUbigeo.Reverse(ent.Ubigeo);

                    await jsUtil.Select.Country('#ubi01');
                    jsUtil.Select.SetVal('#ubi01', `${ubigeo[0].Id}`);
                    $('#ubi01').prop('disabled', false);

                    await jsUtil.Select.Down('#ubi01', '#ubi02');
                    jsUtil.Select.SetVal('#ubi02', `${ubigeo[1].Id}`);
                    $('#ubi02').prop('disabled', false);

                    await jsUtil.Select.Down('#ubi02', '#ubi03');
                    jsUtil.Select.SetVal('#ubi03', `${ubigeo[2].Id}`);
                    $('#ubi03').prop('disabled', false);

                    await jsUtil.Select.Down('#ubi03', '#ubi04');
                    jsUtil.Select.SetVal('#ubi04', `${ubigeo[3].Id}`);
                    $('#ubi04').prop('disabled', false);

                    $('#IncidentSet_Active').prop('checked', ent.Active);
                    $('#IncidentSet_Active').prop('disabled', false);

                    $('#IncidentSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formIncidentSet = function () {
            try {
                $('#IncidentSet_Name').val('');
                $('#IncidentSet_Name').prop('disabled', true);

                $('#IncidentSet_NumberDoc').val('');
                $('#IncidentSet_NumberDoc').prop('disabled', true);

                $('#IncidentSet_Address').val('');
                $('#IncidentSet_Address').prop('disabled', true);

                $('#ubi01').prop('disabled', true);

                $('#ubi02').prop('disabled', true);

                $('#ubi03').prop('disabled', true);

                $('#ubi04').prop('disabled', true);

                $('#IncidentSet_Active').prop('checked', false);
                $('#IncidentSet_Active').prop('disabled', true);

                $('#IncidentSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }

    this.$cb = new function () {
        this.get = async (id, bool = true) => {

            let ent = await _Obj();
            ent.Active = bool;
            let Lis = await jsIncident.Lis(ent);

            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }

}

var jsIncidentExpert = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'NoteInt', title: 'Nota Interna', visible: true },
        { data: 'NoteExt', title: 'Nota Externa', visible: true },
        { data: 'Time', title: 'Minutos', visible: true },
        { data: 'Date', title: 'Fecha', visible: true },
        { data: 'PeoId', title: 'Encargado', visible: true },
        { data: 'IncId', title: 'Incidencia', visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table) {
        try {
            let ent = await _Obj();
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {

                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsIncidentExpert.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Obj = async function () {
        return await _Obj();
    }
    this.Set = async function ( d = null) {

        if (d == null) {
            var ent = await _Obj();
            ent.Id = $('#Id').val();
            ent.Name = $('#IncidentExpertSet_Name').val();
            ent.NumberDoc = $('#IncidentExpertSet_NumberDoc').val();
            ent.Address = $('#IncidentExpertSet_Address').val();
            ent.Ubigeo = $('#ubi04').val();
            ent.Active = $('#IncidentExpertSet_Active').is(":checked") ? true : false;


            let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

            jsUtil.Swal.Question(msg, 2, async function () {
                let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
                if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
            });
        } else {
            let rpt = await _Set(jsUtil.Default.Req.entObj(d));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        }
        
    }
    this.Sel = async function (id) {
        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { /*jsUtil.Notify.Show(1, "Exito.");*/ }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }


    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("incidentExpert_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("incidentExpert_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("incidentExpert_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("incidentExpert_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Sel = async function (modal, form, tbl) {
            try {
                default_formIncidentExpertSet();

                $(`${modal} h4`).text('Ver');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsIncidentExpert.Sel(ent.Id);

                    $('#IncidentExpertSet_Name').val(ent.Name);

                    $('#IncidentExpertSet_NumberDoc').val(ent.NumberDoc);

                    $('#IncidentExpertSet_Address').val(ent.Address);

                    let ubigeo = await jsUbigeo.Reverse(ent.Ubigeo);

                    await jsUtil.Select.Country('#ubi01');
                    jsUtil.Select.SetVal('#ubi01', `${ubigeo[0].Id}`);

                    await jsUtil.Select.Down('#ubi01', '#ubi02');
                    jsUtil.Select.SetVal('#ubi02', `${ubigeo[1].Id}`);

                    await jsUtil.Select.Down('#ubi02', '#ubi03');
                    jsUtil.Select.SetVal('#ubi03', `${ubigeo[2].Id}`);

                    await jsUtil.Select.Down('#ubi03', '#ubi04');
                    jsUtil.Select.SetVal('#ubi04', `${ubigeo[3].Id}`);

                    $('#IncidentExpertSet_Active').prop('checked', ent.Active);

                    jsUtil.Modal.Show(modal);
                }

            } catch (e) {
                jsUtil.Modal.Close(modal);
                console.log(e);
            }
        }
        this.Ins = async function (modal, form) {
            try {
                default_formIncidentExpertSet();

                $(`${modal} h4`).text('Agregar');

                $('#Id').val(0)

                $('#IncidentExpertSet_Name').prop('disabled', false);

                $('#IncidentExpertSet_NumberDoc').prop('disabled', false);

                $('#IncidentExpertSet_Address').prop('disabled', false);

                await jsUtil.Select.Country('#ubi01');
                $('#ubi01').prop('disabled', false);

                await jsUtil.Select.Down('#ubi01', '#ubi02');
                $('#ubi02').prop('disabled', false);

                await jsUtil.Select.Down('#ubi02', '#ubi03');
                $('#ubi03').prop('disabled', false);

                await jsUtil.Select.Down('#ubi03', '#ubi04');
                $('#ubi04').prop('disabled', false);

                $('#IncidentExpertSet_Active').prop('checked', true);

                $('#IncidentExpertSet_Submit').show();

                jsUtil.Modal.Show(modal);

            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formIncidentExpertSet = function () {
            try {
                $('#IncidentExpertSet_Name').val('');
                $('#IncidentExpertSet_Name').prop('disabled', true);

                $('#IncidentExpertSet_NumberDoc').val('');
                $('#IncidentExpertSet_NumberDoc').prop('disabled', true);

                $('#IncidentExpertSet_Address').val('');
                $('#IncidentExpertSet_Address').prop('disabled', true);

                $('#ubi01').prop('disabled', true);

                $('#ubi02').prop('disabled', true);

                $('#ubi03').prop('disabled', true);

                $('#ubi04').prop('disabled', true);

                $('#IncidentExpertSet_Active').prop('checked', false);
                $('#IncidentExpertSet_Active').prop('disabled', true);

                $('#IncidentExpertSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }

    this.$cb = new function () {
        this.get = async (id, bool = true) => {

            let ent = await _Obj();
            ent.Active = bool;
            let Lis = await jsIncidentExpert.Lis(ent);

            jsUtil.Select.Load(id, Lis, 'Id', 'Name');
        }
    }

}

var jsIncidentSla = new function () {

    /*  Model  */
    const _entTbl = [
        { data: 'Id', title: 'Id', visible: false },
        { data: 'Name', title: 'Nombre', visible: true },
        { data: 'State', title: `Cumple`, visible: true }
    ]; // Model by DataTable

    /*  System  */
    this.tblLis = async function (table, cod) {
        try {
            let ent = await _Obj();
            ent.IncId = cod;
            var data = jsUtil.Default.Req.entObj(ent);
            var List = await _Lis(data);

            if (jsUtil.Validate.System.List(List)) {


                jsUtil.Replace.Json(List, lstSiNo, 'State', 'Name');
                jsUtil.DataTable.set(table, _entTbl, List);
                jsUtil.Notify.Show(1, "Listado Exitoso");

            } else {
                jsUtil.DataTable.set(table, _entTbl, jsUtil.Default.Var.List);
            }
        } catch (e) {
            jsUtil.Notify.Show(4, "Error en listar", "jsIncidentSla.tblLis:\n" + e.message.toString());
        }
    } // List in DataTable
    this.Set = async function () {
        var ent = await _Obj();
        ent.Id = $('#Sla').val();
        ent.Name = $('#IncidentSlaSet_Name').val();
        ent.State = $('#IncidentSlaSet_Active').is(":checked") ? true : false;

        let msg = (ent.Id < 1 ? `¿Seguro de agregar ${ent.Name}?` : `¿Seguro de actualizar ${ent.Name}?`);

        jsUtil.Swal.Question(msg, 2, async function () {
            debugger;
            let rpt = await _Set(jsUtil.Default.Req.entObj(ent));
            if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        });
    }
    this.Sel = async function (id) {

        let ent = await _Obj();
        ent.Id = id;
        var rpt = await _Sel(jsUtil.Default.Req.entObj(ent));
        if (jsUtil.Validate.System.Obj(rpt)) { jsUtil.Notify.Show(1, "Exito."); }
        return rpt;
    }
    this.Lis = async function (d = null) {
        let ent = (d == null) ? await _Obj() : d;
        var rpt = await _Lis(jsUtil.Default.Req.entObj(ent));
        return rpt;
    }
    /*  Request  */
    const _Obj = async function () {
        return await jsServer.Request("incidentSla_Obj", jsUtil.Default.Var.Obj, 1);
    } // Get - Object format
    const _Set = async function (d) {
        return await jsServer.Request("incidentSla_Set", d, 1);
    } // Get - Selected object 
    const _Sel = async function (d) {
        return await jsServer.Request("incidentSla_Sel", d, 1);
    } // Get - Selected object 
    const _Lis = async function (d) {
        return await jsServer.Request("incidentSla_Lis", d, 2);
    } // Get - Object list


    /*  Interactive  */
    this.$Form = new function () {

        this.Upd = async function (modal, form, tbl) {
            try {
                default_formIncidentSlaSet();

                $(`${modal} h4`).text('Actualizar');

                let ent = await _Obj();
                ent.Id = jsUtil.DataTable.getSelect(tbl);

                if (ent.Id > 0) {
                    ent = await jsIncidentSla.Sel(ent.Id);

                    $('#Sla').val(ent.Id);

                    $('#IncidentSlaSet_Name').val(ent.Name);
                    $('#IncidentSlaSet_Name').prop('disabled', false);

                    $('#IncidentSlaSet_Active').prop('checked', ent.State);
                    $('#IncidentSlaSet_Active').prop('disabled', false);

                    $('#IncidentSlaSet_Submit').show();

                    jsUtil.Modal.Show(modal);
                }
            } catch (e) { jsUtil.Modal.Close(modal); }
        }

        /*  Default  */
        const default_formIncidentSlaSet = function () {
            try {
                $('#IncidentSlaSet_Name').val('');
                $('#IncidentSlaSet_Name').prop('disabled', true);

                $('#IncidentSlaSet_Active').prop('checked', false);
                $('#IncidentSlaSet_Active').prop('disabled', true);

                $('#IncidentSlaSet_Submit').hide();
            } catch (e) { console.log(ex); }
        }
    }
}

var jsStatus = new function () {
    /*  Interactive  */
    this.$cb = new function () {
        this.get = async (c) => {
            jsUtil.Select.Load(c, lstStatus, 'Id', 'Name');
        }
        this.getAll = async (c) => {
            jsUtil.Select.Load(c, lstStatusAll, 'Id', 'Name');
        }
    }
}
var jsOrigen = new function () {
    /*  Interactive  */
    this.$cb = new function () {
        this.get = async (c) => {
            jsUtil.Select.Load(c, lstOrigen, 'Id', 'Name');
        }
    }
}



/*  Events JQuery   */

// DataTable -- Has a selected item
$('body').on('click', '.dataTable tbody tr', function () {
    var tr = $(this);
    var tbl = tr.parent().parent();
    if (tr.hasClass('even') || tr.hasClass('odd')) {
        if (!tr.children().first().hasClass('dataTables_empty')) {
            if (!tr.parent().hasClass('selected')) {
                tbl.DataTable().$('tr.selected').removeClass('selected');
                tr.addClass('selected');
            }
        }
    }
});

$('#ubi01').on('change', async function () {
    jsUtil.Select.Clear('#ubi02');
    jsUtil.Select.Clear('#ubi03');
    jsUtil.Select.Clear('#ubi04');

    await jsUtil.Select.Down('#ubi01', '#ubi02');
    await jsUtil.Select.Down('#ubi02', '#ubi03');
    await jsUtil.Select.Down('#ubi03', '#ubi04');
})
$('#ubi02').on('change', async function () {
    jsUtil.Select.Clear('#ubi03');
    jsUtil.Select.Clear('#ubi04');

    await jsUtil.Select.Down('#ubi02', '#ubi03');
    await jsUtil.Select.Down('#ubi03', '#ubi04');
})
$('#ubi03').on('change', async function () {
    jsUtil.Select.Clear('#ubi04');

    await jsUtil.Select.Down('#ubi03', '#ubi04');
})
$('#ubi04').on('change', async function () { })

$('#Incident_Status').on('change', function () {
    if ($(this).val() == 3) {
        $('#Incident_Scale').prop('disabled', true);
        $('#Incident_ScaleType').prop('checked', false);
        $('#Incident_ScaleType').prop('disabled', true);
    } else {
        $('#Incident_Scale').prop('disabled', false);
        $('#Incident_ScaleType').prop('disabled', false);

        if ($('#Incident_ScaleType').is(":checked")) {
            $('#Incident_Scale').prop('disabled', true);
        } else {
            $('#Incident_Scale').prop('disabled', false);
        }

    }
});
$('#Incident_ScaleType').on('change', function () {
    
    if ($(this).is(":checked")) {
        $('#Incident_Scale').prop('disabled', true);
    } else {
        $('#Incident_Scale').prop('disabled', false);
    }
    
});


function getValidate() {

    /* Company */
    $('#formCompanySet').validate({
        rules: {
            CompanySet_Name: {
                required: true,
                minlength: 3
            },
            CompanySet_NumberDoc: {
                required: true,
                minlength: 11
            },
            CompanySet_Address: {
                required: true,
                minlength: 5
            },
            ubi04: {
                required: true
            },
            CompanySet_Active: { required: true }
        },
        messages: {
            CompanySet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            CompanySet_NumberDoc: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 11 letras."
            },
            CompanySet_Address: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 5 letras."
            },
            ubi04: {
                required: "Item requerido."
            },
            CompanySet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsCompanySet_md');
            jsCompany.Set();
        }
    });
    /* Office */
    $('#formOfficeSet').validate({
        rules: {
            OfficeSet_Name: {
                required: true,
                minlength: 3
            },
            OfficeSet_Company: {
                required: true
            },
            OfficeSet_Type: { required: true },
            OfficeSet_Active: { required: true }
        },
        messages: {
            OfficeSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            OfficeSet_Company: {
                required: "Item requerido."
            },
            OfficeSet_Type: { required: true },
            OfficeSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsOfficeSet_md');
            jsOffice.Set();
        }
    });
    /* Urgency */
    $('#formUrgencySet').validate({
        rules: {
            UrgencySet_Name: {
                required: true,
                minlength: 3
            }
        },
        messages: {
            UrgencySet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsUrgencySet_md');
            jsUrgency.Set();
        }
    });
    /* Impact */
    $('#formImpactSet').validate({
        rules: {
            ImpactSet_Name: {
                required: true,
                minlength: 3
            }
        },
        messages: {
            ImpactSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsImpactSet_md');
            jsImpact.Set();
        }
    });
    /* Priority */
    $('#formPrioritySet').validate({
        rules: {
            PrioritySet_Name: {
                required: true,
                minlength: 3
            }
        },
        messages: {
            PrioritySet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsPrioritySet_md');
            jsPriority.Set();
        }
    });
    /* Job */
    $('#formJobSet').validate({
        rules: {
            JobSet_Name: {
                required: true,
                minlength: 3
            },
            JobSet_Active: { required: true }
        },
        messages: {
            JobSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            JobSet_Active: { required: 'Item requerido' }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsJobSet_md');
            jsJob.Set();
        }
    });
    /* UserType */
    $('#formUserTypeSet').validate({
        rules: {
            UserTypeSet_Name: {
                required: true,
                minlength: 3
            },
            UserTypeSet_Active: { required: true }
        },
        messages: {
            UserTypeSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            UserTypeSet_Active: { required: 'Item requerido' }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsUserTypeSet_md');
            jsUserType.Set();
        }
    });
    /* People */
    $('#formPeopleSet').validate({
        ignore: ":hidden:not(#summernote),.note-editable.panel-body",
        rules: {
            PeopleSet_Dni: {
                required: true,
                minlength: 8
            },
            PeopleSet_NameP: {
                required: true,
                minlength: 3
            },
            PeopleSet_NameM: {
                required: true,
                minlength: 3
            },
            PeopleSet_Name: {
                required: true,
                minlength: 3
            },
            PeopleSet_Birth: {
                required: true
            },
            PeopleSet_Company: {
                required: true
            },
            PeopleSet_Office: {
                required: true
            },
            PeopleSet_Job: {
                required: true
            },
            PeopleSet_UserType: {
                required: true
            },
            PeopleSet_Email: {
                required: true,
                email: true
            },
            PeopleSet_Telf1: {
                required: true,
                minlength: 7
            },
            PeopleSet_Telf2: {
                required: true,
                minlength: 7
            },
            PeopleSet_Active: { required: true }
        },
        messages: {
            PeopleSet_Dni: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 8 numeros."
            },
            PeopleSet_NameP: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            PeopleSet_NameM: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            PeopleSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            PeopleSet_Birth: {
                required: "Item requerido."
            },
            PeopleSet_Company: {
                required: "Item requerido."
            },
            PeopleSet_Office: {
                required: "Item requerido."
            },
            PeopleSet_Job: {
                required: "Item requerido."
            },
            PeopleSet_UserType: {
                required: "Item requerido."
            },
            PeopleSet_Email: {
                required: "Item requerido.",
                email: "Debe ser un email."
            },
            PeopleSet_Telf1: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 7 numeros."
            },
            PeopleSet_Telf2: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 7 numeros."
            },
            OfficeSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsPeopleSet_md');
            jsPeople.Set();
        }
    });
    /* ServiceType */
    $('#formServiceTypeSet').validate({
        rules: {
            ServiceTypeSet_Name: {
                required: true,
                minlength: 3
            },
            ServiceTypeSet_Active: { required: true }
        },
        messages: {
            ServiceTypeSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            ServiceTypeSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsServiceTypeSet_md');
            jsServiceType.Set();
        }
    });
    /* Service */
    $('#formServiceSet').validate({
        rules: {
            ServiceSet_Name: {
                required: true,
                minlength: 3
            },
            ServiceSet_Type: {
                required: true
            },
            ServiceSet_Active: { required: true }
        },
        messages: {
            ServiceSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            ServiceSet_Type: {
                required: "Item requerido."
            },
            ServiceSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsServiceSet_md');
            jsService.Set();
        }
    });
    
    /* SlaType */
    $('#formSlaTypeSet').validate({
        rules: {
            SlaTypeSet_Name: {
                required: true,
                minlength: 3
            },
            SlaTypeSet_Active: { required: true }
        },
        messages: {
            SlaTypeSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            SlaTypeSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsSlaTypeSet_md');
            jsSlaType.Set();
        }
    });

    /* Sla */
    $('#formSlaSet').validate({
        rules: {
            SlaSet_Name: {
                required: true,
                minlength: 3
            },
            SlaSet_Type: {
                required: true
            },
            SlaSet_Active: { required: true }
        },
        messages: {
            SlaSet_Name: {
                required: "Item requerido.",
                minlength: "Debe ser mayor a 3 letras."
            },
            SlaSet_Type: {
                required: "Item requerido."
            },
            SlaSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsSlaSet_md');
            jsSla.Set();
        }
    });
    /* ComSvc */
    $('#formComSvcSet').validate({
        rules: {
            ComSvcSet_Service: {
                required: true
            },
            ComSvcSet_Active: { required: true }
        },
        messages: {
            ComSvcSet_Service: {
                required: "Item requerido."
            },
            ComSvcSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsComSvcSet_md');
            jsComSvc.Set();
        }
    });

    /* ComSvcSla */
    $('#formComSvcSlaSet').validate({
        rules: {
            ComSvcSlaSet_Sla: {
                required: true
            },
            ComSvcSet_Active: { required: true }
        },
        messages: {
            ComSvcSlaSet_Sla: {
                required: "Item requerido."
            },
            ComSvcSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsComSvcSlaSet_md');
            jsComSvcSla.Set();
        }
    });


    /* Incident Add */
    $('#formIncidentAddSet').validate({
        ignore: ":hidden:not(#summernote),.note-editable.panel-body",
        rules: {
            Incident_Asunto: { required: true },
            Incident_Origen: { required: true },
            Incident_Status: { required: true },
            Incident_Company: { required: true },
            Incident_Office: { required: true },
            Incident_People: { required: true },
            Incident_Service: { required: true },
            Incident_Urgency: { required: true },
            Incident_Impact: { required: true },
            Incident_Priority: { required: true },
            Incident_Scale: { required: true },
            Incident_Time: { required: true }
        },
        messages: {
            Incident_Asunto: { required: "Item requerido." },
            Incident_Origen: { required: "Item requerido." },
            Incident_Status: { required: "Item requerido." },
            Incident_Company: { required: "Item requerido." },
            Incident_Office: { required: "Item requerido." },
            Incident_People: { required: "Item requerido." },
            Incident_Service: { required: "Item requerido." },
            Incident_Urgency: { required: "Item requerido." },
            Incident_Impact: { required: "Item requerido." },
            Incident_Priority: { required: "Item requerido." },
            Incident_Scale: { required: "Item requerido." },
            Incident_Time: { required: "Item requerido." }
        },
        submitHandler: function (form) {
            jsIncident.Set();
        }
    });

    /* Incident Sla */
    $('#formIncidentSlaSet').validate({
        rules: {
            IncidentSlaSet_Name: {
                required: true
            },
            IncidentSlaSet_Active: { required: true }
        },
        messages: {
            IncidentSlaSet_Name: {
                required: "Item requerido."
            },
            IncidentSlaSet_Active: { required: true }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsIncidentSlaSet_md');
            jsIncidentSla.Set();
        }
    });

    /* Incident Likert */
    $('#formIncidentLikertSet').validate({
        rules: {
            IncidentLikertSet_Likert: {
                required: true
            }
        },
        messages: {
            IncidentLikertSet_Likert: {
                required: "Item requerido."
            }
        },
        submitHandler: function (form) {
            jsUtil.Modal.Close('#jsIncidentLikertSet_md');
            jsIncident.Likert();
        }
    });
    
}


/* Test */
var lstLikert =
    [
        { "Id": 1, "Name": "Muy Mala" },
        { "Id": 2, "Name": "Mala" },
        { "Id": 3, "Name": "Regular" },
        { "Id": 4, "Name": "Buena" },
        { "Id": 5, "Name": "Muy Buena" }
    ];
var lstOfficeType =
    [
        { "Id": false, "Name": "Común" },
        { "Id": true, "Name": "Operativa" }
    ];
var lstActive =
    [
        { "Id": false, "Name": "Desactivado" },
        { "Id": true, "Name": "Activado" }
    ];
var lstSiNo =
    [
        { "Id": false, "Name": "No" },
        { "Id": true, "Name": "Si" }
    ];
var lstStatus =
    [
        { "Id": 1, "Name": "Abierto" },
        { "Id": 2, "Name": "Validar" },
        { "Id": 3, "Name": "Solucionado" }
    ];
var lstStatusAll =
    [
        { "Id": 1, "Name": "Abierto" },
        { "Id": 2, "Name": "Validar" },
        { "Id": 3, "Name": "Solucionado" },
        { "Id": 4, "Name": "Cerrado" }
    ];
var lstOrigen =
    [
        { "Id": 1, "Name": "Llamada" },
        { "Id": 2, "Name": "Correo" }
    ];




function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? addZero(hours) : addZero(12); // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}






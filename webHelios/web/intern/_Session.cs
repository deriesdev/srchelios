﻿using System;
using System.Web;

namespace web.intern
{
    public static class _Session
    {
        //List Session
        public enum list { id, dni, name, surname, email, com, office, job, type, incId, test }

        //get and set
        public static object get(list name)
        {
            object session = HttpContext.Current.Session[Enum.GetName(typeof(list), name)];
            if (session == null) return null;
            else return session;
        }

        internal static void set(list name, object value)
        {
            string session = Enum.GetName(typeof(list), name);
            HttpContext.Current.Session[session] = value;
        }
             
        //option
        internal static void exists()
        {
            if (get(list.id) == null) HttpContext.Current.Response.RedirectToRoute("Login");
        }

        internal static void del(list name)
        {
            set(name, null);
        }

        internal static void delAll()
        {
            HttpContext.Current.Session.Clear();
        }

        internal static int? parseInt(list name)
        {
            int rpt = 0;
            bool temp = int.TryParse(get(name).ToString(), out rpt);
            return rpt;
        }

        //Show in aspx

    }
}
using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcSla
    {
        public entSla add(entSla _obj)
        {
            try
            {
                logSla oEnt = new logSla();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entSla upd(entSla _obj)
        {
            try
            {
                logSla oEnt = new logSla();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entSla del(entSla _obj)
        {
            try
            {
                logSla oEnt = new logSla();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entSla sel(entSla _obj)
        {
            try
            {
                logSla oEnt = new logSla();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entSla> lis(entSla _obj)
        {
            try
            {
                logSla oEnt = new logSla();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcSla")]
    public partial interface intSvcSla
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addSla")]
        entSla add(entSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updSla")]
        entSla upd(entSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delSla")]
        entSla del(entSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selSla")]
        entSla sel(entSla _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisSla",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisSla")]
        IList<entSla> lis(entSla _obj);
    }
}


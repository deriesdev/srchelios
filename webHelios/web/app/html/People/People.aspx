﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="People.aspx.cs" Inherits="web.app.html.People.People" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Persona • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Persona</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Administración de la configuración</a></li>
                <li class="active"><strong>Persona</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista de Persona</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsPeople.tblLis('#tblPeople');"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            <li><a href="#" onclick="jsPeople.$Form.Sel('#jsPeopleSet_md', '#formPeopleSet', '#tblPeople');"><i class="fa fa-eye"></i>Ver</a></li>
                            <li><a href="#" onclick="jsPeople.$Form.Ins('#jsPeopleSet_md', '#formPeopleSet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsPeople.$Form.Upd('#jsPeopleSet_md', '#formPeopleSet', '#tblPeople');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblPeople" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>

            </div>
        </div>

    </div>



    <!-- Modal -->
    <div class="modal inmodal" id="jsPeopleSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formPeopleSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 nopadding">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Documento:</label>
                                        <input type="text" id="PeopleSet_Dni" name="PeopleSet_Dni" placeholder="Documento" class="form-control valid" required />
                                    </div>
                                </div>
                            
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Apellido paterno:</label>
                                        <input type="text" id="PeopleSet_NameP" name="PeopleSet_NameP" placeholder="Apellido paterno" class="form-control valid" required />
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Apellido materno:</label>
                                        <input type="text" id="PeopleSet_NameM" name="PeopleSet_NameM" placeholder="Apellido materno" class="form-control valid" required />
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 nopadding">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Nombre:</label>
                                        <input type="text" id="PeopleSet_Name" name="PeopleSet_Name" placeholder="Nombre" class="form-control valid" required />
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Fecha de nacimiento:</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="PeopleSet_Birth" name="PeopleSet_Birth"  class="DatePYear form-control" />
                                        </div>
                                    </div>
                                    <label id="PeopleSet_Birth-error" class="error" for="PeopleSet_Birth" style=""></label>
                                </div>
                            </div>
                            <div class="col-lg-12 nopadding">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Compañia:</label>
                                        <div class="form-control-select2">
                                            <select id="PeopleSet_Company" name="PeopleSet_Company" class="select2" style="width: 100% !important;" onchange="jsOffice.$cbOffice.get('#PeopleSet_Company','#PeopleSet_Office');"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Área:</label>
                                        <div class="form-control-select2">
                                            <select id="PeopleSet_Office" name="PeopleSet_Office" class="select2" style="width: 100% !important;"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 nopadding">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Puesto de trabajo:</label>
                                        <div class="form-control-select2">
                                            <select id="PeopleSet_Job" name="PeopleSet_Job" class="select2" style="width: 100% !important;"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Cargo en sistema:</label>
                                        <div class="form-control-select2">
                                            <select id="PeopleSet_UserType" name="PeopleSet_UserType" class="select2" style="width: 100% !important;"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-12 nopadding">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Correo Electronico:</label>
                                        <input type="email" id="PeopleSet_Email" name="PeopleSet_Email" placeholder="Correo electronico" class="form-control valid" required />
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Telefono N°01:</label>
                                        <input type="tel" id="PeopleSet_Telf1" name="PeopleSet_Telf1" placeholder="Telefono N°01" class="form-control valid" required />
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Telefono N°02:</label>
                                        <input type="tel" id="PeopleSet_Telf2" name="PeopleSet_Telf2" placeholder="Telefono N°02" class="form-control valid" required />
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nota:</label>
                                    <div id="PeopleSet_Note" class="summernote"></div>    
                                </div>
                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Estado:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="PeopleSet_Active" name="PeopleSet_Active" class="onoffswitch-checkbox" disabled />
                                            <label class="onoffswitch-label" for="PeopleSet_Active">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="PeopleSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            try {
                jsPeople.tblLis('#tblPeople');
            } catch (e) {
                jsNotify.Show(5, "Error", e.message.toString());
            }
        });

    </script>

</asp:Content>
﻿CREATE TABLE [dbo].[incidentExpert]
(
	[id]			INT				NOT NULL PRIMARY KEY IDENTITY,
	[noteInt]		VARCHAR(MAX)	NOT NULL DEFAULT 'Nota adicional',
	[noteExt]		VARCHAR(MAX)	NOT NULL DEFAULT 'Nota adicional',
	[time]			TIME(0)			NOT NULL,
	[date]			DATETIME		NOT NULL,
	[peoId]			INT				NOT NULL REFERENCES[people](id),
	[incId]			INT				NOT NULL REFERENCES[incident](id),
)

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;

namespace DataAccess
{
    public abstract class Persistence
    {
        #region Property
        protected abstract DbConnection cn { get; }
        protected abstract string cnStr { get; }
        #endregion

        #region Connection
        public void cnOpen()
        {
            try
            {
                cn.ConnectionString = cnStr;
                cn.Open();
            }
            catch (Exception ex) { throw ex; }

        }
        public void cnClose()
        {
            try { cn.Close(); }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region Request
        public int getScalar(string usp, object obj)
        {
            try
            {
                int rpt = 0;
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, usp, obj);
                    int.TryParse(Cmd.ExecuteScalar().ToString(), out rpt);
                    return rpt;
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public int getScalar(string Procedure, params object[] param)
        {
            try
            {
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, Procedure, param);
                    return Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public int getScalar(string Query)
        {
            try
            {
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, Query);
                    return Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public int getExecute(string usp, object obj)
        {
            try
            {
                int rpt = 0;
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, usp, obj);
                    int.TryParse(Cmd.ExecuteNonQuery().ToString(), out rpt);
                    return rpt;
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public int getExecute(string Procedure, params object[] param)
        {
            try
            {
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, Procedure, param);
                    return Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public int getExecute(string Query)
        {
            try
            {
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, Query);
                    return Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex) { throw ex; }
        }


        public DbDataReader getDataReader(string usp, object obj)
        {
            try
            {
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, usp, obj);
                    return Cmd.ExecuteReader();
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader getDataReader(string usp, params object[] param)
        {
            try
            {
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, usp, param);
                    return Cmd.ExecuteReader();
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public DbDataReader getDataReader(string Query)
        {
            try
            {
                using (var Cmd = cn.CreateCommand())
                {
                    PrepareCommand(Cmd, Query);
                    return Cmd.ExecuteReader();
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public bool getObj<T>(string usp, object obj)
        {
            try
            {
                var dr = getDataReader(usp, obj);
                if (dr.HasRows)
                {
                    while (dr.Read()) { setProperties(dr, obj); }
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex) { throw ex; }
        }
        public bool getObj<T>(string Procedure, params object[] param)
        {
            throw new NotImplementedException();
        }
        public bool getObj<T>(string Query)
        {
            throw new NotImplementedException();
        }

        public List<T> getList<T>(string usp, object obj)
        {
            try
            {
                var dr = getDataReader(usp, obj);
                var rpt = DRtoList<T>(dr);
                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }
        public List<T> getList<T>(string usp, params object[] param)
        {
            try
            {
                var dr = getDataReader(usp, param);
                var rpt = DRtoList<T>(dr);

                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }
        public List<T> getList<T>(string qr)
        {
            try
            {
                var dr = getDataReader(qr);
                var rpt = DRtoList<T>(dr);

                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }

        private void PrepareCommand(DbCommand cmd, string usp, object obj)
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = usp;

            SqlCommandBuilder.DeriveParameters(cmd as SqlCommand);

            SetParametersValue(cmd, obj);

        }
        private void PrepareCommand(DbCommand cmd, string usp, params object[] param)
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = usp;

            SqlCommandBuilder.DeriveParameters(cmd as SqlCommand);

            var j = Math.Min(cmd.Parameters.Count, param.Length);

            for (int i = 0; i < j; i++)
            {
                setParameterValue(cmd.Parameters[i], param[i]);
            }
        }
        private void PrepareCommand(DbCommand cmd, string qr)
        {
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = qr;
        }
        #endregion

        private void SetParametersValue(DbCommand cmd, object obj)
        {
            try
            {
                var Type = obj.GetType();

                foreach (DbParameter Prm in cmd.Parameters)
                {
                    PropertyInfo qw = Type.GetProperty(Prm.ToString().Substring(1));
                    if (qw != null) setParameterValue(Prm, qw.GetValue(obj));
                }
            }
            catch (Exception) { throw; }

        }

        private void setParameterValue(DbParameter param, object val)
        {
            param.Value = val ?? DBNull.Value;
        }

        public static void setProperties(DbDataReader dr, object obj)
        {
            var Type = obj.GetType();

            for (int i = 0; i < dr.FieldCount; i++)
            {
                var prpInf = Type.GetProperty(dr.GetName(i));
                if (prpInf != null && !dr.IsDBNull(i))
                    prpInf.SetValue(obj, dr.GetValue(i));
            }
        }

        public List<T> DRtoList<T>(DbDataReader dr)
        {
            try
            {
                var rpt = new List<T>();
                while (dr.Read())
                {
                    T m = (T)Activator.CreateInstance(typeof(T), new object[] { });
                    setProperties(dr, m);
                    rpt.Add(m);
                }
                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }

    }

}
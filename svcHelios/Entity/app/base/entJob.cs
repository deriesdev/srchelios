using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entJob : entBase<entJob>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;

        #endregion

        #region Constructor
        public entJob() { }
        public entJob(int _id) { id = _id; }
        public entJob(int _id, string _name, bool _active)
        {
            id = _id;
            name = _name;
            active = _active;
        }
        #endregion
    }
}

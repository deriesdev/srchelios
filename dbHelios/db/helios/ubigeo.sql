﻿CREATE TABLE [dbo].[ubigeo]
(
	[id]	INT NOT NULL PRIMARY KEY IDENTITY,
	[cod]	VARCHAR(10) NOT NULL,
	[name]	VARCHAR(50) NOT NULL,
	[up]	INT NULL REFERENCES[ubigeo](id),
	[type]	INT NULL REFERENCES[ubigeoType](id)
)

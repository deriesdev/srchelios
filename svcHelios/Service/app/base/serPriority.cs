using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcPriority
    {
        public entPriority add(entPriority _obj)
        {
            try
            {
                logPriority oEnt = new logPriority();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entPriority upd(entPriority _obj)
        {
            try
            {
                logPriority oEnt = new logPriority();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entPriority del(entPriority _obj)
        {
            try
            {
                logPriority oEnt = new logPriority();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entPriority sel(entPriority _obj)
        {
            try
            {
                logPriority oEnt = new logPriority();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entPriority> lis(entPriority _obj)
        {
            try
            {
                logPriority oEnt = new logPriority();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcPriority")]
    public partial interface intSvcPriority
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addPriority",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addPriority")]
        entPriority add(entPriority _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updPriority",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updPriority")]
        entPriority upd(entPriority _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delPriority",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delPriority")]
        entPriority del(entPriority _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selPriority",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selPriority")]
        entPriority sel(entPriority _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisPriority",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisPriority")]
        IList<entPriority> lis(entPriority _obj);
    }
}


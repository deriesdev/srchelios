using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entUserType : entBase<entUserType>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;

        #endregion

        #region Constructor
        public entUserType() { }
        public entUserType(int _id) { id = _id; }
        public entUserType(int _id, string _name, bool _active)
        {
            id = _id;
            name = _name;
            active = _active;
        }
        #endregion
    }
}

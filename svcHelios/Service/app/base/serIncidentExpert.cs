using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcIncidentExpert
    {
        public entIncidentExpert add(entIncidentExpert _obj)
        {
            try
            {
                logIncidentExpert oEnt = new logIncidentExpert();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentExpert upd(entIncidentExpert _obj)
        {
            try
            {
                logIncidentExpert oEnt = new logIncidentExpert();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentExpert del(entIncidentExpert _obj)
        {
            try
            {
                logIncidentExpert oEnt = new logIncidentExpert();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncidentExpert sel(entIncidentExpert _obj)
        {
            try
            {
                logIncidentExpert oEnt = new logIncidentExpert();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entIncidentExpert> lis(entIncidentExpert _obj)
        {
            try
            {
                logIncidentExpert oEnt = new logIncidentExpert();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcIncidentExpert")]
    public partial interface intSvcIncidentExpert
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addIncidentExpert",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addIncidentExpert")]
        entIncidentExpert add(entIncidentExpert _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updIncidentExpert",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updIncidentExpert")]
        entIncidentExpert upd(entIncidentExpert _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delIncidentExpert",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delIncidentExpert")]
        entIncidentExpert del(entIncidentExpert _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selIncidentExpert",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selIncidentExpert")]
        entIncidentExpert sel(entIncidentExpert _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisIncidentExpert",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisIncidentExpert")]
        IList<entIncidentExpert> lis(entIncidentExpert _obj);
    }
}


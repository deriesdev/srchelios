using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcComSvc
    {
        public entComSvc add(entComSvc _obj)
        {
            try
            {
                logComSvc oEnt = new logComSvc();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entComSvc upd(entComSvc _obj)
        {
            try
            {
                logComSvc oEnt = new logComSvc();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entComSvc del(entComSvc _obj)
        {
            try
            {
                logComSvc oEnt = new logComSvc();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entComSvc sel(entComSvc _obj)
        {
            try
            {
                logComSvc oEnt = new logComSvc();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entComSvc> lis(entComSvc _obj)
        {
            try
            {
                logComSvc oEnt = new logComSvc();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcComSvc")]
    public partial interface intSvcComSvc
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addComSvc",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addComSvc")]
        entComSvc add(entComSvc _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updComSvc",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updComSvc")]
        entComSvc upd(entComSvc _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delComSvc",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delComSvc")]
        entComSvc del(entComSvc _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selComSvc",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selComSvc")]
        entComSvc sel(entComSvc _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisComSvc",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisComSvc")]
        IList<entComSvc> lis(entComSvc _obj);
    }
}


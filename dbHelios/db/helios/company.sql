﻿CREATE TABLE [dbo].[company]
(
	[id]			INT NOT NULL PRIMARY KEY IDENTITY,
	[name]			VARCHAR(50) NOT NULL,
	[numberDoc]		VARCHAR(20) NOT NULL,
	[address]		VARCHAR(200) NOT NULL,
	[ubigeo]		INT NULL REFERENCES[ubigeo](id),
	[active]		BIT NOT NULL DEFAULT 1
)

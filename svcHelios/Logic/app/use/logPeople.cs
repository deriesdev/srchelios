﻿using DataAccess;
using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logPeople : ILogic<entPeople>
    {
        public entPeople auth(entPeople _obj)
        {
            try
            {
                var _rpt = new entPeople();
                var _pass = _obj.pass;
                var _temp = _obj;

                if ( _temp.auth() )
                {
                    if (_temp.pass.Equals(Crypt.En(_pass)))
                    {
                        if (_temp.active) { _rpt = _temp; }
                        else { _rpt.err = "Usuario Deshabilitado."; }
                    }
                    else { _rpt.err = "Contraseña incorrecta."; }
                }
                else { _rpt.err = "Usuario incorrecto."; }

                return _rpt;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}


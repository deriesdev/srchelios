using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entIncidentExpert : entBase<entIncidentExpert>
    {
        #region Propiedades
        [DataMember(Name ="NoteInt", Order = 2)]
        public string noteInt { set; get; } = string.Empty;
        [DataMember(Name ="NoteExt", Order = 3)]
        public string noteExt { set; get; } = string.Empty;
        [DataMember(Name ="Time", Order = 4)]
        public TimeSpan time { set; get; } = TimeSpan.MinValue;
        [DataMember(Name ="Date", Order = 5)]
        public DateTime date { set; get; } = DateTime.MinValue;
        [DataMember(Name ="IncId", Order = 7)]
        public int incId { set; get; } = 0;
        #endregion

        #region Constructor
        public entIncidentExpert () { }
        public entIncidentExpert (int _id) { id = _id; }
        public entIncidentExpert(int _id, string _noteInt, string _noteExt, TimeSpan _time, DateTime _date, int _peoId, int _incId)
        {
            id = _id;
            noteInt = _noteInt;
            noteExt = _noteExt;
            time = _time;
            date = _date;
            peoId = _peoId;
            incId = _incId;
        }
        #endregion
        }
    }

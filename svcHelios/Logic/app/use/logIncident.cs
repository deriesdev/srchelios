using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logIncident : ILogic<entIncident>
    {

        public entIncident updLikert(entIncident _obj)
        {
            try
            {
                if (!_obj.updLikert()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncident updExpert(entIncident _obj)
        {
            try
            {
                if (!_obj.updExpert()) { _obj.id = 0; }
                return _obj;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}


using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entImpact : entBase<entImpact>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        #endregion

        #region Constructor
        public entImpact() { }
        public entImpact(int _id) { id = _id; }
        public entImpact(int _id, string _name)
        {
            id = _id;
            name = _name;
        }
        #endregion
    }
}

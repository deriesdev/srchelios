using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcServiceType
    {
        public entServiceType add(entServiceType _obj)
        {
            try
            {
                logServiceType oEnt = new logServiceType();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entServiceType upd(entServiceType _obj)
        {
            try
            {
                logServiceType oEnt = new logServiceType();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entServiceType del(entServiceType _obj)
        {
            try
            {
                logServiceType oEnt = new logServiceType();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entServiceType sel(entServiceType _obj)
        {
            try
            {
                logServiceType oEnt = new logServiceType();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entServiceType> lis(entServiceType _obj)
        {
            try
            {
                logServiceType oEnt = new logServiceType();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcServiceType")]
    public partial interface intSvcServiceType
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addServiceType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addServiceType")]
        entServiceType add(entServiceType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updServiceType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updServiceType")]
        entServiceType upd(entServiceType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delServiceType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delServiceType")]
        entServiceType del(entServiceType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selServiceType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selServiceType")]
        entServiceType sel(entServiceType _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisServiceType",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisServiceType")]
        IList<entServiceType> lis(entServiceType _obj);
    }
}


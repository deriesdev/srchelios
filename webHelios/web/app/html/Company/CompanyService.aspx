﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="CompanyService.aspx.cs" Inherits="web.app.html.Company.CompanyService" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Compañia - Servicio • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Compañia - Servicio</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Administración de la configuración</a></li>
                <li class="active"><strong>Compañia - Servicio</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Listado de empresa</h5>
                    <div class="ibox-tools">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsCompany.$cb.get('#ComSvc_Company', true);"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            <li><a href="#" onclick="jsComSvc.tblLis('#tblComSvc', '#ComSvc_Company');"><i class="fa fa-refresh"></i>Listar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Empresa:</label>
                                <div class="form-control-select2">
                                    <select id="ComSvc_Company" name="ComSvc_Company" class="select2" style="width: 100% !important;"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista servicio</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsComSvcSla.tblLis('#tblComSvcSla', '#tblComSvc');"><i class="fa fa-eye"></i>Detalle</a></li>
                            <li><a href="#" onclick="jsComSvc.$Form.Sel('#jsComSvcSet_md', '#formComSvcSet', '#tblComSvc');"><i class="fa fa-eye"></i>Ver</a></li>
                            <li><a href="#" onclick="jsComSvc.$Form.Ins('#jsComSvcSet_md', '#formComSvcSet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsComSvc.$Form.Upd('#jsComSvcSet_md', '#formComSvcSet', '#tblComSvc');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblComSvc" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>

            </div>
        </div>

        <!-- Modal Sla -->
        <div class="modal inmodal" id="jsComSvcSet_md" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <!--modal-lg modal-sm-->
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <form id="formComSvcSet" role="form" novalidate="novalidate">
                        <div class="modal-body">

                            <!-- -->
                            <div class="row">


                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 opc">
                                    <div class="form-group">
                                        <label>Tipo:</label>
                                        <div class="form-control-select2">
                                            <select id="ComSvcSet_ServiceType" name="ComSvcSet_ServiceType" onchange="jsService.$cb.get('#ComSvcSet_ServiceType', '#ComSvcSet_Service');" class="select2" style="width: 100% !important;"></select>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Servicio:</label>
                                        <div class="form-control-select2">
                                            <select id="ComSvcSet_Service" name="ComSvcSet_Service" class="select2" style="width: 100% !important;"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                    <div class="form-group">
                                        <label>Estado:</label>
                                        <div class="switch">
                                            <div class="onoffswitch">
                                                <input type="checkbox" id="ComSvcSet_Active" name="ComSvcSet_Active" class="onoffswitch-checkbox" disabled />
                                                <label class="onoffswitch-label" for="ComSvcSet_Active">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- -->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                            <button id="ComSvcSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>




        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista de tipo servicio</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsComSvcSla.$Form.Ins('#jsComSvcSlaSet_md', '#formComSvcSlaSet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsComSvcSla.$Form.Upd('#jsComSvcSlaSet_md', '#formComSvcSlaSet', '#tblComSvcSla');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblComSvcSla" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>

            </div>
        </div>



    </div>



    



    <!-- Modal Sla Type -->
    <div class="modal inmodal" id="jsComSvcSlaSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formComSvcSlaSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 opc">
                                    <div class="form-group">
                                        <label>Tipo:</label>
                                        <div class="form-control-select2">
                                            <select id="ComSvcSlaSet_SlaType" name="ComSvcSlaSet_SlaType" onchange="jsSla.$cb.get('#ComSvcSlaSet_SlaType', '#ComSvcSlaSet_Sla');" class="select2" style="width: 100% !important;"></select>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Servicio:</label>
                                        <div class="form-control-select2">
                                            <select id="ComSvcSlaSet_Sla" name="ComSvcSlaSet_Sla" class="select2" style="width: 100% !important;"></select>
                                        </div>
                                    </div>
                                </div>


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Valor:</label>
                                    <input type="text" id="ComSvcSlaSet_Data" name="ComSvcSlaSet_Data" placeholder="Nombre" class="form-control valid"/>
                                </div>
                            </div>                        

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Estado:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="ComSvcSlaSet_Active" name="ComSvcSlaSet_Active" class="onoffswitch-checkbox" disabled />
                                            <label class="onoffswitch-label" for="ComSvcSlaSet_Active">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="ComSvcSlaSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            try {
                jsCompany.$cb.get('#ComSvc_Company', true);
            } catch (e) {
                jsNotify.Show(5, "Error", e.message.toString());
            }
        });

    </script>

</asp:Content>

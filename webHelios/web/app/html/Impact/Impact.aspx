﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="Impact.aspx.cs" Inherits="web.app.html.Impact.Impact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Impacto • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Impacto</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Administración de la configuración</a></li>
                <li class="active"><strong>Impacto</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista de impacto</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsImpact.tblLis('#tblImpact');"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            <li><a href="#" onclick="jsImpact.$Form.Sel('#jsImpactSet_md', '#formImpactSet', '#tblImpact');"><i class="fa fa-eye"></i>Ver</a></li>
                            <li><a href="#" onclick="jsImpact.$Form.Ins('#jsImpactSet_md', '#formImpactSet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsImpact.$Form.Upd('#jsImpactSet_md', '#formImpactSet', '#tblImpact');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblImpact" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>

            </div>
        </div>

    </div>



    <!-- Modal -->
    <div class="modal inmodal" id="jsImpactSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formImpactSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre:</label>
                                    <input type="text" id="ImpactSet_Name" name="ImpactSet_Name" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="ImpactSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            try {
                jsImpact.tblLis('#tblImpact');
            } catch (e) {
                jsNotify.Show(5, "Error", e.message.toString());
            }
        });

    </script>

</asp:Content>


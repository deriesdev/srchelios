using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entComSvcSla : entBase<entComSvcSla>
    {
        #region Propiedades
        [DataMember(Name = "Data", Order = 2)]
        public string data { set; get; } = string.Empty;
        [DataMember(Name = "ComSvc", Order = 3)]
        public int comSvc { set; get; } = 0;
        [DataMember(Name = "Sla", Order = 4)]
        public int sla { set; get; } = 0;
        #endregion

        #region Constructor
        public entComSvcSla() { }
        public entComSvcSla(int _id) { id = _id; }
        public entComSvcSla(int _id, string _data, int _comSvc, int _sla, bool _active)
        {
            id = _id;
            data = _data;
            comSvc = _comSvc;
            sla = _sla;
            active = _active;
        }
        #endregion
    }
}

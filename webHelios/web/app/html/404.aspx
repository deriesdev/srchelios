﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="web.app.html._404" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        
        <!-- Meta -->
        <!-- #include file="../meta.html" -->
        <!-- End Meta -->

        <title>404 • Portal Cliente</title>

        <!-- Style -->
        <!-- #include file="../style.html" -->    
        <!-- End Style -->

    </head>
    <body class="gray-bg">

        <div class="middle-box text-center animated fadeInDown">
            <h1>404</h1>
            <h3 class="font-bold">Page Not Found</h3>

            <div class="error-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod purus id felis egestas, eu gravida nunc volutpat. Etiam in justo urna. Duis vitae varius quam, eget semper nisi. Curabitur nec vehicula nunc, vitae efficitur ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fermentum, ligula eget fermentum vehicula, libero metus viverra nulla, quis finibus sem quam eu nisi. Praesent hendrerit ornare tellus, accumsan sodales nibh tincidunt at. Nam eu dui tincidunt, iaculis augue et, maximus lectus.: <br/><a href="Dashboard" class="btn btn-primary m-t">Dashboard</a></div>
        </div>

        <!-- Footer -->
        <!-- #include file="../footer.html" -->
        <!-- End Footer -->

    </body>

</html>


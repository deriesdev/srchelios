﻿CREATE TABLE [dbo].[menu]
(
	[id]			INT NOT NULL PRIMARY KEY IDENTITY,
	[name]			varchar(200) NOT NULL,
	[url]			varchar(100) NULL,
	[order]			int NOT NULL,
	[typeId]		int NOT NULL REFERENCES[menuType](id),
	[up]			int NULL REFERENCES[menu](id),
	[active]		bit NOT NULL
)

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="UserType.aspx.cs" Inherits="web.app.html.UserType.UserType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Tipo de Usuario • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Tipo de Usuario</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Configuración</a></li>
                <li class="active"><strong>Tipo de Usuario</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista de tipo de usuario</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsUserType.tblLis('#tblUserType');"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            <li><a href="#" onclick="jsUserType.$Form.Sel('#jsUserTypeSet_md', '#formUserTypeSet', '#tblUserType');"><i class="fa fa-eye"></i>Ver</a></li>
                            <li><a href="#" onclick="jsUserType.$Form.Ins('#jsUserTypeSet_md', '#formUserTypeSet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsUserType.$Form.Upd('#jsUserTypeSet_md', '#formUserTypeSet', '#tblUserType');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblUserType" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>

            </div>
        </div>

    </div>



    <!-- Modal -->
    <div class="modal inmodal" id="jsUserTypeSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formUserTypeSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre:</label>
                                    <input type="text" id="UserTypeSet_Name" name="UserTypeSet_Name" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Estado:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="UserTypeSet_Active" name="UserTypeSet_Active" class="onoffswitch-checkbox" disabled />
                                            <label class="onoffswitch-label" for="UserTypeSet_Active">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="UserTypeSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            try {
                jsUserType.tblLis('#tblUserType');
            } catch (e) {
                jsNotify.Show(5, "Error", e.message.toString());
            }
        });

    </script>

</asp:Content>


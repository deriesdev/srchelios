﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="IncidentUpd.aspx.cs" Inherits="web.app.html.Incident.IncidentUpd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Seguimiento Incidencia • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Seguimiento Incidencia</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Incidente</a></li>
                <li class="active"><strong>Seguimiento</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <form id="formIncidentAddSet" role="form" novalidate="novalidate">
        <div class="row">

            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Incidencia</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Asunto:</label>
                                    <input type="text" id="Incident_Asunto" name="Incident_Asunto" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <i class="fa fa-ticket" aria-hidden="true"></i>
                                    <label>Ticket:</label>
                                    <p id="Incident_Cod"></p>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Origen:</label>
                                    <p id="Incident_Origin"></p>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Estado:</label>
                                    <select id="Incident_Status" name="Incident_Status" class="select2" style="width: 100% !important;"></select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Compañia:</label>
                                    <p id="Incident_Company"></p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Área:</label>
                                    <p id="Incident_Office"></p>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Persona:</label>
                                    <p id="Incident_People"></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Servicio:</label>
                                    <p id="Incident_Service"></p>
                                </div>
                            </div>

                        </div>
                        <!-- -->

                    </div>

                </div>

            </div>


            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Prioridad</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Urgency:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Urgency" name="Incident_Urgency" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Impacto:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Impact" name="Incident_Impact" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Prioridad:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Priority" name="Incident_Priority" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-lg-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Detalle</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Interno:</label>
                                    <div id="Incident_NoteInt" class="summernote"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Publico:</label>
                                    <div id="Incident_NoteExt" class="summernote"></div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Escalado</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Jerárquico:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="Incident_ScaleType" name="Incident_ScaleType" class="onoffswitch-checkbox" />
                                            <label class="onoffswitch-label" for="Incident_ScaleType">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Área:</label>
                                    <div class="form-control-select2">
                                        <select id="Incident_Scale" name="Incident_Scale" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Acuerdos de Servicio</h5>
                        <div class="ibox-tools">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-ellipsis-v"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" onclick="tblF5();"><i class="fa fa-refresh"></i>Actualizar</a></li>
                                <li><a href="#" onclick="jsIncidentSla.$Form.Upd('#jsIncidentSlaSet_md', '#formIncidentSlaSet', '#tblIncidentSla');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                            </ul>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">
                        <table id="tblIncidentSla" class="table table-striped table-bordered table-hover dataTables-example"></table>
                    </div>

                </div>
            </div>

            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Cierre</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: block;">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Tiempo utilizado:</label>
                                    <input type="time" id="Incident_Time" name="Incident_Time" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                        </div>
                        <!-- -->

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                            <button id="CompanySet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </div>
            </div>




        </div>
    </form>

    <!-- Modal -->
    <div class="modal inmodal" id="jsIncidentSlaSet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formIncidentSlaSet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Acuerdo de Servicio:</label>
                                    <input type="text" id="IncidentSlaSet_Name" name="IncidentSlaSet_Name" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Estado:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="IncidentSlaSet_Active" name="IncidentSlaSet_Active" class="onoffswitch-checkbox" disabled />
                                            <label class="onoffswitch-label" for="IncidentSlaSet_Active">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="IncidentSlaSet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        function tblF5() {
            jsIncidentSla.tblLis("#tblIncidentSla", $('#Id').val());
        }


        $(document).ready(function () {

            start();

        });

        async function start() {

            await jsStatus.$cb.get('#Incident_Status');
            await jsUrgency.$cb.get('#Incident_Urgency');
            await jsImpact.$cb.get('#Incident_Impact');
            await jsPriority.$cb.get('#Incident_Priority');
            await jsOffice.$cbOffice.getOP('#Incident_Scale');

            var Inc = await jsIncident.Sel($("#sInc").val());

            $('#Id').val(Inc.Id);

            if (Inc.Id > 0) {

                $('#Incident_Cod').text(Inc.Name);
                $('#Incident_Asunto').val(Inc.Subject);
                $('#Incident_Origin').text(jsUtil.Replace.byId(lstOrigen, 'Id', 'Name', Inc.Origin));
                $('#Incident_Company').text(jsUtil.Replace.byId(await jsCompany.Lis(), 'Id', 'Name', Inc.ComId));
                $('#Incident_Office').text(jsUtil.Replace.byId(await jsOffice.Lis(), 'Id', 'Name', Inc.OffId));
                var Peo = await jsUtil.Replace.getObj(await jsPeople.Lis(), 'Id', Inc.PeoId);
                $('#Incident_People').text(`${Peo['NameP']} ${Peo['NameM']}, ${Peo['Name']}`);
                $('#Incident_Service').text(jsUtil.Replace.byId(await jsService.Lis(), 'Id', 'Name', Inc.SerId));

                jsUtil.Select.SetVal('#Incident_Status', Inc.State);
                jsUtil.Select.SetVal('#Incident_Urgency', Inc.UrgId);
                jsUtil.Select.SetVal('#Incident_Impact', Inc.ImpId);
                jsUtil.Select.SetVal('#Incident_Priority', Inc.PriId);


                await jsIncidentSla.tblLis("#tblIncidentSla", Inc.Id);


            }

        }



    </script>

</asp:Content>

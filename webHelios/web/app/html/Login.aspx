﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="web.app.html.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        
        <!-- Meta -->
        <!-- #include file="../meta.html" -->
        <!-- End Meta -->

        <!-- Title -->
        <title>Acceso • Service Desk - SigloBPO</title>
        <!-- End Title -->

        <!-- Style -->
        <!-- #include file="../style.html" -->    
        <!-- End Style -->
        
    </head>

    <body  class="gray-bg">

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <h1><img style="width:70%;" alt="image" src="/src/img/SigloBPO.png" /></h1>
                </div>
                <h3>Service Desk</h3>
                <p></p>
                <form id ="formLogin" class="m-t" role="form" method="post" action="javascript:jsPeople.Auth();" runat="server">
                    <div class="form-group">
                        <input id="userLogin" type="text" class="form-control" placeholder="Usuario" required="required" value="1111111"/>
                    </div>
                    <div class="form-group">
                        <input id="passLogin" type="password" class="form-control" placeholder="Contraseña" required="required" value="123bocnar"/>
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>

                    <p id="formResult"></p>
                    <a href="#"><small>¿Olvidaste tu Contraseña?</small></a>
                </form>
                <p class="m-t"><small>SigloBPO - Copyright 2017</small></p>
            </div>
        </div>


        <!-- Footer -->
        <!-- #include file="../footer.html" -->
        <!-- End Footer -->

    </body>

</html>
using Entity;
using System;
using System.Collections.Generic;

namespace Logic
{
    public partial class logUbigeo : ILogic<entUbigeo> {

        public IList<entUbigeo> children(entUbigeo _obj)
        {
            try { return _obj.children(); }
            catch (Exception ex) { throw ex; }
        }

        public IList<entUbigeo> reverse(entUbigeo _obj)
        {
            try { return _obj.reverse(); }
            catch (Exception ex) { throw ex; }
        }

    }
}


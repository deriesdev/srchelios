﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/Site.Master" AutoEventWireup="true" CodeBehind="Company.aspx.cs" Inherits="web.app.html.Company.Company" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Compañia • SigloBPO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="addHead" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="breadcrumb" runat="server">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Compañia</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="#">Administración de la configuración</a></li>
                <li class="active"><strong>Compañia</strong></li>
            </ol>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Lista de puestos de trabajo</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" onclick="jsCompany.tblLis('#tblCompany');"><i class="fa fa-refresh"></i>Actualizar</a></li>
                            <li><a href="#" onclick="jsCompany.$Form.Sel('#jsCompanySet_md', '#formCompanySet', '#tblCompany');"><i class="fa fa-eye"></i>Ver</a></li>
                            <li><a href="#" onclick="jsCompany.$Form.Ins('#jsCompanySet_md', '#formCompanySet');"><i class="fa fa-plus"></i>Agregar</a></li>
                            <li><a href="#" onclick="jsCompany.$Form.Upd('#jsCompanySet_md', '#formCompanySet', '#tblCompany');"><i class="fa fa-pencil-square-o"></i>Editar</a></li>
                            <li><a href="#"><i class="fa fa-times"></i>Eliminar</a></li>
                        </ul>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <table id="tblCompany" class="table table-striped table-bordered table-hover dataTables-example"></table>
                </div>
                <!--
                <div class="ibox-footer"></div>
                -->
            </div>
        </div>

    </div>



    <!-- Modal -->
    <div class="modal inmodal" id="jsCompanySet_md" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <!--modal-lg modal-sm-->
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <form id="formCompanySet" role="form" novalidate="novalidate">
                    <div class="modal-body">

                        <!-- -->
                        <div class="row">

                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre:</label>
                                    <input type="text" id="CompanySet_Name" name="CompanySet_Name" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>RUC:</label>
                                    <input type="number" id="CompanySet_NumberDoc" name="CompanySet_NumberDoc" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Dirección:</label>
                                    <input type="text" id="CompanySet_Address" name="CompanySet_Address" placeholder="Ingrese nombre" class="form-control valid" required />
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Pais:</label>
                                    <div class="form-control-select2">
                                        <select id="ubi01" name="ubi01" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Departamento:</label>
                                    <div class="form-control-select2">
                                        <select id="ubi02" name="ubi02" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Provincia:</label>
                                    <div class="form-control-select2">
                                        <select id="ubi03" name="ubi03" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Distrito:</label>
                                    <div class="form-control-select2">
                                        <select id="ubi04" name="ubi04" class="select2" style="width: 100% !important;"></select>
                                    </div>
                                </div>
                            </div>



                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label>Estado:</label>
                                    <div class="switch">
                                        <div class="onoffswitch">
                                            <input type="checkbox" id="CompanySet_Active" name="CompanySet_Active" class="onoffswitch-checkbox" disabled />
                                            <label class="onoffswitch-label" for="CompanySet_Active">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button id="CompanySet_Submit" type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="addFooter" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            try {
                jsCompany.tblLis('#tblCompany');
            } catch (e) {
                jsNotify.Show(5, "Error", e.message.toString());
            }
        });

    </script>

</asp:Content>

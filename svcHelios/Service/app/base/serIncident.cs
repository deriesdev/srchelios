using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcIncident
    {
        public entIncident add(entIncident _obj)
        {
            try
            {
                logIncident oEnt = new logIncident();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncident upd(entIncident _obj)
        {
            try
            {
                logIncident oEnt = new logIncident();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncident del(entIncident _obj)
        {
            try
            {
                logIncident oEnt = new logIncident();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entIncident sel(entIncident _obj)
        {
            try
            {
                logIncident oEnt = new logIncident();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entIncident> lis(entIncident _obj)
        {
            try
            {
                logIncident oEnt = new logIncident();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcIncident")]
    public partial interface intSvcIncident
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addIncident",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addIncident")]
        entIncident add(entIncident _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updIncident",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updIncident")]
        entIncident upd(entIncident _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delIncident",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delIncident")]
        entIncident del(entIncident _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selIncident",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selIncident")]
        entIncident sel(entIncident _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisIncident",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisIncident")]
        IList<entIncident> lis(entIncident _obj);
    }
}


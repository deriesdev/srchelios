﻿using Entity;
using Logic;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace Service
{
    public partial class Service : intSvcPeople
    {
        public entPeople add(entPeople _obj)
        {
            try
            {
                logPeople oEnt = new logPeople();
                return oEnt.add(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entPeople upd(entPeople _obj)
        {
            try
            {
                logPeople oEnt = new logPeople();
                return oEnt.upd(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entPeople del(entPeople _obj)
        {
            try
            {
                logPeople oEnt = new logPeople();
                return oEnt.del(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public entPeople sel(entPeople _obj)
        {
            try
            {
                logPeople oEnt = new logPeople();
                return oEnt.sel(_obj);
            }
            catch (Exception ex) { throw ex; }
        }

        public IList<entPeople> lis(entPeople _obj)
        {
            try
            {
                logPeople oEnt = new logPeople();
                return oEnt.lis(_obj);
            }
            catch (Exception ex) { throw ex; }
        }
    }

    [ServiceContract(Name = "svcPeople")]
    public partial interface intSvcPeople
    {
        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/addPeople",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "addPeople")]
        entPeople add(entPeople _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/updPeople",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "updPeople")]
        entPeople upd(entPeople _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/delPeople",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "delPeople")]
        entPeople del(entPeople _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/selPeople",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "selPeople")]
        entPeople sel(entPeople _obj);

        [WebInvoke(
        BodyStyle = WebMessageBodyStyle.Wrapped,
        Method = "POST",
        UriTemplate = "/lisPeople",
        ResponseFormat = WebMessageFormat.Json)]
        [OperationContract(Name = "lisPeople")]
        IList<entPeople> lis(entPeople _obj);
    }
}


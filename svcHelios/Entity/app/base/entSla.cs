using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entSla : entBase<entSla>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        [DataMember(Name = "Type", Order = 3)]
        public int type { set; get; } = 0;
        #endregion

        #region Constructor
        public entSla() { }
        public entSla(int _id) { id = _id; }
        public entSla(int _id, string _name, int _type, bool _active)
        {
            id = _id;
            name = _name;
            type = _type;
            active = _active;
        }
        #endregion
    }
}

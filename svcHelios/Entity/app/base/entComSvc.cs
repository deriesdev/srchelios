using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entComSvc : entBase<entComSvc>
    {
        #region Propiedades
        [DataMember(Name ="SerId", Order = 3)]
        public int serId { set; get; } = 0;
        #endregion

        #region Constructor
        public entComSvc () { }
        public entComSvc (int _id) { id = _id; }
        public entComSvc(int _id, int _comId, int _serId, bool _active)
        {
            id = _id;
            comId = _comId;
            serId = _serId;
            active = _active;
        }
        #endregion
        }
    }

using System;
using System.Runtime.Serialization;
namespace Entity
{
    [DataContract]
    public partial class entUrgency : entBase<entUrgency>
    {
        #region Propiedades
        [DataMember(Name = "Name", Order = 2)]
        public string name { set; get; } = string.Empty;
        #endregion

        #region Constructor
        public entUrgency() { }
        public entUrgency(int _id) { id = _id; }
        public entUrgency(int _id, string _name)
        {
            id = _id;
            name = _name;
        }
        #endregion
    }
}

﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Entity
{
    [DataContract]
    public abstract class entBase<T> : ICRUD<T> where T : entBase<T>
    {
        #region Properties
        protected string uspIns => $"usp{this.GetType().Name.Substring(3)}Ins";
        protected string uspUpd => $"usp{this.GetType().Name.Substring(3)}Upd";
        protected string uspDel => $"usp{this.GetType().Name.Substring(3)}Del";
        protected string uspLis => $"usp{this.GetType().Name.Substring(3)}Lis";
        protected string uspSel => $"usp{this.GetType().Name.Substring(3)}Sel";

        [DataMember(Name = "Id", Order = 1)]
        public int id { get; set; } = 0;

        [DataMember(Name = "PeoId", Order = 100)]
        public int peoId { set; get; } = 0;
        [DataMember(Name = "ComId", Order = 101)]
        public int comId { set; get; } = 0;
        [DataMember(Name = "Active", Order = 102)]
        public bool active { set; get; } = true;

        [DataMember(Name = "Err", Order = 150)]
        public string err { get; set; } = string.Empty;
        [DataMember(Name = "ErrCatch", Order = 150)]
        public string errCatch { get; set; } = string.Empty;

        #endregion        

        #region Methods
        public bool add()
        {
            try
            {
                using (var con = new Connection())
                {
                    int req = con.getScalar(uspIns, this);
                    return (id = req) > 0;
                }
                
            }
            catch (Exception ex) { throw ex; }
        }
        public bool upd()
        {
            try
            {
                using (var con = new Connection())
                {
                    int req = con.getExecute(uspUpd, this);
                    return (id = req) > 0;
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public bool del()
        {
            try
            {
                using (var con = new Connection())
                {
                    int req = con.getExecute(uspDel, this);
                    return (id = req) > 0;
                }
            }
            catch (Exception ex) { throw ex; }
        }
        public bool sel()
        {
            try
            {
                bool rpt = false;
                using (var con = new Connection())
                {
                    rpt = con.getObj<T>(uspSel, this);
                }
                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }
        public IList<T> lis()
        {
            try
            {
                var rpt = new List<T>();
                using (var con = new Connection())
                {
                    rpt = con.getList<T>(uspLis, this);
                }
                return rpt;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion
    }
}

﻿CREATE TABLE [dbo].[menuType]
(
	[id]		INT NOT NULL PRIMARY KEY IDENTITY,
	[name]		varchar(200) NOT NULL,
	[active]	bit NULL
)
